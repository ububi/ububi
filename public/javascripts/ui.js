//Preload data
var units;
var idsNotLinked;
var UNITS_CACHE = [];
var lstBIMProcess = [];
var API_INFO = {};


/**
 * @summary Displays a process' information in the right-side panel.
 * @param processUrl The process' URL from which to query the REST information.
 */
displayProcess = function(processUrl) {
    //get basic process information
    let procAjax = getUrlAjax(processUrl);
    $.when(processUrl, procAjax).done(function(processUrl, procJson) {
        let process = procJson[0];

        // show the process documentation
        let docAjax = getUrlAjax(process.documentation);
        $.when(processUrl, process, docAjax).done(function(processUrl, process, docJson){
            let doc = docJson[0];
            displayProcessInfoAndDoc(processUrl, process, doc);
        });

        // show the process exchanges
        getUrlAjax(process.exchanges).done(function(exchJson) {
            let exchanges = exchJson.lst;
            displayProcessExchanges(exchanges);
        });

        // show the process parameter information
        getUrlAjax(process.parameters).done(function(paramJson) {
            let params = paramJson.lst;
            displayProcessParams(params,PARAM_TABLE_TEMPLATE, PARAM_ROW_TEMPLATE);
        });
    }).fail(function(err){
        cosole.error("Error while displaying process");
    });
};

setParameterFormula = function(processId, iterateOnce) {

    $('#table_local_parameter').each(function(){
        $(this).find('tr').not(':first').each(function(){
            let name = $(this).find('td').eq(0).text();
            let value = $(this).find('input').eq(0).val();
            let formula = $(this).find('input').eq(1).val();

            if(!formula)
                formula = " ";
            if(!value)
                value = 0;

            getAjaxFromRouter(
                jsModel.controllers.API.model.Process.setParameterFormula(processId, formula, name, value),""
            ).done(function(response){
                refreshProcessParams(PARAM_TABLE_TEMPLATE, PARAM_ROW_TEMPLATE);
            }).fail(function(jqXHR, textStatus, errorThrown) {
                console.log("failed!");
                console.log(errorThrown);
            });
        });
    });

    if(iterateOnce != 0){
        iterateOnce--;
        setParameterFormula(processId, iterateOnce);
    }

    refreshProcessParams(PARAM_TABLE_TEMPLATE,PARAM_ROW_TEMPLATE);
};

refreshProcessParams = function(tableTemplateType, rowTemplateType) {
    let processUrl = $("#process_url").val();
    let procAjax = getUrlAjax(processUrl);
    $.when(processUrl, procAjax).done(function(processUrl, procJson) {
        let process = procJson[0];

        getUrlAjax(process.parameters).done(function(paramJson) {
            let params = paramJson.lst;
            displayProcessParams(params, tableTemplateType, rowTemplateType);
        });
    });
};

retrieveGlobalParams = function() {
    getAjaxFromRouter(jsModel.controllers.API.model.Process.getGlobalParameters(),"").done(function(paramJson) {
        let params = paramJson.lst;
        displayGlobalParams(params);
    });
};

// Display sub-functions
displayProcessInfoAndDoc = function(url, process, doc) {
    let html = PROCESS_INFOBOX_TEMPLATE.replace("%process_url%", url);
    html = html
            .replace("%process_id%", process.id)
            .replace("%process_name%", escapeInputString(process.name))
            .replace("%process_description%", process.description == null ? "" : escapeHtmlString(process.description))
            .replace("%doc_creationDate%", doc.creationDate == null ? "" : escapeInputString(doc.creationDate))
            .replace("%doc_geography%", doc.geography == null ? "" : escapeInputString(doc.geography))
            .replace("%doc_modeling_constant%", doc.modelingConstant == null ? "" : escapeInputString(doc.modelingConstant))
            .replace("%doc_copyright%", doc.copyright == null ? "" : escapeInputString(doc.copyright))
            .replace("%doc_valid_from%", doc.validFrom == null ? "" : escapeInputString(doc.validFrom))
            .replace("%doc_technology%", doc.technology == null ? "" : escapeInputString(doc.technology))
            .replace("%doc_data_treatment%", doc.treatment == null ? "" : escapeInputString(doc.treatment))
            .replace("%doc_valid_until%", doc.validUntil == null ? "" : escapeInputString(doc.validUntil))
            .replace("%doc_time%", doc.time == null ? "" : escapeInputString(doc.time))
            .replace("%doc_sampling%", doc.sampling == null ? "" : escapeInputString(doc.sampling))
            .replace("%doc_location_code%",process.location==null || process.location.code==null?"":escapeInputString(process.location.code))
            .replace("%process_uuid%",process==null?"":process.refId);


    //let selector = $("#exch_" + exchange.id+".providerSelect");
    processSelected = process.id; //now you can link with revit from right click, note that this should be set eventually directly on right click

    $("#process").html(html);
};
displayProcessExchanges = function(exchanges) {

    //build header/table bodies/footer
    html = "";
    html += EXCHANGE_HEADER_INPUT;
    html += EXCHANGE_TABLE_TEMPLATE.replace("%tbody_id", "exchanges_tbody_in");
    html += EXCHANGE_HEADER_OUTPUT;
    html += EXCHANGE_TABLE_TEMPLATE.replace("%tbody_id", "exchanges_tbody_out");
    html += MODAL_EXCHANGE;

    $("#exchanges").html(html);

    $('#btnInput').click(function(){
        $('#newex_isinput').attr('checked', 'checked');
        $('#newex_isinput').attr('data-value', 'input');
    });

    $('#btnOutput').click(function(){
        $('#newex_isinput').attr('data-value', 'output');
        $('#newex_isinput').removeAttr('checked');
    });

    //then, for each exchange...
    for(var exchange of exchanges) {
        let producersAjax = null;
        if(exchange.isInput!=0)
            producersAjax = getUrlAjax(exchange.producers);

        let flowAjax = getUrlAjax(exchange.flow);

        // get the associated flow and unit, and the producers
        $.when(exchange, producersAjax, flowAjax).done(function(exchange, producersJson, flowJson) {
            let producers =null;
            if(producersJson!=null && producersJson[0].lst!=null)
                producers = producersJson[0].lst;
            let flow = flowJson[0];
            let unit = getUnit(exchange.unitId);

            //build the row
            let sel = exchange.isInput ? $("#exchanges_tbody_in") : $("#exchanges_tbody_out");
            sel.append(
                EXCHANGE_ROW_TEMPLATE
                    .replace(/%id/g, exchange.id)
                    .replace("%input", exchange.isInput)
                    .replace("%flow", flow.name)
                    .replace("%amount", "<input type='text' onfocus='clicksToUpdateExchangeAmount(this)' onblur='updateExchangeAmount("+exchange.id+", this)' value='"+exchange.amountValue+"'>")
                    .replace("%formula", exchange.amountFormula == null ? "" : exchange.amountFormula)
                    .replace("%unit", unit.name)
            );

            $(".btnFormula").click(function(){
                $('#paramTab').tab('show');
            });

            //build the select options
            let selector = $("#exch_" + exchange.id+".providerSelect");
            if(flow.flowType == "ELEMENTARY_FLOW") {
                selector.append(buildSelectOptionHtml("default", "Elementary Flow", true));
                selector.attr("disabled", "");
            }
            else if(exchange.isInput==0){
                selector.append(buildSelectOptionHtml("default", "&lt;Default&gt;", true));
                selector.attr("disabled", "");
            }
            else {
                selector.append(buildSelectOptionHtml("default", "&lt;Default&gt;", exchange.defaultProviderId == 0));
                for (let producer of producers) {
                    var location = "";
                    if(producer.location!=null && producer.location.code !=null)
                        location =  " - "+producer.location.code;

                    selector.append(
                        buildSelectOptionHtml(producer.id, escapeHtmlString(producer.name), exchange.defaultProviderId == producer.id)
                    );//exchange id is provided in the select, so option only needs producer id
                }
            }

        });
    }
};

clicksToUpdateExchangeAmount = function(inputElement)
{
    //store this in global in order to set back to this value if the qty cant be changed
    exchangeAmountPrevValue = inputElement.value;
}

updateExchangeAmount = function(exchangeId, inputElement)
{
        if(exchangeAmountPrevValue != inputElement.value)
        {
            getAjaxFromRouter(
                jsModel.controllers.API.model.BIMModel.getExchange(Number.parseInt($("#modelSelect").val()), exchangeId),null
            ).done(function(response){
                updateAmount(exchangeId,inputElement,response.BIMEntityType);
            }).fail(function(){
                updateAmount(exchangeId,inputElement,null);
            });
        }
}

function updateAmount(exchangeId, inputElement, bimEntity)
{
    if(bimEntity == "BIMUniformat" || bimEntity == "BIMRoot" || bimEntity == "Unknown")
    {
        inputElement.value = exchangeAmountPrevValue;
        $("#errorDialogueWhenChangingAmount").modal();
    }
    else
    {
        let createAjax = getAjaxFromRouter(
            jsModel.controllers.API.model.Exchange.update(exchangeId),
            {
                amountValue : parseFloat(inputElement.value)
            }
        );
    }
}

displayProcessParams = function(params, tableTemplateType, rowTemplateType) {
    $("#params").html(tableTemplateType);

    for(let param of params) {
        if(param.value == 0)
            param.value = "";

        $("#param_tbody").append(rowTemplateType
            .replace("%id", param.id)
            .replace("%name", param.name)
            .replace("%value", param.value)
            .replace("%desc", param.description ? param.description : "" )
            .replace("%formula", param.formula ? param.formula : ""));

        if(param.formula)
            $("#parameter_value_"+param.id).attr("disabled", true);
    }
};

displayGlobalParams = function(params) {
    $("#global_parameter").html(GLOBAL_PARAM_TABLE_TEMPLATE);

    for(let param of params) {
        $("#global_param_tbody").append(GLOBAL_PARAM_ROW_TEMPLATE
            .replace("%name", param.name)
            .replace("%value", param.value)
            .replace("%desc", param.description ? param.description : "" )
            .replace("%formula", param.formula ? param.formula : ""));
    }
};

updateUnitsSelect = function(flowId, defaultUnit) {
    let flowUnitsAjax = getGroupUnitsAjax(flowId);
    flowUnitsAjax.done(function(units){
        let unitSelectHtml = "";
        for(let unit of units.lst) {
            unitSelectHtml += buildSelectOptionHtml(unit.id, unit.name,defaultUnit==unit.id);
        }
        $('#newex_unit')
            .empty()
            .append(unitSelectHtml);
        $('#newex_unit').val(defaultUnit);
    });
};

hideProcessDetails = function() {
    $("#global_parameter").html("");
    $("#process").html("");
    $("#exchanges").html("");
    $("#params").html("");
};

buildSelectOptionHtml = function(value, text, isSelected) {
    //`<option value="%val" %attr>%text</option>`;
    if(isSelected == undefined) isSelected = false;
    return SELECT_OPTION_TEMPLATE
        .replace("%val", value)
        .replace("%text", text)
        .replace("%attr", isSelected ? "selected" : "");
};

// Button Events for editing process information
$(document).on("click", "#process_edit", function() {
    $(".process_input").removeAttr("disabled");

    //show save/cancel buttons, hide edit button
    $("#process_edit").attr("type", "hidden");
    $("#process_save").attr("type", "button");
    $("#process_cancel").attr("type", "button");
    $("#process_button_spacer").attr("class", "col-md-6");
});
$(document).on("click", "#process_save", function() {
    $(".process_input").attr("disabled", "");

    //show edit button, hide save/cancel buttons
    $("#process_edit").attr("type", "button");
    $("#process_save").attr("type", "hidden");
    $("#process_cancel").attr("type", "hidden");
    $("#process_button_spacer").attr("class", "col-md-10");
});
$(document).on("click", "#process_cancel", function() {
    //restore from embedded url
    let url = $("#process_url").val();
    displayProcess(url);
});

// Select event for changing the default provider
$(document).on("change", ".providerSelect", function(sender) {
    let exchangeId = sender.currentTarget.id.split("_")[1];//get the ##### from the "exch_######" part
    let exchangeUrl = getExchangeUrl(exchangeId);
    let selected = sender.currentTarget.value;
    let newVal = selected == "default" ? "0" : selected;
    let root = getRootNode();

    var ajaxUpdate = getAjaxFromRouter(jsModel.controllers.API.model.Exchange.update(exchangeId),{ defaultProviderId : newVal });

    ajaxUpdate.done(function(){
        //refresh the nodes
        recursiveDo(root.children, function(node, args) {
                let tree = $("#tree").jstree(true);
                let exchangeUrl = args[0];
                let providerUrl = exchangeUrl + "/provider";
                let newVal = args[1];

                if(node.original.providerUrl == providerUrl) {
                    //get the new process
                    $.when(node, getUrlAjax(providerUrl)).done(function(node, providerJson) {
                        let newProvider = providerJson[0];
                        node.text = newProvider.name;   //update display text to new process name
                        tree.delete_node(node.children);//clear its children
                        tree.close_node(node);          //close the node
                        node.state.loaded = false;      //make it re-openable
                        tree.refresh_node(node);        //refresh it to get children and restore the openable + marker
                    });
                }
            },
            [exchangeUrl, newVal]);
        displayProcess($("#process_url").val());//refresh process pane
    });
});

// Button Event for deleting an exchange
$(document).on("click", ".deleteExchange", function(sender) {
    let res = confirm("Are you sure you wish to delete this exchange form this process?");
    if(!res) {
        return;
    }
    let exchangeId = sender.currentTarget.id.split("_")[1];//get the ##### from the "exch_######" part
    let exchangeUrl = getExchangeUrl(exchangeId);

    //delete the node
    $.ajax(jsModel.controllers.API.model.Exchange.delete(exchangeId))
        .fail((resp) => {
            console.error(`Deleting exchange ID ${exchangeId} failed with error ${resp.statusCode}`);
            $(".button_row").html(ALERT_TEMPLATE.replace("%msg%", "Could not delete that exchange"));
        })
        .done((resp) => {
            recursiveDo(getRootNode().children, function(node, args) {
                    let exchangeUrl = args[0];
                    let providerUrl = exchangeUrl + "/provider";
                    if(node.original.providerUrl == providerUrl) {
                        deleteTreeNode(node);
                    }
                },
                [exchangeUrl]);
            displayProcess($("#process_url").val());//refresh process pane
        });
});

// Button and control events for adding a new exchange

$(document).on("click", "#newex_done", function(sender){

    let parentProviderUrl = $("#process_url").val();
    let newex_processId = $("#process_id").val();
    let newex_flowId = $("#newex_flow").val();


    if($("#newex_isinput").attr('data-value') != 'input'){
        newex_isinput = 0;
        newex_isinput = 0;
    }else{
        newex_isinput = 1;
    }
    // let newex_isinput = $("#newex_isinput").val() == "on" ? 1 : 0;
    let newex_unitId = $("#newex_unit").val();
    let newex_amount = parseFloat($("#newex_amount").val());

    let createAjax = getAjaxFromRouter(
        jsModel.controllers.API.model.Exchange.insert(),
        {
            processId : newex_processId,
            flowId : newex_flowId,
            isInput : newex_isinput,
            amountValue : newex_amount,
            unitId : newex_unitId
        }
    );

    let flowAjax = getUrlAjax(jsModel.controllers.API.model.Flow.getInfo(newex_flowId).url);

    $.when(createAjax, flowAjax, parentProviderUrl).then(function(exchangeJson, flowJson, parentProviderUrl){
        let exchange = exchangeJson[0];
        let flow = flowJson[0];

        let providerAjax = flow.flowType == "ELEMENTARY_FLOW" ? "No Provider" : getUrlAjax(exchange.provider);

        $.when(exchange, flow, parentProviderUrl, providerAjax).then(function(exchange, flow, parentProviderUrl, providerJson) {
            let provider = providerJson[0];
            let nodeData = createNodeJson(flow.name, exchange.provider, provider.id, flow.flowType != "ELEMENTARY_FLOW");//(text, providerUrl, processUrl, hasChildren)

            recursiveDo(getRootNode().children, function(node, args) {
                let providerUrl = args[0];
                let nodeData = args[1];
                if(node.original.providerUrl == providerUrl && node.state.loaded) {
                    createTreeNode(node, nodeData);
                }
            },
            [parentProviderUrl, nodeData]);
        });


    });

    hideNewExchangeModal();
    displayProcess(parentProviderUrl);//refresh process pane
});
$(document).on("click", "#newex_cancel", function(sender){
    hideNewExchangeModal();
});

var flowsFound;
$(document).on("click", "#newex_flowsearchbtn", function(sender) {
    let searchVal = $("#newex_flowsearchtext").val();
    let flow_type = $("#newex_flowtype").val();


    getAjaxFromRouter(
        jsModel.controllers.API.model.Flow.search(),
        {
            'keyword' : searchVal,
            'flow_type' : flow_type
        }
    ).done(function(response){
        let flows = response.lst;
        flowsFound=flows;
        let html = "";
        for(let flow of flows) {
            html += buildSelectOptionHtml(flow.id, flow.name);
        }
        $("#newex_flow").append(html);
        updateUnitsSelect(flows[0].id,flows[0].defaultUnit);

    }).fail(function() {
        $("#newex_alert").html(
            ALERT_TEMPLATE.replace("%msg%", "Search term needs to be at least 4 characters long.")
        );
    });
});

$(document).on("change", "#newex_flow", function(sender) {
    let newex_flowId = $("#newex_flow").val();

    var flow = null;
    var defaultUnit = -1;
    var i = 0;
    while(flow==null && i<flowsFound.length)
    {
        if(flowsFound[i].id==newex_flowId)
            flow = flowsFound[i];
        i++;
    }
    if(flow!=null)
        defaultUnit=flow.defaultUnit;

    updateUnitsSelect(newex_flowId,defaultUnit);
});

$(document).on("click", "#parameter_edit", function() {
    refreshProcessParams(PARAM_TABLE_TEMPLATE_EDIT, PARAM_ROW_TEMPLATE_EDIT);
});
$(document).on("click", "#parameter_save", function() {
    let processId = $("#process_id").val();
    setParameterFormula(processId, 2);
});

$(document).on("click", "#parameter_cancel", function() {
    refreshProcessParams(PARAM_TABLE_TEMPLATE, PARAM_ROW_TEMPLATE);
});

hideNewExchangeModal = function() {
    let sel = $("#newExchangeModalForm");
    sel.removeClass("in");
    sel.modal("hide");
    $(".modal-backdrop").remove();
};

recursiveDo = function(nodeIds, doer, args) {
    for(let nodeId of nodeIds) {
        let node = getTreeNode(nodeId);
        doer(node, args);
        recursiveDo(node.children, doer, args);
    }
};

/**
 * @summary Sets up the page to be ready for user input
 */
$(document).ready(function() {
    initPage();
    // init jstree
    initTree();

    //get api version
    getApiAjax().done(function(apiJson) {
        API_INFO = JSON.parse(apiJson);
        $("#api_name").html(API_INFO.name);
        $("#api_version").html(API_INFO.version);
    });

    // setup controls for model selection and refresh
    // 1 - populate drop-down
    var modelSelect = $("#modelSelect");
    var modelUploadSelect = $("#modelUploadSelect");
    getModelsAjax().done(function(modelsJson) {
        let models = modelsJson.lst;

        for(let model of models) {
            modelSelect.append(`<option value="${model.id}">${model.name}</option>`);
            modelUploadSelect.append(`<option value="${model.id}">${model.name}</option>`);
        }

        if(document.getElementById("modelUploadSelect").options.length>1)
        {
            $("#btnUploadDialog").removeClass("glow");
        }
        else
        {
            $("#btnUploadDialog").addClass("glow");
        }
    });

    modelUploadSelect.change(function() {

        //new model (create)
        if(document.getElementById("modelUploadSelect").selectedIndex  == 0)
        {
            document.getElementById("boxModelName").style.display = "";

        }
        else //exisiting
        {
            document.getElementById("boxModelName").style.display = "none";
        }
    });

    var modelResultSet = $("#modelResultSet");
    getResultSetsAjax().done(function(modelsJson) {
        let models = modelsJson.lst;

        for(let model of models) {
            modelResultSet.append(`<option value="${model.id}">${model.description} - ${model.calculationDate}</option>`);
        }
    }).fail(function(XMLHttpRequest, textStatus, errorThrown) {
        console.error(textStatus);
        console.error(errorThrown);
    });

    // 2 - add event handler for the drop down
    var loadModel = function() {
        let modelId = Number.parseInt(modelSelect.val());
        let modelResultSetId = Number.parseInt(modelResultSet.val());

        loadUnits();
        loadBimProcess(modelId);
        getNotLinkedId(modelId, function(){
            fetchRoot(modelId, modelResultSetId);
            if(modelId > 0) {
                showCalculateButtonAndComboBox();
                retrieveGlobalParams();
            } else {
                hideCalculateButtonAndComboBox();
                hideProcessDetails();
            }
        });
    };
    modelSelect.change(loadModel);
    modelResultSet.change(loadModel);

    // 3 - add event handler for refresh button
    $("#btnRefreshModel").click(loadModel);


    //Method for Calculate button
    var methodSelect = $('#methodSelect');
    getImpactMethodsAjax().done(function(modelsJson) {
        let impactMethods = modelsJson.lst;

        for(let impactMethod of impactMethods) {
            methodSelect.append(`<option value="${impactMethod.id}">${impactMethod.name}</option>`);
        }
    });

    var calculateResult = function() {
        showLoadingCircle();
        let modelId = Number.parseInt(modelSelect.val());
        let impactMethodId = Number.parseInt(methodSelect.val());
        let description = $('#methodDescription').val();

        getAjaxFromRouter(
            jsModel.controllers.API.model.BIMModel.calculateResult(modelId, impactMethodId, description),""
        ).done(function(response){
            hideLoadingCircle();
            console.log(response);
        }).fail(function(jqXHR, textStatus, errorThrown) {
            hideLoadingCircle();
            console.log("failed!")
            console.log(errorThrown);
        });
    };
    $("#btnStartCalculate").click(calculateResult);

    hideLoadingCircle = function() {
        $("#btnStartCalculate").attr("disabled", false);
        $('#btnStartCalculate').val('Calculate');
        //$('#loader img').hide();
        $('#loaderModal img').hide();
        $('#loader').css('visibility', 'hidden');
    };

    showLoadingCircle = function() {
        $("#btnStartCalculate").attr("disabled", true);
        $('#btnStartCalculate').val('Calculating...');
        //$('#loader img').show();
        $('#loaderModal img').show();
        $('#loader').css('visibility', 'visible');
    };

    hideCalculateButtonAndComboBox = function() {

        $('#btnCalculateResult').css('visibility', 'hidden');
        $('#btnCalculateResult').hide();

        $('#btnShowResults').css('visibility', 'hidden');
        $('#btnShowResults').hide();
    };

    showCalculateButtonAndComboBox = function() {
        $('#btnCalculateResult').css('visibility', 'visible');
        $('#btnCalculateResult').show();

        $('#btnShowResults').css('visibility', 'visible');
        $('#btnShowResults').show();
    };

    var uploadFiles = function() {
        var formData = new FormData();
        startUpload();

        var isEdit; //bool

        //new model (create)
        if(document.getElementById("modelUploadSelect").selectedIndex  == 0)
        {
            isEdit  = false;
            modelId = -1;
        }
        else //exisiting
        {
            document.getElementById("modelName").value = "";
            modelId = $("#modelUploadSelect").val();

            isEdit = true;
        }

        var name = document.getElementById("modelName").value;

        formData.append("name", name);
        formData.append("modelId", modelId);
        formData.append("ied", document.getElementById("ied").files[0]);
        formData.append("upload_file", true);
        formData.append("isEdit", isEdit);
        $.ajax({
            type: "POST",
            url: "/api/IED",
            async: true,
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            timeout: 600000,
            success: function(data) {
                if(isNaN(data))//usually the BIMModel id is returned, if not, it's an error.
                {
                    document.getElementById("upload-model-messages-section").innerHTML = data;
                    $("#upload-model-messages-section").show();
                }
                else
                {
                    if(modelId==-1)
                        addModel(data,name);
                    document.getElementById("upload-model-messages-section").innerHTML = "Upload Successful";
                    $("#upload-model-messages-section").show();
                }
                endUpload();
            }
        });
    };


    $("#btnUploadFiles").click(uploadFiles);

    $('#modelSelect').click(function(){
        initPage();
    });

});

$( window ).resize(function() {
    initPage();
});

$("#btnShowResults").click(function(){
    window.open(jsWeb.controllers.Web.Application.results($("#modelSelect").val()).url,"_blank");
});

$(document).on('click', '.browse', function(){
    var file = $(this).parent().parent().parent().find('.file');
    file.trigger('click');
});

$(document).on('change', '.file', function(){
    $(this).parent().find('.form-control').val($(this).val().replace(/C:\\fakepath\\/i, ''));
});

function showInRevit() {
    var id = document.getElementById("process_id").value;
    var jsonObj = {"id":id};

    getAjaxFromRouter(jsAPI.controllers.API.RevitSocketLink.selectId(),jsonObj).done(
        function(response) {
            if(response.error!=0)
                alert("Error: "+response.message);
        }
    );
}

function toggleLoadingScreen()
{
    $('#loadingDialogue').modal('toggle');
}

function selectRevitId(selectId)
{
    var formData = new FormData();

    formData.append("id", parseInt(selectId));
    $.ajax({
        type: "POST",
        url: "/api/RevitSocketLink/selectId",
        async: true,
        data: formData,
        cache: false,
        contentType: false,
        processData: false,
        timeout: 60000,
        success: function(data) {

        }
    });
}

function renewToken()
{
    getAjaxFromRouter(jsAPI.controllers.API.RevitSocketLink.renewToken(),null).done(
        function(response) {
            document.getElementById("revitToken").innerHTML=response.token;
        }
    );
}

function startUpload()
{
    $("#upload-model-messages-section").hide();
    document.getElementById("btnUploadFiles").innerHTML = "Working...";
    document.getElementById("btnUploadFiles").disabled = true;
    $('#loaderUploadModal img').show();
    $('#loaderUpload').css('visibility', 'visible');
}

function endUpload()
{
    $('#loaderUploadModal img').hide();
    $('#loaderUpload').css('visibility', 'hidden');
    document.getElementById("modelName").value = "";
    document.getElementById("btnUploadFiles").innerHTML = "Upload";
    document.getElementById("btnUploadFiles").disabled = false;
}

function addModel(idmodel, name)
{
    var option = '<option value="'+idmodel+'">'+name+'</option>';
    $("#modelSelect").append(option);
    $("#modelUploadSelect").append(option);
    $("#btnUploadDialog").removeClass("glow");
}

function initPage(){
    $('#boxContent').css('margin-top', $('#menuTop').height());
    $('.client-sidebar').css('top', $('#menuTop').height());
}

function getNotLinkedId(idModel,callback)
{
    idsNotLinked = {lst:[]};
    if(idModel>0)
    {
        $.ajax(jsModel.controllers.API.model.BIMModel.getForeGroundProcess(idModel)).done(function(response){
            idsNotLinked = response;
            callback();
        });
    }
    else
        callback();
}

function escapeHtmlString(str)
{
    return escapeString(str,["<",">","\"","&"],["&lt;","&gt;","&quot","&amp"]);
}

function escapeInputString(str)
{
    return escapeString(str,["\""],["\\\""]);
}

function escapeString(str,inChar,outChar)
{
    if(str==null || typeof str != "string")
        return str;

    for(var i=0;i<inChar.length;i++)
        str=replaceAll(str,inChar[i],outChar[i]);
    return str;
}
function escapeRegExp(str) {
    return str.replace(/([.*+?^=!:${}()|\[\]\/\\])/g, "\\$1");
}

function replaceAll(str, find, replace) {
    return str.replace(new RegExp(escapeRegExp(find), 'g'), replace);
}

function loadUnits(callback)
{
    if(units==null) {
        $.ajax(jsModel.controllers.API.model.Unit.all()).done(function (response) {
            if (response != null && response.lst != null) {
                units=[];
                for (var i = 0; i < response.lst.length; i++) {
                    units[response.lst[i].id]=response.lst[i];
                }
            }
            if (callback != null)
                callback();
        }).fail(function () {
            console.error("Units cachings failed");
            if (callback != null)
                callback();
        });
    }
    else if (callback != null)
        callback();
}

function getUnit(idUnit)
{
    loadUnits();
    return units[idUnit];
}

function loadBimProcess(idModel,callback)
{
    if(idModel>0) {
        $.ajax(jsModel.controllers.API.model.BIMProcess.all()).done(function (response) {
            if (response != null && response.lst != null) {
                lstBIMProcess = [];
                for (var i = 0; i < response.lst.length; i++) {
                    lstBIMProcess[response.lst[i].idEntity] = response.lst[i];
                }
            }
            if (callback != null)
                callback();
        }).fail(function () {
            console.error("BIMProcess cachings failed");
            if (callback != null)
                callback();
        });
    }
    else {
        lstBIMProcess=[];
        if (callback != null)
            callback();
    }
}

function findBimProcess(idProcess)
{
    return lstBIMProcess[idProcess];
}

function showFlowProperty()
{
    var idProcess = document.getElementById("process_id").value;
    var lastIdProcess=document.getElementById("flowPropertyProcess");
    var table = document.getElementById("flowPropertyTable");
    var elem="";
    if(idProcess!=lastIdProcess.value)
        $.ajax(jsModel.controllers.API.model.Process.getFlowPropertyFactor(idProcess)).done(function(response){

            if(response!=null && response.lst!=null)
                for(var i=0;i<response.lst.length;i++) {

                    elem += '<tr><td>'+
                            response.lst[i].flowProperty.name+'</td><td>'+
                            response.lst[i].conversionFactor+'</td><td>'+
                            response.lst[i].flowProperty.unitGroup.defaultUnit.name+'</td></tr>';
                }
            table.innerHTML=elem;
            lastIdProcess.value=idProcess;
            $('#flowPropertyModal').modal('show');
        });
    else
        $('#flowPropertyModal').modal('show');
}
function Test(Title, Text)
{
    var start = new Date();
    PrintTestTitle(start + " "+ Title);
    PrintTestResult(Text);
    PrintTestEnd(Title, start);
}

function TestJson(Title, Url)
{
    var start = new Date();
    PrintTestTitle(start + " Start "+ Title + "(" + Url + ")");
    $.getJSON(Url, function(data){
        PrintTestResult(data);
        PrintTestEnd(Title, start);
    });

}

function TestAjax(Title,router)
{
    TestAjax(Title,router, "");
}

function TestAjax(Title,router, data)
{
    var start = new Date();
    PrintTestTitle(start + " Start "+ Title + "(" + router.url + ")");

    $.ajax({
        type: router.type,
        url: router.url,
        contentType: 'application/json; charset=utf-8',
        data: JSON.stringify(data),
        dataType: 'json',
        success: function(jsonData) {
            PrintTest("Success");
            PrintJsonResult(jsonData);
        },
        error: function( jqXHR, textStatus, errorThrown ) {
            PrintTest("Error");
            PrintTestResult(textStatus+" "+errorThrown);
        },
        complete:function() {
            PrintTestEnd(Title, start);
        }
    });
}

function TestAjaxFile(Title,router, data)
{
    var start = new Date();
    PrintTestTitle(start + " Start "+ Title + "(" + router.url + ")");

    $.ajax({
        type: router.type,
        url: router.url,
        async: true,
        data: data,
        cache: false,
        contentType: false,
        processData: false,
        timeout: 600000,
        success: function(jsonData) {
            PrintTest("Success");
            PrintJsonResult(jsonData);
        },
        error: function( jqXHR, textStatus, errorThrown ) {
            PrintTest("Error");
            PrintTestResult(textStatus+" "+errorThrown);
        },
        complete:function() {
            PrintTestEnd(Title, start);
        }
    });
}

function PrintTest(Text)
{
    var out = document.getElementById("out");
    var div = document.createElement("div");
    div.innerHTML=Text;
    out.insertBefore(div,out.firstChild);
}

function PrintTestTitle(Title)
{
    PrintTest("<p class='TestTitle'>" + Title +"</p>")
}

function PrintTestResult(Result)
{
    PrintTest("<p class='TestResult'>"+Result+"</p>");
}

function PrintJsonResult(Result)
{
    PrintTest("<p class='TestResult'>"+JSON.stringify(Result,null,2)+"</p>");
}

function PrintTestEnd(Title,Start)
{
    var stop = new Date();
    var elapsed = stop.getTime()-Start.getTime();

    PrintTest("<p class='TestEnd'>"+ stop +" End "+ Title +" ("+ elapsed +" ms)</p>");

}

$(document).on('click', '.browse', function(){
    var fileCtrl = this.getAttribute("file-control");
    document.getElementById(fileCtrl).click();
});

function changeFile() {
    var input = $("#middlegroundfile"),
        numFiles = input.get(0).files ? input.get(0).files.length : 1,
        label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
    input.trigger('fileselect', [numFiles, label]);
    $("#"+document.getElementById("middlegroundfile").getAttribute("input-label")).val(input.val().replace(/C:\\fakepath\\/i, ''));
}

function UploadMiddleGround()
{
    var formData = new FormData();
    formData.append("middlegroundfile", document.getElementById("middlegroundfile").files[0]);
    formData.append("entityType","M");
    TestAjaxFile("Import middleground",jsModel.controllers.API.model.MiddleGround.create(),formData);
}
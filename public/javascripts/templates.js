// template for the process tab
// *** should probably be ported to the .html file, since this is mostly static
var PROCESS_INFOBOX_TEMPLATE = "";
PROCESS_INFOBOX_TEMPLATE += '<form class="form-horizontal container-process">';
PROCESS_INFOBOX_TEMPLATE += '   <input type="hidden" id="process_url" value="%process_url%">';
PROCESS_INFOBOX_TEMPLATE += '   <input type="hidden" id="process_id" value="%process_id%">';
PROCESS_INFOBOX_TEMPLATE += '   <div class="row button_row">';
PROCESS_INFOBOX_TEMPLATE += '       <div class="col-md-2">';
PROCESS_INFOBOX_TEMPLATE += '           <input class="form-control btn btn-default" type="button" id="process_viewInRevit" value="View in Revit" onclick="showInRevit();"/>';
PROCESS_INFOBOX_TEMPLATE += '       </div>';
PROCESS_INFOBOX_TEMPLATE += '       <div id="process_button_spacer" class="col-md-8"></div>';
PROCESS_INFOBOX_TEMPLATE += '       <div class="col-md-2">';
PROCESS_INFOBOX_TEMPLATE += '           <input class="form-control btn btn-primary" type="button" id="process_edit" value="Edit" />';
PROCESS_INFOBOX_TEMPLATE += '       </div>';
PROCESS_INFOBOX_TEMPLATE += '       <div class="col-md-2">';
PROCESS_INFOBOX_TEMPLATE += '           <input class="form-control btn btn-primary" type="hidden" id="process_save" value="Save">';
PROCESS_INFOBOX_TEMPLATE += '       </div>';
PROCESS_INFOBOX_TEMPLATE += '       <div class="col-md-2">';
PROCESS_INFOBOX_TEMPLATE += '           <input class="form-control" type="hidden" id="process_cancel" value="Cancel">';
PROCESS_INFOBOX_TEMPLATE += '       </div>';
PROCESS_INFOBOX_TEMPLATE += '   </div>';
PROCESS_INFOBOX_TEMPLATE += '   <div class="row process_row">';
PROCESS_INFOBOX_TEMPLATE += '       <div class="col-md-1">';
PROCESS_INFOBOX_TEMPLATE += '           <label for="process_name" style="text-align:left; float:left;" class="control-label">Name:</label>';
PROCESS_INFOBOX_TEMPLATE += '       </div>';
PROCESS_INFOBOX_TEMPLATE += '           <div class="col-md-11">';
PROCESS_INFOBOX_TEMPLATE += '               <input class="form-control process_input" id="process_name" type="text" value="%process_name%" disabled>';
PROCESS_INFOBOX_TEMPLATE += '           </div>';
PROCESS_INFOBOX_TEMPLATE += '   </div>';
PROCESS_INFOBOX_TEMPLATE += '   <div class="row process_row">';
PROCESS_INFOBOX_TEMPLATE += '       <div class="col-md-1">';
PROCESS_INFOBOX_TEMPLATE += '           <span><b>UUID:</b></span>';
PROCESS_INFOBOX_TEMPLATE += '       </div>';
PROCESS_INFOBOX_TEMPLATE += '       <div class="col-md-11">';
PROCESS_INFOBOX_TEMPLATE += '           <span>%process_uuid%</span>';
PROCESS_INFOBOX_TEMPLATE += '       </div>';
PROCESS_INFOBOX_TEMPLATE += '   </div>';
PROCESS_INFOBOX_TEMPLATE += '   <div class="row process_row">';
PROCESS_INFOBOX_TEMPLATE += '       <div class="col-md-12">';
PROCESS_INFOBOX_TEMPLATE += '           <label for="process_description" class="control-label">Description:</label>';
PROCESS_INFOBOX_TEMPLATE += '           <textarea class="form-control process_input" id="process_description" disabled>%process_description%</textarea>';
PROCESS_INFOBOX_TEMPLATE += '       </div>';
PROCESS_INFOBOX_TEMPLATE += '   </div>';
PROCESS_INFOBOX_TEMPLATE += '   <hr>Details';
PROCESS_INFOBOX_TEMPLATE += '   <div class="row process_row">';
PROCESS_INFOBOX_TEMPLATE += '       <div class="col-md-2"><label for="doc_creationDate" class="control-label">Creation Date:</label></div>';
PROCESS_INFOBOX_TEMPLATE += '       <div class="col-md-2"><input class="form-control process_input inputDetail" id="doc_creationDate" type="date" value="%doc_creationDate%" disabled></div>';
PROCESS_INFOBOX_TEMPLATE += '       <div class="col-md-2"><label for="doc_valid_from" class="control-label">Valid From:</label></div>';
PROCESS_INFOBOX_TEMPLATE += '       <div class="col-md-2"><input class="form-control process_input inputDetail" id="doc_valid_from" type="date" value="%doc_valid_from%" disabled></div>';
PROCESS_INFOBOX_TEMPLATE += '       <div class="col-md-2"><label for="doc_valid_until" class="control-label">Valid Until:</label></div>';
PROCESS_INFOBOX_TEMPLATE += '       <div class="col-md-2"><input class="form-control process_input inputDetail" id="doc_valid_until" type="date" value="%doc_valid_until%" disabled></div>';
PROCESS_INFOBOX_TEMPLATE += '   </div>';
PROCESS_INFOBOX_TEMPLATE += '   <div class="row process_row">';
PROCESS_INFOBOX_TEMPLATE += '       <div class="col-md-2"><label for="doc_geography" class="control-label">Geography:</label></div>';
PROCESS_INFOBOX_TEMPLATE += '       <div class="col-md-2"><input class="form-control process_input inputDetail" id="doc_geography" type="text" value="%doc_geography%" disabled></div>';
PROCESS_INFOBOX_TEMPLATE += '       <div class="col-md-2"><label for="doc_technology" class="control-label">Technology:</label></div>';
PROCESS_INFOBOX_TEMPLATE += '       <div class="col-md-2"><input class="form-control process_input inputDetail" id="doc_technology" type="text" value="%doc_technology%" disabled></div>';
PROCESS_INFOBOX_TEMPLATE += '       <div class="col-md-2"><label for="doc_time" class="control-label">Time:</label></div>';
PROCESS_INFOBOX_TEMPLATE += '       <div class="col-md-2"><input class="form-control process_input inputDetail" id="doc_time" type="text" value="%doc_time%" disabled></div>';
PROCESS_INFOBOX_TEMPLATE += '   </div>';
PROCESS_INFOBOX_TEMPLATE += '   <div class="row process_row">';
PROCESS_INFOBOX_TEMPLATE += '       <div class="col-md-2"><label for="doc_modeling_constant" class="control-label">Modeling Constant:</label></div>';
PROCESS_INFOBOX_TEMPLATE += '       <div class="col-md-2"><input class="form-control process_input inputDetail" id="doc_modeling_constant" type="text" value="%doc_modeling_constant%" disabled></div>';
PROCESS_INFOBOX_TEMPLATE += '       <div class="col-md-2"><label for="doc_data_treatment" class="control-label">Data Treatment:</label></div>';
PROCESS_INFOBOX_TEMPLATE += '       <div class="col-md-2"><input class="form-control process_input inputDetail" id="doc_data_treatment" type="text" value="%doc_data_treatment%" disabled></div>';
PROCESS_INFOBOX_TEMPLATE += '       <div class="col-md-2"><label for="doc_sampling" class="control-label">Sampling:</label></div>';
PROCESS_INFOBOX_TEMPLATE += '       <div class="col-md-2"><input class="form-control process_input inputDetail" id="doc_sampling" type="text" value="%doc_sampling%" disabled></div>';
PROCESS_INFOBOX_TEMPLATE += '   </div>';
PROCESS_INFOBOX_TEMPLATE += '   <div class="row process_row">';
PROCESS_INFOBOX_TEMPLATE += '       <div class="col-md-2"><label for="doc_copyright" class="control-label">Copyright:</label></div>';
PROCESS_INFOBOX_TEMPLATE += '       <div class="col-md-2"><input class="form-control process_input inputDetail" id="doc_copyright" type="text" value="%doc_copyright%" disabled></div>';
PROCESS_INFOBOX_TEMPLATE += '       <div class="col-md-2"><label for="doc_location_code" class="control-label">Location:</label></div>';
PROCESS_INFOBOX_TEMPLATE += '       <div class="col-md-2"><input class="form-control process_input inputDetail" id="doc_location_code" type="text" value="%doc_location_code%" disabled></div>';
PROCESS_INFOBOX_TEMPLATE += '       <div class="col-md-2"><label for="btnFlowProperty" class="control-label">Flow properties:</label></div>';
PROCESS_INFOBOX_TEMPLATE += '       <div class="col-md-2"><input id="btnFlowProperty" type="button" class="form-control btn btn-default" onclick="showFlowProperty();" value="View..."/></div>';
PROCESS_INFOBOX_TEMPLATE += '   </div>';
PROCESS_INFOBOX_TEMPLATE += '</form>';

// template for the exchange tab header, including the add-exchange form
var EXCHANGE_HEADER_INPUT = "";
EXCHANGE_HEADER_INPUT += '<div class="row">';
EXCHANGE_HEADER_INPUT += '   <div class="col-md-10">';
EXCHANGE_HEADER_INPUT += '      <h1 class="noPadding noMargin">Inputs</h1>';
EXCHANGE_HEADER_INPUT += '   </div>';
EXCHANGE_HEADER_INPUT += '   <div class="col-md-2">';
EXCHANGE_HEADER_INPUT += '      <div class="button_row">';
EXCHANGE_HEADER_INPUT += '          <div id="btnInput">';
EXCHANGE_HEADER_INPUT += '              <button  type="button" class="form-control btn btn-primary" data-toggle="modal" data-target="#newExchangeModalForm">Add Exchange...</button>';
EXCHANGE_HEADER_INPUT += '          </div>';
EXCHANGE_HEADER_INPUT += '      </div>';
EXCHANGE_HEADER_INPUT += '   </div>';
EXCHANGE_HEADER_INPUT += '</div>';

var EXCHANGE_HEADER_OUTPUT = "";
EXCHANGE_HEADER_OUTPUT += '<div class="row">';
EXCHANGE_HEADER_OUTPUT += ' <div class="col-md-10">';
EXCHANGE_HEADER_OUTPUT += '      <h1 class="noPadding noMargin">Outputs</h1>';
EXCHANGE_HEADER_OUTPUT += ' </div>';
EXCHANGE_HEADER_OUTPUT += ' <div class="col-md-2">';
EXCHANGE_HEADER_OUTPUT += '     <div class="button_row">';
EXCHANGE_HEADER_OUTPUT += '         <div id="btnOutput">';
EXCHANGE_HEADER_OUTPUT += '             <button  type="button" class="form-control btn btn-primary" data-toggle="modal" data-target="#newExchangeModalForm">Add Exchange...</button>';
EXCHANGE_HEADER_OUTPUT += '         </div>';
EXCHANGE_HEADER_OUTPUT += '     </div>';
EXCHANGE_HEADER_OUTPUT += ' </div>';
EXCHANGE_HEADER_OUTPUT += '</div>';

var MODAL_EXCHANGE = "";
MODAL_EXCHANGE += '<div class="modal fade" id="newExchangeModalForm" role="dialog">';
MODAL_EXCHANGE += ' <div class="modal-dialog">';
MODAL_EXCHANGE += '     <div class="modal-content">';
MODAL_EXCHANGE += '         <div class="modal-header">';
MODAL_EXCHANGE += '             <button type="button" class="close" data-dismiss="modal">&times;</button>';
MODAL_EXCHANGE += '             <h4 class="modal-title">New Exchange</h4>';
MODAL_EXCHANGE += '         </div>';
MODAL_EXCHANGE += '         <div class="modal-body form-horizontal">';
MODAL_EXCHANGE += '             <input type="hidden" id="newex_processId" />';
MODAL_EXCHANGE += '             <div class="row" id="newex_alert"></div>';
MODAL_EXCHANGE += '             <div class="row">';
MODAL_EXCHANGE += '                 <div class="col-md-12">';
MODAL_EXCHANGE += '                     <label>Flow Type</label>';
MODAL_EXCHANGE += '                     <select id="newex_flowtype" class="form-control">';
MODAL_EXCHANGE += '                         <option value="ALL">All</option>';
MODAL_EXCHANGE += '                         <option value="ELEMENTARY_FLOW">Elementary</option>';
MODAL_EXCHANGE += '                         <option value="PRODUCT_FLOW" selected>Product</option>';
MODAL_EXCHANGE += '                     </select>';
MODAL_EXCHANGE += '                 </div>';
MODAL_EXCHANGE += '             </div>';
MODAL_EXCHANGE += '             <div class="row">';
MODAL_EXCHANGE += '                 <div class="col-md-12">';
MODAL_EXCHANGE += '                 <label>Flow</label>';
MODAL_EXCHANGE += '                 <div class="input-group">';
MODAL_EXCHANGE += '                     <input type="text" class="form-control" placeholder="Search flows..." id="newex_flowsearchtext">';
MODAL_EXCHANGE += '                     <div class="input-group-btn">';
MODAL_EXCHANGE += '                         <button class="btn btn-default" id="newex_flowsearchbtn" type="submit"><i class="glyphicon glyphicon-search"></i></button>';
MODAL_EXCHANGE += '                     </div>';
MODAL_EXCHANGE += '                 </div>';
MODAL_EXCHANGE += '                 </div>';
MODAL_EXCHANGE += '             </div>';
MODAL_EXCHANGE += '             <div class="row">';
MODAL_EXCHANGE += '                 <div class="col-md-12">';
MODAL_EXCHANGE += '                     <select id="newex_flow" class="form-control"></select>';
MODAL_EXCHANGE += '                 </div>';
MODAL_EXCHANGE += '             </div>';
MODAL_EXCHANGE += '             <div class="row hideElement">';
MODAL_EXCHANGE += '                 <div class="col-md-12"><input type="checkbox" id="newex_isinput" data-value="input" class="form-control"/></div>';
MODAL_EXCHANGE += '             </div>';
MODAL_EXCHANGE += '             <div class="row">';
MODAL_EXCHANGE += '                 <div class="col-md-12">';
MODAL_EXCHANGE += '                     <label>Unit</label>';
MODAL_EXCHANGE += '                     <select id="newex_unit" class="form-control">%unitOptions%</select>';
MODAL_EXCHANGE += '                 </div>';
MODAL_EXCHANGE += '             </div>';
MODAL_EXCHANGE += '             <div class="row hideElement">';
MODAL_EXCHANGE += '                     <div class="col-md-12"><input type="number" id="newex_amount" value="1" class="form-control"/></div>';
MODAL_EXCHANGE += '                 </div>';
MODAL_EXCHANGE += '             </div>';
MODAL_EXCHANGE += '             <div class="modal-footer">';
MODAL_EXCHANGE += '                 <button type="button" id="newex_done" class="btn btn-primary">Done</button>';
MODAL_EXCHANGE += '                 <button type="button" id="newex_cancel" class="btn btn-default" data-dismiss="modal">Cancel</button>';
MODAL_EXCHANGE += '             </div>';
MODAL_EXCHANGE += '         </div>';
MODAL_EXCHANGE += '     </div>';
MODAL_EXCHANGE += ' </div>';
MODAL_EXCHANGE += '</div>';

// template for the exchanges table body (both input/output)
var EXCHANGE_TABLE_TEMPLATE = "";
EXCHANGE_TABLE_TEMPLATE += '<div class="row">';
EXCHANGE_TABLE_TEMPLATE += '    <table class="table table-bordered">';
EXCHANGE_TABLE_TEMPLATE += '        <thead>';
EXCHANGE_TABLE_TEMPLATE += '            <tr>';
EXCHANGE_TABLE_TEMPLATE += '                <th class="button-column"></th>';
EXCHANGE_TABLE_TEMPLATE += '                <th class="col-md-5">Flow</th>';
EXCHANGE_TABLE_TEMPLATE += '                <th class="col-md-1">Amount</th>';
EXCHANGE_TABLE_TEMPLATE += '                <th class="col-md-1">Unit</th>';
EXCHANGE_TABLE_TEMPLATE += '                <th class="col-md-2">Formula</th>';
EXCHANGE_TABLE_TEMPLATE += '                <th class="col-md-2">Provider</th>';
EXCHANGE_TABLE_TEMPLATE += '            </tr>';
EXCHANGE_TABLE_TEMPLATE += '        </thead>';
EXCHANGE_TABLE_TEMPLATE += '        <tbody id="%tbody_id"></tbody>';
EXCHANGE_TABLE_TEMPLATE += '    </table>';
EXCHANGE_TABLE_TEMPLATE += '</div>';
// template for a row in the exchanges table body
var EXCHANGE_ROW_TEMPLATE = "";
EXCHANGE_ROW_TEMPLATE += '<tr>';
EXCHANGE_ROW_TEMPLATE += '  <td>';
EXCHANGE_ROW_TEMPLATE += '      <button class="btn btn-default deleteExchange" id="exch_%id">';
EXCHANGE_ROW_TEMPLATE += '          <span class="glyphicon glyphicon-remove" style="color: red; position: relative; top: 2px;"></span>';
EXCHANGE_ROW_TEMPLATE += '      </button>';
EXCHANGE_ROW_TEMPLATE += '  </td>';
EXCHANGE_ROW_TEMPLATE += '  <td>%flow</td>';
EXCHANGE_ROW_TEMPLATE += '  <td>%amount</td>';
EXCHANGE_ROW_TEMPLATE += '  <td>%unit</td>';
EXCHANGE_ROW_TEMPLATE += '  <td><a class="btnFormula" href="#">%formula</a></td>';
EXCHANGE_ROW_TEMPLATE += '  <td>';
EXCHANGE_ROW_TEMPLATE += '      <select class="form-control providerSelect" id="exch_%id">';
EXCHANGE_ROW_TEMPLATE += '      </select>';
EXCHANGE_ROW_TEMPLATE += '  </td>';
EXCHANGE_ROW_TEMPLATE += '</tr>';

// template for the exchange tab footer
var EXCHANGE_FOOTER = '</div>';//closes container div



// template for the parameters table body
var PARAM_TABLE_TEMPLATE = '';
PARAM_TABLE_TEMPLATE += '<div class="row button_row">';
PARAM_TABLE_TEMPLATE += '   <div id="parameter_button_spacer" class="col-md-10"></div>';
PARAM_TABLE_TEMPLATE += '   <div class="col-md-2">';
PARAM_TABLE_TEMPLATE += '       <input class="form-control btn btn-primary" type="button" id="parameter_edit" value="Edit"/>';
PARAM_TABLE_TEMPLATE += '   </div>';
PARAM_TABLE_TEMPLATE += '</div>';
PARAM_TABLE_TEMPLATE += '<table class="table table-bordered" id="table_local_parameter_edit">';
PARAM_TABLE_TEMPLATE += '   <thead>';
PARAM_TABLE_TEMPLATE += '       <tr>';
PARAM_TABLE_TEMPLATE += '           <th class="col-md-1">Name</th>';
PARAM_TABLE_TEMPLATE += '           <th class="col-md-4">Value</th>';
PARAM_TABLE_TEMPLATE += '           <th class="col-md-4">Description</th>';
PARAM_TABLE_TEMPLATE += '           <th class="col-md-4">Formula</th>';
PARAM_TABLE_TEMPLATE += '       </tr>';
PARAM_TABLE_TEMPLATE += '   </thead>';
PARAM_TABLE_TEMPLATE += '   <tbody id="param_tbody"></tbody>';
PARAM_TABLE_TEMPLATE += '</table>';

var PARAM_TABLE_TEMPLATE_EDIT = '';
PARAM_TABLE_TEMPLATE_EDIT += '<div class="row button_row">';
PARAM_TABLE_TEMPLATE_EDIT += '  <div id="parameter_button_spacer" class="col-md-8"></div>';
PARAM_TABLE_TEMPLATE_EDIT += '  <div class="col-md-2"><input class="form-control" type="button" id="parameter_save" value="Save"/> </div>';
PARAM_TABLE_TEMPLATE_EDIT += '  <div class="col-md-2"><input class="form-control" type="button" id="parameter_cancel" value="Cancel"/> </div>';
PARAM_TABLE_TEMPLATE_EDIT += '</div>';
PARAM_TABLE_TEMPLATE_EDIT += '<table class="table table-bordered" id="table_local_parameter">';
PARAM_TABLE_TEMPLATE_EDIT += '  <thead>';
PARAM_TABLE_TEMPLATE_EDIT += '      <tr>';
PARAM_TABLE_TEMPLATE_EDIT += '          <th class="col-md-1">Name</th>';
PARAM_TABLE_TEMPLATE_EDIT += '          <th class="col-md-4">Value</th>';
PARAM_TABLE_TEMPLATE_EDIT += '          <th class="col-md-4">Description</th>';
PARAM_TABLE_TEMPLATE_EDIT += '          <th class="col-md-4">Formula</th>';
PARAM_TABLE_TEMPLATE_EDIT += '      </tr>';
PARAM_TABLE_TEMPLATE_EDIT += '  </thead>';
PARAM_TABLE_TEMPLATE_EDIT += '  <tbody id="param_tbody"></tbody>';
PARAM_TABLE_TEMPLATE_EDIT += '</table>';
// template for a row in the parameters table
var PARAM_ROW_TEMPLATE = '<tr><td>%name</td><td>%value</td><td>%desc</td><td>%formula</td></tr>';

var PARAM_ROW_TEMPLATE_EDIT = "";
PARAM_ROW_TEMPLATE_EDIT += '<tr>';
PARAM_ROW_TEMPLATE_EDIT += '    <td>%name</td>';
PARAM_ROW_TEMPLATE_EDIT += '    <td>';
PARAM_ROW_TEMPLATE_EDIT += '        <input class="form-control parameter_input" id="parameter_value_%id" type="text" value="%value">';
PARAM_ROW_TEMPLATE_EDIT += '    </td>';
PARAM_ROW_TEMPLATE_EDIT += '    <td>%desc</td>';
PARAM_ROW_TEMPLATE_EDIT += '    <td>';
PARAM_ROW_TEMPLATE_EDIT += '        <input class="form-control parameter_input" id="parameter_formula" type="text" value="%formula">';
PARAM_ROW_TEMPLATE_EDIT += '    </td>';
PARAM_ROW_TEMPLATE_EDIT += '</tr>';

// template for the global parameters table body
var GLOBAL_PARAM_TABLE_TEMPLATE = "";
GLOBAL_PARAM_TABLE_TEMPLATE += '<table class="table table-bordered">';
GLOBAL_PARAM_TABLE_TEMPLATE += '    <thead>';
GLOBAL_PARAM_TABLE_TEMPLATE += '        <tr>';
GLOBAL_PARAM_TABLE_TEMPLATE += '            <th class="col-md-1">Name</th>';
GLOBAL_PARAM_TABLE_TEMPLATE += '            <th class="col-md-4">Value</th>';
GLOBAL_PARAM_TABLE_TEMPLATE += '            <th class="col-md-4">Description</th>';
GLOBAL_PARAM_TABLE_TEMPLATE += '            <th class="col-md-4">Formula</th>';
GLOBAL_PARAM_TABLE_TEMPLATE += '        </tr>';
GLOBAL_PARAM_TABLE_TEMPLATE += '    </thead>';
GLOBAL_PARAM_TABLE_TEMPLATE += '    <tbody id="global_param_tbody"></tbody>';
GLOBAL_PARAM_TABLE_TEMPLATE += '</table>';


// template for a row in the global parameters table
var GLOBAL_PARAM_ROW_TEMPLATE = '<tr><td>%name</td><td>%value</td><td>%desc</td><td>%formula</td></tr>';


// generic template for an option
var SELECT_OPTION_TEMPLATE = '<option value="%val" %attr>%text</option>';

// generic template for an alert
var ALERT_TEMPLATE = "";
ALERT_TEMPLATE += '<div class="alert alert-warning alert-dismissible">';
ALERT_TEMPLATE += ' <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>';
ALERT_TEMPLATE += ' %msg%';
ALERT_TEMPLATE += '</div>';
getUrlAjax = function(url) {
    return $.ajax({
        type: "GET",
        url: url,
        contentType: 'application/json',
        timeout: 120000
    });
};

getAjaxFromRouter = function(router, data) {
    if(data == null || data == undefined)
        return $.ajax({
            type: router.type,
            url: router.url,
            contentType: 'application/json; charset=utf-8'
        });
    else
        return $.ajax({
            type: router.type,
            url: router.url,
            contentType: 'application/json; charset=utf-8',
            data: JSON.stringify(data),
            dataType: 'json'
        });
};


getAjaxWithData = function(ajaxUrl, data) {
    return $.ajax({
        type: 'GET',
        url: ajaxUrl,
        contentType: 'application/json; charset=utf-8',
        data: JSON.stringify(data),
        dataType: 'json'
    });
};

getGroupUnitsAjax = function(flowId) {
    return $.ajax(jsModel.controllers.API.model.Flow.getGroupUnits(flowId));
};

getApiAjax = function() {
    return $.ajax(jsAPI.controllers.API.Service.info());
};

getModelsAjax = function() {
    return $.ajax(jsModel.controllers.API.model.BIMModel.all());
};

getResultSetsAjax = function() {
    return $.ajax(jsModel.controllers.API.model.ResultSets.all());
};

getImpactMethodsAjax = function() {
    return $.ajax(jsAPI.controllers.API.Spark.getAllImpactMethod());
};

getModelUrl = function(modelId) {
    return jsModel.controllers.API.model.BIMModel.getInfo(modelId).url;
};

getProcessUrl = function(id) {
    return jsModel.controllers.API.model.Process.getInfo(id).url;
};

getExchangeUrl = function(id) {
    return jsModel.controllers.API.model.Exchange.getInfo(id).url;
};

/**
 * Contains methods to interact with the jsTree
 */


/**
 * @summary Recieves a node object, extracts the URL element from it, revives the process object,
 * then for each exchange, fetches the producing process and adds it to the childrenprocesses array.
 * The 'callback' method is then called with the sorted childrenProcesses array.
 * This method is called on-demand by jstree (for lazy-loading), it's responsibility is to build an array of children objects then to send it via the callback method.
 *
 * @param node The jstree-format node
 * @param callback The jstree method to call with the array of jstree-compliant objects. The array can be empty or "true" (to cancel loading). Callback can also be called multiple times, jstree will replace the existing data with the new data .
 */
getChildrenOfNode = function(node, callback) {
    let childrenProcesses = [];

    //build an array of processes that appear before a children
    //used to not have infinite chains, a process stops becoming expandable once it has occured twice in the chain
    let tree = $("#tree");
    let parentChain = [node.original.processUrl];
    for(let parentId of node.parents) {
        if(parentId == '#') continue;
        let aParentNode = getTreeNode(parentId);
        parentChain.push(aParentNode.original.processUrl);
    }

    //revive the process
    let processUrl = node.original.providerUrl;
    $.when(parentChain, getUrlAjax(processUrl)).done(function(parentChain, procJson) {

        //get the exchanges of this process
        let process = procJson[0];
        $.when(parentChain, getUrlAjax(process.exchanges)).done(function(parentChain, exchangesAjax) {
            let exchanges = exchangesAjax[0].lst;

            //let params contains N producer ajax queries and the parentChain at the end
            var params = [];

            for (let exchange of exchanges) {
                if (exchange.isInput) {//specs: only consider input
                    //pairing...
                    params.push(getUrlAjax(exchange.flow));
                    params.push(exchange);
                }
            }

            params.push(parentChain);

            //get the flowType from the flow object to determine if the exchange has a provider
            $.when.apply($, params).then(function(){
                if (arguments.length == 1) {//leaf
                    callback.call(this, []);
                    return;
                }

                //removes parentChain from arguments, leaving only the providers
                var parentChain = arguments[arguments.length-1];
                delete arguments[arguments.length-1];
                arguments.length = arguments.length-1;//delete doesnt update length, and for(...of...) uses length
                let params = [];
                for(let i = 0; i < arguments.length; i=i+2) {
                    let exchange = arguments[i+1];
                    let flow = arguments[i][0];
                    exchange.flowType = flow.flowType;//add flowType data to exchange object

                    if(exchange.flowType == "ELEMENTARY_FLOW") {//elementary flows have no providers
                        //No Elementary Flows in the tree
                        //params.push(flow.name);
                        //params.push(exchange);
                    }
                    else {
                        params.push(getUrlAjax(exchange.provider));
                        params.push(exchange);
                    }
                }

                params.push(parentChain);

                //get the provider information
                $.when.apply($, params).then(function () {
                    if (arguments.length == 1) {//leaf
                        callback.call(this, []);
                        return;
                    }
                    //removes parentChain from arguments, leaving only the providers
                    var parentChain = arguments[arguments.length-1];
                    delete arguments[arguments.length-1];
                    arguments.length = arguments.length-1;//delete doesnt update length, and for(...of...) uses length
                    var exchangeProviderPairs = [];
                    
                    for(let i = 0; i < arguments.length; i=i+2) {
                        let isLeaf = typeof(arguments[i]) == "string";//== elementary flow
                        let provider = isLeaf ? arguments[i] : arguments[i][0];
                        exchangeProviderPairs.push( {'provider' : provider, 'exchange' : arguments[i+1], 'isLeaf' : isLeaf} );
                    }


                    for (let epp of exchangeProviderPairs) {

                        let provider    = epp.provider;
                        let exchange    = epp.exchange;
                        let isLeaf      = epp.isLeaf;
                        let processUrl  = isLeaf ? "" : getProcessUrl(provider.id);
                        let processName = isLeaf ? provider : provider.name;
                        let exchangeUrl = getExchangeUrl(exchange.id);
                        let hasChildren = !parentChain.includes(processUrl) && !isLeaf;

                        let nodeType    = '';

                        let entityType=findBimProcess(provider.id);
                        if(entityType!=null)
                            nodeType=entityType.BIMEntityType;

                        addNodeToArray(childrenProcesses, processUrl, exchangeUrl, exchange.provider, processName, hasChildren, nodeType,provider.id);

                        //only have children if this node is not the same as it's parent (root '#' always has children)


                    }
                    
                    childrenProcesses.sort(function (a, b) {
                        if (a.text < b.text) {
                            return -1;//lt
                        }
                        else if (a.text > b.text) {
                            return 1;//gt
                        }
                        else {
                            return 0;//eq
                        }
                    });
                    callback.call(this, childrenProcesses);
                },
                function () {
                    var args = arguments;
                    console.error("promise broken");
                });

            });


        });
    });//end of getProcessFromIdAjax
};

/**
 * @summary Adds a jstree-compliant node to the array.
 * @param array The array that has the node added to it
 * @param id The process ID to be used when creating the process URL (for 'reviving' the process from the REST service)
 * @param text The string displayed on the jstree node
 * @returns {number} Returns the index of the newly added node (always length-1)
 */
addNodeToArray = function(array, processUrl, exchangeUrl, providerUrl,text, hasChildren, nodeType,idprovider) {
    return array.push(createNodeJson(text, providerUrl, processUrl, hasChildren, nodeType,idprovider)) - 1;//push returns length, -1 makes it the index
};

createNodeJson = function(text, providerUrl, processUrl, hasChildren, nodeType,idprovider) {
    return {
        "text"          : text+getNotLinkedFlag(idprovider),
        "providerUrl"   : providerUrl,
        "processUrl"    : processUrl,
        "processId"    : idprovider,
        "type"          : nodeType,
        "children"      : hasChildren
    };
}

/**
 * @summary Initializes the jstree element with the desired configuration and event handlers. Must be called before using updateTreeData
 */
initTree = function() {

    //bind jstree instance to #tree
    var tree = $("#tree");
    tree.jstree({
        core : {
            data : null,
            check_callback : true
        },
        types : {
            default : { "icon" : "fa fa-file" },
            BIMUniformat : { "icon" : "fa fa-folder" },
            BIMObject : { "icon" : "fa fa-cube" },
            BIMObjectType : { "icon" : "fa fa-cubes" },
            BIMMaterial : { "icon" : "fa fa-files-o" },
            BIMRoot : { "icon" : "fa fa-building" }
        },
        contextmenu : { "items" : customMenu },
        plugins : ["types","contextmenu"]
    });

    //setup on-click to show process details
    tree.on("select_node.jstree", function (e, data) {
        let url = data.node.original.providerUrl;

        if(url == undefined) {
            console.error("Selected a node with no provider URL!");
            return;
        }

        //position returns relative to viewport, so if posleft=100, scrollleft(100), posleft becomes 0
        //but scroll requires absolute value, so need to use current scroll position
        let sidebar = $(".client-sidebar");
        let pos = $("#" + data.node.id).position();
        let scrolled = sidebar.scrollLeft();
        sidebar.scrollLeft(scrolled + pos.left);

        displayProcess(url);
    });
};

/**
 * @summary Gets the root process of the desired model id and initializes the jstree data with it.
 * @param modelId The desired model id number. If it is invalid (negative or undef), clears the tree
 * @param resultSetId
 */
fetchRoot = function(modelId, resultSetId) {

    //bad id, alert with error and clear tree
    if(modelId == undefined || modelId < 0) {
        updateTreeData(null);
        return;
    }
    getUrlAjax(getModelUrl(modelId)).done(function(modelJson) {
        let model = modelJson;
        let url = model.idRootProcess;

        if(resultSetId !== undefined && resultSetId > 0){
            url = url + '/history/' + resultSetId;
        }

        $.when(model.idRootProcess, getUrlAjax(url)).done(function(processUrl, processJson) {
            let rootProcess = processJson[0];
            //update and refresh the tree
            var data = function(node, callback) {
                if(node.id =="#") {
                    callback.call(this, [{
                        "text"          : rootProcess.name+getNotLinkedFlag(rootProcess.id),
                        "providerUrl"   : processUrl,
                        "processUrl"    : processUrl,
                        "processId"     : rootProcess.id,
                        "type"          : "BIMRoot",
                        "children"      : true,
                        "state"         : "leaf" }]
                    );
                }
                else {
                    getChildrenOfNode(node, callback);
                }
            };
            updateTreeData(data);
        });

    });
};

function getNotLinkedFlag(idProcess)
{
    return idsNotLinked.lst.indexOf(idProcess)<0?"":" <i id=\"NotLinked_"+idProcess+"\" style=\"color:red\" class=\"fa fa-question-circle\" aria-hidden=\"true\"></i>";
}

/**
 * @summary Function that handles the access to the jstree data. Can be used to clear the tree.
 * @param data If undefined, clears the tree, otherwise inserts the object into the tree and refreshes it
 */
updateTreeData = function(data) {
    if(data == null || data == undefined) {
        data = false;
    }
    var tree = $("#tree");
    tree.jstree(true).settings.core.data = data;
    tree.jstree(true).refresh(false, true);//skip_loading, forget_state
};

getRootNode = function() {
    return getTreeNode("#");
};

getTreeNode = function(nodeId) {
    return $("#tree").jstree(true).get_node(nodeId, false);
};

deleteTreeNode = function(nodeId) {
    return $("#tree").jstree(true).delete_node(nodeId);
};

createTreeNode = function(parent, data) {
    return $("#tree").jstree('create_node', parent, data, 'last');
}

function customMenu(node)
{
    var viewInRevit = node.type=="BIMObject";
    var items = {
        viewInRevit : {
            label:"View in Revit",
            action: function(node){
                var i=$(node);
                var k=0;

            },
            "_disabled":!viewInRevit
        }
    }
    return items;
}


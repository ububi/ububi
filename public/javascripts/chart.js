var requestHistory = [];
var graphData = [];
var graphIndex = 0;
var graphTitle = "UBUBI - Drill-down chart";
var modelName = $("#hidProjectName").val();
var modelId = $("#hidProjectId").val();
var categoryList = [];
var isInitDone = false;

//get result sets
getAjaxFromRouter(
    jsModel.controllers.API.model.ResultSets.getResultSetFromModelId(modelId),""
).done(function(response){
    setResultSetsList(response)
}).fail(function(error) {
    console.log("failed!" + error);
    throw new Error("ERROR");
});

// Set the result sets list for the model
function setResultSetsList(data){
    var listSize = data.lst.length;
    for (i = 0; i < listSize; i++) {
        $("#selResults").append($("<option>", {
            value: data.lst[i].id,
            text: data.lst[i].description
        }));
    }
}

//get base data by set
function getBaseDataBySetId(id){
    getAjaxFromRouter(
        jsModel.controllers.API.model.Results.getData(id),""
    ).done(function(response){

        initCategory(response);
        //setDataProvider(0);
        getSubElements(response.lst[0].processId);
        refreshChart();
    }).fail(function(error) {

        chartdiv.innerHTML = "ERROR";
        console.log("failed!" + error);
        throw new Error("ERROR");
    });
}


function initCategory(data){

    var listSize = data.lst.length;
    for (i = 0; i < listSize; i++) {

        categoryList.push(data.lst[i].impactCategoryName);
    }
}

function getSubElements(id){
    getAjaxFromRouter(

        jsModel.controllers.API.model.Results.getDataByNodeId(id, $("#selResults option:selected").val()), ""
    ).done(function (response) {

        if (isInitDone) {
            setGraphIndex(getGraphIndex() + 1);
        }
        else {
            isInitDone = true;
        }
        prepareDataHierarchy(response);
        setDataProvider(getGraphIndex());
        refreshChart();

    }).fail(function(error){
        console.log("drill down failed!" + error);
    });
}

// Generate a list of root Impact categories
function prepareDataHierarchy(data){

    var structure = [];
    var numCat = categoryList.length;
    var listSize = data.lst.length;
    var dataPerElement = listSize/numCat;
    var needNewGraph = true;

    chart.graphs = [];

    var startPoint = 0;
    for (i = 0; i < numCat; i++) {
        var category = {
            "impactCat": categoryList[i]
        };


        for (j = 0; j < dataPerElement; j++) {
            category["Name" + j] = data.lst[startPoint + j].processName;
            category["Qty" + j] = data.lst[startPoint + j].qty.toFixed(6);
            category[data.lst[startPoint + j].processName] = data.lst[startPoint + j].percent;
            category["Variance"+j] = data.lst[startPoint + j].uncertainty.toFixed(6);
            category["Unit"] = data.lst[startPoint + j].referenceUnit;

            if (needNewGraph) {
                var graph = new AmCharts.AmGraph();
                graph.id = data.lst[startPoint + j].processId;
                graph.type = "column";
                graph.fillAlphas = 0.9;
                graph.lineAlpha = 0.2;
                graph.title = data.lst[startPoint + j].processName;
                graph.balloonText = "<b>Quatity</b>: [[Qty" + j + "]]" + " [[Unit]]\n" +
                                    "<b>Percent</b>: [[percents]]%\n"+
                                    "<b>Variance</b>: [[Variance"+j+"]]\n";
                graph.valueField = data.lst[startPoint + j].processName;
                chart.addGraph(graph);
            }
        }
        needNewGraph = false;
        startPoint += dataPerElement;
        structure.push(category);
    }

    setRequestHistory(data);
    setGraphData(structure);
}

//Creation of the chart object
chart = AmCharts.makeChart("chartdiv", {
    "type": "serial",
    "theme": "light",
    "marginRight": 70,
    "titles": [
        {
            "id": "Title-1",
            "size": 15,
            "text": graphTitle
        }
    ],
    "rotate": true,
    "valueAxes": [
        {
            "axisAlpha": 0,
            "id": "ValueAxis-1",
            "stackType": "100%",
        }
    ],
    "startDuration": 1,
    "categoryField": "impactCat",
    "categoryAxis": {
        "gridPosition": "middle",
        "labelRotation": 45
    },
    "export": {
        "enabled": true
    },
    "legend": {
        "enabled": true,
        "useGraphSettings": true,
        "valueText": "[[percents]]%",
        "valueWidth": 80,
        "switchable": true
    }
});

// event listener to drill down
// inspired by https://www.amcharts.com/kbase/drill-column-chart/
chart.addListener("clickGraphItem", function(event) {
    // update the chart title
    chart.titles[0].text = chart.titles[0].text + "/" + event.graph.title;
    getSubElements(event.item.graph.id);

    // Label to beginning
    event.chart.addLabel(
        30, 8,
        "<< To Beginning",
        undefined,
        15,
        undefined,
        undefined,
        undefined,
        true,
        'javascript:resetChart();');

    // Label go back
    event.chart.addLabel(
        40, 25,
        "< Go Back",
        undefined,
        15,
        undefined,
        undefined,
        undefined,
        true,
        'javascript:goBack();');
});

function refreshChart() {
    chart.validateData();
    chart.animateAgain();
    chart.invalidateSize();
}

// function which resets the chart back to base data
// taken from https://www.amcharts.com/kbase/drill-column-chart/
function resetChart() {
    setGraphIndex(0);
    setDataProvider(0);
    chart.titles[0].text = graphTitle;
    chart.graphs = [];
    // remove the labels
    chart.allLabels = [];
    prepareDataHierarchy(requestHistory[getGraphIndex()]);
}

function goBack() {

    if (getGraphIndex() == 1) {
        resetChart();
    } else {
        setGraphIndex(getGraphIndex() - 1);
        setDataProvider(getGraphIndex());
        var titleSplit = chart.titles[0].text.split("/");
        var newtitle = "";
        for (x = 0; x <= getGraphIndex(); x++) {
            newtitle = newtitle + titleSplit[x];
            if (x != getGraphIndex()){
                newtitle = newtitle + "/";
            }
        }
        chart.titles[0].text = newtitle;
        prepareDataHierarchy(requestHistory[getGraphIndex()]);
    }
}

function setDataProvider(requestedIndex){
    chart.dataProvider = graphData[requestedIndex];
}

function setGraphData(data) {
    graphData[getGraphIndex()] = data;
}

function setRequestHistory(data) {
    requestHistory[getGraphIndex()] = data;
}

function setGraphIndex(newIndex){
    graphIndex = newIndex;
}

function getGraphIndex(){
    return graphIndex;
}

$("#selResults").change(function() {
    var visibility = "visible";
    if($("#selResults").val()==-1){
        $("#btnDownload").css("visibility", "hidden");
        $("#chartdiv").css("display", "none");
        $("#baseCategories").css("display", "none");
    }
    else
    {
        $("#btnDownload").css("visibility", "visible");
        $("#chartdiv").css("display", "block");
        chart.validateSize();
        graphTitle = modelName + " - " + $("#selResults option:selected").text();
        getBaseDataBySetId($("#selResults option:selected").val())
        resetChart();
    }
});

$("#btnClose").click(function(){
    window.close();
});

$("#btnDownload").click(function(){
    var idReport = $("#selResults").val();
    if(idReport!=null && idReport > 0)
        window.location='/api/Spark/GetExcelFile/'+idReport;
});
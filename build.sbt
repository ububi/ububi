import AssemblyKeys._
import sbtassembly.Plugin._
import sbt._
import Keys._
import sbtbuildinfo.Plugin._

resolvers += "Github Project" at "https://github.com/mathieu55/IEDManager/raw/master/release/"
resolvers += "scalaz-bintray" at "https://dl.bintray.com/scalaz/releases"

name := "UBUBI"
version := "0.0.7"
scalaVersion := "2.11.7"

buildInfoSettings
sourceGenerators in Compile <+= buildInfo
buildInfoKeys := Seq[BuildInfoKey](name, version, scalaVersion, sbtVersion)
buildInfoPackage := "org.ububiGroup.ububi"

lazy val main = (project in file(".")).enablePlugins(PlayJava, PlayScala)

javacOptions ++= Seq("-source", "1.8", "-target", "1.8")

unmanagedSourceDirectories in Compile <+= baseDirectory ( _ /"OLCA")
unmanagedResourceDirectories in Test <+=  baseDirectory ( _ /"target/web/public/test" )

libraryDependencies ++= Seq(
  javaJpa,
  javaJdbc,
  cache,
  javaWs,
  "mysql" % "mysql-connector-java" % "5.1.36",
  "org.hibernate" % "hibernate-core" % "5.2.7.Final",
  "org.hibernate" % "hibernate-entitymanager" % "5.2.7.Final",
  "org.hibernate.javax.persistence" % "hibernate-jpa-2.1-api" % "1.0.0.Final",
  "org.ububiGroup" % "IEDManager" % "0.0.2",
  "org.webjars" %% "webjars-play" % "2.5.0", //TODO set webjars with webjars controller
  "org.webjars" % "bootstrap" % "3.3.4",
  "org.webjars" % "angularjs" % "1.3.15",
  "org.webjars" % "angular-ui-bootstrap" % "0.13.0",
  "org.webjars" % "angular-tree-control" % "0.2.22",
  "org.webjars" % "jstree" % "3.2.1",
  "org.webjars" % "font-awesome" % "4.7.0",
  "dom4j" % "dom4j" % "1.6.1",
  "com.jcraft" % "jsch" % "0.1.54",
  "org.apache.camel" % "camel-saxon" % "2.19.0",
  "net.sf.saxon" % "Saxon-HE" % "9.7.0-18",
  "org.apache.velocity" % "velocity" % "1.7",
  "org.apache.poi" % "poi" % "3.17",
  "org.apache.poi" % "poi-ooxml" % "3.17"
)

routesGenerator := InjectedRoutesGenerator
PlayKeys.externalizeResources := false

assemblySettings
mainClass in assembly := Some("play.core.server.ProdServerStart")

fullClasspath in assembly += Attributed.blank(PlayKeys.playPackageAssets.value)

mergeStrategy in assembly := {
  case PathList("javax", "servlet", xs @ _*)         => MergeStrategy.first
  case PathList(ps @ _*) if ps.last endsWith ".html" => MergeStrategy.first
  case PathList(ps @ _*) if ps.last endsWith ".jar"  => MergeStrategy.first
  case PathList("META-INF", xs @ _*) => MergeStrategy.discard
  //case PathList(ps @ _*) if ps.last endsWith ".conf" => MergeStrategy.first
  case manifest if manifest.contains("MANIFEST.MF")  => MergeStrategy.discard
  case referenceOverrides if referenceOverrides.contains("reference-overrides.conf") => MergeStrategy.concat
  case "application.conf"                            => MergeStrategy.concat
  case x =>
    val oldStrategy = (mergeStrategy in assembly).value
    oldStrategy(x)
}

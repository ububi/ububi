/**---------------------------   
 * PROJECT: UBUBI
 * Auth:
 *   Mathieu Dupuis
 * Date: ETE 2017
 *
 * Copyright [2017] [Mathieu Dupuis]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. 
 *
 *--------------------------*/
package util;

import models.IED.BIMEnergyType;
import models.IED.BIMUniformatClassification;
import models.LCA.*;
import org.openlca.core.model.*;
import org.openlca.core.model.Process;
import org.ububiGroup.IEDManager.IO.BIM.IEDImporter;
import org.ububiGroup.IEDManager.IO.generic.IEDImportHandler;
import org.ububiGroup.IEDManager.IO.generic.baseImporter;
import org.ububiGroup.IEDManager.model.BIM.*;
import org.ububiGroup.IEDManager.model.generic.IEDUnitDimension;
import org.ububiGroup.IEDManager.model.generic.IEDUnitSystem;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import java.io.IOException;
import java.math.BigInteger;
import java.util.*;

//TODO JAVADOC
/**
 * Created by mathieu on 8/30/2016.
 *
 * Limitation:
 * -Support only process with one output flow
 * -Need a better unit management
 */
public class IED2LCA
{
    private static String[][] mapUnit;

    private EntityManager em;
    private baseImporter importer;

    private ArrayList<Parameter> lstParam;

    private String detailesLog="";

    //TODO is it needed?
    //Cache var
    private Map<ModelType,HashMap<BIMEntityType,BIMCategory>> lstCategCache;
    private HashMap<String,Unit> lstUnitCache;
    private HashMap<IEDUnitDimension,FlowProperty> lstFlowPropertyCache;

    private String[] defaultUnit;
    private long LastId=0;

    /***********************************************
     *                  Public member
     * **********************************************/

    private BIMModel bimModel;

    private HashMap<String,BIMProcess> lstBIMTypeObject;
    private HashMap<String,BIMProcess> lstBIMObject;
    private HashMap<String,BIMProcess> lstBIMMaterial;
    private HashMap<String,BIMProcess> lstUniformat;
    private HashMap<String,BIMProcess> lstBimObjectTypeToUpdate;

    public BIMModel getBimModel() {
        return bimModel;
    }

    public HashMap<String, BIMProcess> getLstBIMTypeObject() {
        return lstBIMTypeObject;
    }

    public HashMap<String, BIMProcess> getLstBIMObject() {
        return lstBIMObject;
    }

    public HashMap<String, BIMProcess> getLstBIMMaterial() {
        return lstBIMMaterial;
    }

    public HashMap<String, BIMProcess> getLstUniformat() {
        return lstUniformat;
    }

    public HashMap<String, BIMProcess> getLstBimObjectTypeToUpdate() {
        return lstBimObjectTypeToUpdate;
    }

    public HashMap<BIMEntityType, HashMap<Long, Exchange>> getLstAutoLinked() {
        return lstAutoLinked;
    }

    private HashMap<BIMEntityType,HashMap<Long,Exchange>> lstAutoLinked;

    private HashMap<BIMEntityType,HashMap<String,BIMProcess>> lstBuffer;

    private HashMap<String,Middleground> lstAutoLink[];

    //<editor-fold desc="Public method">
    public IED2LCA(EntityManager em)
    {
        this.em = em;
        createUnitMap();
        lstBIMMaterial = new HashMap<>();
        lstBIMObject = new HashMap<>();
        lstBIMTypeObject = new HashMap<>();
        lstUniformat = new HashMap<>();
        lstBuffer = new HashMap<>();
        lstParam = new ArrayList<>();
        lstBimObjectTypeToUpdate = new HashMap<>();

        //Init cache var
        lstCategCache = new HashMap<>();
        for (ModelType mt : ModelType.values()) {
            lstCategCache.put(mt, new HashMap<>());
        }
        lstFlowPropertyCache = new HashMap<>();
        lstUnitCache = new HashMap<>();

        //Map the buffer
        lstBuffer.put(BIMEntityType.BIMMaterial,lstBIMMaterial);
        lstBuffer.put(BIMEntityType.BIMObject,lstBIMObject);
        lstBuffer.put(BIMEntityType.BIMObjectType,lstBIMTypeObject);
        lstBuffer.put(BIMEntityType.BIMUniformat,lstUniformat);
        lstBuffer.put(BIMEntityType.BIMRoot,lstUniformat);
        lstBuffer.put(BIMEntityType.BIMEnergy,lstUniformat);

        lstAutoLinked=new HashMap<>();
        for(int i=0;i<BIMEntityType.values().length;i++)
            lstAutoLinked.put(BIMEntityType.values()[i],new HashMap<>());
        lstAutoLinked.put(null,new HashMap<>());

        initAutoLink();
	}

    public void convert(long idModel, String modelName, String iedFile) throws IOException
    {
        importer = initImporter(iedFile);

        IEDImportHandler<BIMMaterial> materialHandler = new IEDImportHandler<BIMMaterial>() {
            @Override
            public boolean processData(BIMMaterial bimData) {
                return getOrCreateMaterialProcess(bimData) != null;
            }
        };

        IEDImportHandler<BIMObject> objectHandler = new IEDImportHandler<BIMObject>() {
            @Override
            public boolean processData(BIMObject bimData) {
                return getOrCreateObjectProcess(bimData) != null;
            }
        };

        IEDImportHandler<BIMObjectType> objectTypeHandler = new IEDImportHandler<BIMObjectType>() {
            @Override
            public boolean processData(BIMObjectType bimData) {
                return getOrCreateObjectTypeProcess(bimData) != null;
            }
        };

        if (idModel != -1) {
            clearBIMModel(em.find(models.LCA.BIMModel.class, idModel));
            detailesLog+="model action: update;";
        } else {
            createBIMModel(modelName);
            detailesLog+="model action: create;";
        }
        BIMProcess pUnknown = getOrCreateUniformatProcess(null, "Unknown", "");

        importer.ProcessAll(materialHandler, objectHandler, objectTypeHandler);
        getOrCreateEnergyProcess();
        importer.close();

    }
    //</editor-fold>

    private void getOrCreateEnergyProcess()
    {
        for (BIMEnergyType energyType : BIMEnergyType.values()) {

            BIMProcess process = lstUniformat.getOrDefault(energyType.toString(), null);

            //Create if it doesnt exist
            if (process == null) {
                BIMProcess subEnergyProcess = createEmptyProcess(BIMEntityType.BIMEnergy, energyType.toString(), energyType.toString(), energyType.returnEnergyDimention(energyType));
                BIMProcess energyRootProcess = getOrCreateUniformatProcess("G", "Operational Energy", "Operational Energy");
                linkProcesses(subEnergyProcess, energyRootProcess, 0, "");
            }
        }
    }


    //<editor-fold desc="Utils">

    /***********************************************
     *                  Utils
     * **********************************************/
    private baseImporter initImporter(String iedFile) throws IOException
    {
        baseImporter importer = new IEDImporter();
        importer.init(iedFile);

        //TODO manage unit
        createUnitSystem();

        return importer;
    }

    private static void createUnitMap()
    {
        if (mapUnit == null) {
            mapUnit = new String[IEDUnitSystem.values().length][IEDUnitDimension.values().length];

            mapUnit[IEDUnitSystem.metric.ordinal()][IEDUnitDimension.Item.ordinal()] = "Item(s)";
            mapUnit[IEDUnitSystem.metric.ordinal()][IEDUnitDimension.Length.ordinal()] = "m";
            mapUnit[IEDUnitSystem.metric.ordinal()][IEDUnitDimension.LengthByTime.ordinal()] = "m*a";
            mapUnit[IEDUnitSystem.metric.ordinal()][IEDUnitDimension.Area.ordinal()] = "m2";
            mapUnit[IEDUnitSystem.metric.ordinal()][IEDUnitDimension.AreaByTime.ordinal()] = "m2*a";
            mapUnit[IEDUnitSystem.metric.ordinal()][IEDUnitDimension.Volume.ordinal()] = "m3";
            mapUnit[IEDUnitSystem.metric.ordinal()][IEDUnitDimension.VolumeByTime.ordinal()] = "m3*a";

        }
    }

    private void createUnitSystem()
    {
        //TODO Add some option maybe a custom system.
        this.defaultUnit = mapUnit[IEDUnitSystem.metric.ordinal()];
    }

    private String getDefaultUnit(IEDUnitDimension dimension) {
        return defaultUnit[dimension.ordinal()];
    }

    private void save(Object entity)
    {
        try
        {
            em.persist(entity);
        }
        catch (Exception e)
        {
            e.printStackTrace();
            throw e;
        }
    }
    //</editor-fold>

    //<editor-fold desc="Managing OLCA">
    /***********************************************
     *                  Managing OLCA
     * **********************************************/

    //<editor-fold desc="Managing model">
    /***********************************************
     *                  Managing model
     * **********************************************/
    private BIMModel createBIMModel(String modelName)
    {
        bimModel=new BIMModel();
        bimModel.setName(modelName);
        bimModel.setCreationDate(new Date());
        save(bimModel);

        BIMProcess p = getOrCreateUniformatProcess("",modelName, "");
        bimModel.setRootProcess(p.getProcess());
        bimModel.setRootFlow(getOutputFlow(p.getProcess()));
        save(bimModel);

        return bimModel;
    }

    private BIMModel clearBIMModel(BIMModel bimModelToUpdate)
    {

        List<BIMProcess> pl = new ArrayList<>();
        List<BIMFlow> fl = new ArrayList<>();
        List<BIMExchange> el = new ArrayList<>();
        BIMProcess root = null;

        /* Add all Process, flows and exchanges to list */
        /* Keep Root for each */

        for(BIMProcess p : bimModelToUpdate.getProcesses()){

            /* Keep ObjectTypes to only modify qty */
            if(p.getBimEntity().getBimEntityType().name().equals(BIMEntityType.BIMObjectType.name())) {
                lstBimObjectTypeToUpdate.put(p.getBimEntity().getBimKey(), p);
            }

            if(p.getIdProcess()==bimModelToUpdate.getIdRootProcess()){
                root = p;
            } else {
                pl.add(p);
            }
        }

        for(BIMFlow f : bimModelToUpdate.getFlows()) {
            if(f.getIdFlow()!=bimModelToUpdate.getIdRootFlow() ) {
                fl.add(f);
            }
        }
        /*&& !f.getBimEntity().getBimEntityType().equals(BIMEntityType.BIMObjectType) */

        long ex_rootId = -1;
        BIMExchange ex_root = null;
        for(BIMExchange e: bimModelToUpdate.getExchanges()){
            /* Find root exchange */
            if(e.getBimEntity().getBimEntityType().name().equals(BIMEntityType.BIMRoot.name())) {
                if (ex_rootId == -1 || e.getId() < ex_rootId) {
                    ex_rootId = e.getId();
                    ex_root = e;
                }
            }
            el.add(e);
        }

        el.remove(ex_root);
        /* FK constraints need to be removed */
        for (BIMExchange e: el){
            e.getExchange().setFlowPropertyFactor(null);
            e.getExchange().setFlow(null);
        }

        /* Drop tree */
        bimModelToUpdate.getExchanges().removeAll(el);
        bimModelToUpdate.getFlows().removeAll(fl);
        bimModelToUpdate.setRootProcess(root.getProcess());
        bimModelToUpdate.getProcesses().removeAll(pl);

        bimModel = bimModelToUpdate;

        return bimModel;
    }

    //</editor-fold>

    //<editor-fold desc="Managing general process">
    /***********************************************
     *                   Managing General Process
     * **********************************************/

    private BIMProcess createEmptyProcess(BIMEntityType entityType, String key, String name, IEDUnitDimension unit)
    {
        HashMap<IEDUnitDimension,Double> lstDimension = new HashMap<>();
        lstDimension.put(unit,1.0);

        /* If is update, Root should already exist */
        /* return it instead of creating new */
        if (entityType.equals(BIMEntityType.BIMRoot) && bimModel.getRootProcess()!=null) {
            for (BIMProcess p: bimModel.getProcesses()){
                if(p.getIdProcess() == bimModel.getIdRootProcess()){
                    return p;
                }
            }
        }
        return createEmptyProcess(entityType,key,name,unit,lstDimension);
    }


    private BIMProcess createEmptyQuantifiedProcess(BIMEntityType entityType, String key, String name, IEDUnitDimension defaultUnit, Double area, Double volume)
    {
        HashMap<IEDUnitDimension,Double> lstDimension = new HashMap<>();

        if(volume!=0D)lstDimension.put(IEDUnitDimension.Volume,volume);
        if(area!=0D)lstDimension.put(IEDUnitDimension.Area,volume==0D?1D:area/volume);


        return createEmptyProcess(entityType,key,name, defaultUnit,lstDimension);
    }

    private BIMProcess createEmptyProcess(BIMEntityType entityType, String key, String name, IEDUnitDimension defaultUnit,HashMap<IEDUnitDimension,Double> lstDimension)
    {
        //Create the process documentation
        ProcessDocumentation processDoc = new ProcessDocumentation();
        processDoc.setCreationDate(new Date());
        processDoc.setCopyright(false);
        save(processDoc);

        //Create the process
        Process process = new Process();
        process.setVersion(0);
        process.setLastChange(0);
        process.setLocation(null);
        process.setProcessType(ProcessType.UNIT_PROCESS);
        process.setDescription("");
        process.setName(name);
        process.setRefId(UUID.randomUUID().toString());
        process.setDocumentation(processDoc);
        save(process);

        //Create the BIMProcess
        BIMProcess bimProcess = new BIMProcess();
        bimProcess.setBimEntity(new BIMEntity(key,entityType));
        bimProcess.setProcess(process);
        bimProcess.setModel(bimModel);
        lstBuffer.getOrDefault(entityType,null).put(key,bimProcess);
        save(bimProcess);

        BIMCategory bimCategory = getOrCreateBIMCategory(ModelType.PROCESS,entityType);
        process.setCategory(bimCategory.getCategory());

        process.setQuantitativeReference(createDefaultOutputExchange(bimProcess,defaultUnit,lstDimension).getExchange());
        save(process);

        return bimProcess;
    }

    public Process createEmptyProcess(String name, Category category)
    {
        //Create the process
        Process process = new Process();
        process.setVersion(0);
        process.setLastChange(0);
        process.setLocation(null);
        process.setProcessType(ProcessType.UNIT_PROCESS);
        process.setDescription("");
        process.setName(name);
        process.setRefId(UUID.randomUUID().toString());
        process.setCategory(category);
        Exchange exchange = new Exchange();
        process.setQuantitativeReference(exchange);
        process.getExchanges().add(exchange);
        save(process);

        return process;
    }

    public Process createTemplateProcess(String name, Category catProcess, Category catFlow, String unit)
    {
        Flow flow = createEmptyFlow(name, catFlow, unit);
        //Create the process
        Process process = new Process();
        process.setVersion(0);
        process.setLastChange(0);
        process.setLocation(null);
        process.setProcessType(ProcessType.UNIT_PROCESS);
        process.setDescription("");
        process.setName(name);
        process.setRefId(UUID.randomUUID().toString());
        process.setCategory(catProcess);
        Exchange exchange = new Exchange();
        process.setQuantitativeReference(exchange);
        process.getExchanges().add(exchange);

        //Create output flow
        exchange.setFlow(flow);
        exchange.setUnit(getLcaUnit(unit));
        exchange.setAmountFormula(null);
        exchange.setAmountValue(1);
        exchange.setFlowPropertyFactor(flow.getReferenceFactor());

        save(process);

        return process;
    }

    //</editor-fold>

    //<editor-fold desc="Managing ObjectType process">
    /***********************************************
     *                  Managing ObjectType process
     ***********************************************/

    private BIMProcess getOrCreateObjectTypeProcess(BIMObjectType objectType)
    {
        //TODO choose good unit
        BIMProcess process = getOrCreateObjectTypeProcess(objectType.getId(),objectType.getName()+"-"+objectType.getId());
        //Create uniformat process for later
        String key=objectType.getUniformatCode();
        if(!("".equals(key)))
        {
            getOrCreateUniformatProcess(key);
        }

        return process;
    }

    private BIMProcess getOrCreateObjectTypeProcess(Long idObjectType,String name)
    {
        //TODO choose good unit
        BIMProcess process = this.lstBIMTypeObject.getOrDefault(idObjectType.toString(),null);
        if(process==null)
        {
            //Create a new process
            process = createEmptyProcess(BIMEntityType.BIMObjectType, idObjectType.toString(),
                                                   name, IEDUnitDimension.Volume);
            autoLink(process.getProcess(),BIMEntityType.BIMObjectType);
        }
        return process;
    }

    //</editor-fold>

    //<editor-fold desc="Managing Object process">
    /***********************************************
     *                  Managing Object process
     ***********************************************/

    private BIMProcess getOrCreateObjectProcess(BIMObject object)
    {
        BIMProcess process = createEmptyProcess(BIMEntityType.BIMObject, object.getId()+"", object.getName()+"-"+object.getId(), IEDUnitDimension.Item);
        setQtyParameter(process.getProcess(),object.getArea(), object.getVolume());

        // Link to Object Type process
        BIMProcess objectTypeProcess = getOrCreateObjectTypeProcess(object.getTypeId(),"");

        // Set the ratio of the object type if is not set
        Flow flow = getOutputFlow(objectTypeProcess.getProcess());
        if(flow.getFlowPropertyFactors().size()<=1 && object.getVolume()!=0)
        {
            updateFlowProperty(flow,object.getArea(),object.getVolume());
        }

        //TODO choose good unit
        linkObjectToObjectTypeProcess(objectTypeProcess, process, object.getArea(),object.getVolume());

        // Linking in the uniformat process
        String key=null;
        if(!("".equals(object.getUniformatCode())))
        {
            key = object.getUniformatCode();
        }

        BIMProcess uniformatProcess = getOrCreateUniformatProcess(key);
        linkProcesses(process,uniformatProcess,1.0,"");

        return process;
    }

    //</editor-fold>

    //<editor-fold desc="Managing Material process">

    /***********************************************
     *                  Managing Material process
     ***********************************************/


    private BIMProcess getOrCreateMaterialProcess(BIMMaterial material)
    {
        BIMProcess materialProcess = getOrCreateMaterialProcess(material.getId(),material.getName());

        //Maybe it was created but empty, so we need to complete the object
        Exchange exchange = getOutputExchange(materialProcess.getProcess());
        Flow flow = exchange.getFlow();
        updateFlowProperty(flow,material.getArea(),material.getVolume());
        if(!"".equals(material.getName()))
        {
            materialProcess.getProcess().setName(material.getName());
            flow.setName(material.getName());
            autoLink(materialProcess.getProcess(),BIMEntityType.BIMMaterial);
        }

        save(flow);
        save(exchange);

        //Link Type and Material
        BIMProcess ObjTypeProcess= getOrCreateObjectTypeProcess(material.getOwnerId(),"");
        linkProcesses(materialProcess,ObjTypeProcess,material.getRatioVolume(), null);

        return materialProcess;
    }

    private BIMProcess getOrCreateMaterialProcess(Long id, String name)
    {
        //Verify if its already exist
        BIMProcess process = this.lstBIMMaterial.getOrDefault(name,null);
        if(process==null)
        {
            //Create a new process
            process = createEmptyProcess(BIMEntityType.BIMMaterial, id.toString(), name, IEDUnitDimension.Volume);
            this.lstBIMMaterial.put(name,process);
        }

        return process;
    }

    //</editor-fold>

    //<editor-fold desc="Managing Uniformat process">

    /***********************************************
     *                  Managing Uniformat process
     ***********************************************/

    private BIMProcess getOrCreateUniformatProcess(String key)
    {
        String keySearch = key==null?null:key.trim();

        if(keySearch!=null && keySearch.length()>5)
        {
            keySearch = keySearch.substring(0,5);
        }

        BIMUniformatClassification uniformatClassification = BIMUniformatClassification.getUniformat(keySearch,BIMUniformatClassification.EMPTY_UNIFORMAT);

        return getOrCreateUniformatProcess(keySearch,keySearch+" "+uniformatClassification.getName(),uniformatClassification.getName());
    }

    private BIMProcess getOrCreateUniformatProcess(String key, String name, String description)
    {
        //null --> unknown
        //"" --> root

        //Skip Unitformat rank 4 and more
        String currentKey = key;
        if(currentKey!=null && currentKey.length()>5)
        {
            currentKey = key.substring(0,5);
        }

        BIMProcess process=lstUniformat.getOrDefault(currentKey,null);

        //Create if it doesnt exist
        if(process==null)
        {
            //Create the Uniformat process
            process = createEmptyProcess("".equals(currentKey) ? BIMEntityType.BIMRoot : BIMEntityType.BIMUniformat, currentKey, name, IEDUnitDimension.Item);

            //The root has no parent
            if(!("".equals(currentKey)))
            {
                //Link with his parent
                String parentKey = "";
                if (currentKey == null || currentKey.length() == 1) // Uniformat unknown or rank 1
                {
                    parentKey = ""; //Send root
                }
                else if (currentKey.length() == 2 || currentKey.length() == 3 ) //Rank 2
                {
                    parentKey = currentKey.substring(0,1); //Send rank1
                }
                else //Rank 3 and more
                {
                    parentKey = currentKey.substring(0,3); //Send rank2
                }
                BIMProcess parent=getOrCreateUniformatProcess(parentKey);
                linkProcesses(process,parent,1,null);
            }
        }

        //Set the description if its not set
        if(description!=null && !("".equals(description)))
        {
            process.getProcess().setDescription(description);
        }

        return process;
    }

    //</editor-fold>

    //<editor-fold desc="Linking method">

    private BIMExchange linkProcesses(BIMProcess producer, BIMProcess receiver, double qty, String formula)
    {
        //Create the input Exchange

        if(lstBimObjectTypeToUpdate.get(receiver.getBimEntity().getBimKey())!=null){
            receiver = lstBimObjectTypeToUpdate.get(receiver.getBimEntity().getBimKey());
        }
        Exchange producedExchange = getOutputExchange(producer.getProcess());

        Exchange exchange = new Exchange();
        Flow flow=producedExchange.getFlow();
        exchange.setFlow(flow);
        exchange.setInput(true);
        exchange.setUnit(producedExchange.getUnit());
        exchange.setAmountFormula("".equals(formula)?null:formula);
        exchange.setAmountValue(qty);
        exchange.setFlowPropertyFactor(flow.getReferenceFactor());
        receiver.getProcess().getExchanges().add(exchange);
        save(exchange);

        BIMExchange bimExchange = new BIMExchange();
        bimExchange.setBimEntity(new BIMEntity(receiver.getBimEntity()));
        bimExchange.setModel(bimModel);
        bimExchange.setExchange(exchange);
        save(bimExchange);

        bimModel.getExchanges().add(bimExchange);

        save(receiver.getProcess());

        return bimExchange;
    }

    private BIMExchange linkObjectToObjectTypeProcess(BIMProcess objectTypeProcess, BIMProcess objectProcess, double area, double volume)
    {
        Exchange producedExchange = getOutputExchange(objectTypeProcess.getProcess());

        double qty;
        String formula;
        if(getDefaultUnit(IEDUnitDimension.Area).equals(producedExchange.getUnit().getName()))
        {
            qty = area;
            formula="area";
        }
        else
        {
            qty=volume;
            formula="volume";
        }

        //Create the input Exchange
        return linkProcesses(objectTypeProcess,objectProcess,qty,formula);
    }

    private Exchange autoLink(Process proc, BIMEntityType entityType)
    {
        String name = proc.getName();
        if(name==null || "".equals(name.trim()))
            return null;

        Middleground middle = this.lstAutoLink[entityType.ordinal()].getOrDefault(name,null);

        if(middle==null)
            middle = this.lstAutoLink[BIMEntityType.values().length].getOrDefault(name,null);

        //If we need to link it and is not already linked
        if(middle!=null && !isLinked(proc, middle.getIdFlow()))
        {
            Double amount;
            String unit;
            String amountDesc;
            FlowPropertyFactor fpfExchange;
            Unit unitExchange;

            String desc="Auto-Link:";
            Flow flow=em.find(Flow.class, middle.getIdFlow());

            HashMap<Long, FlowPropertyFactor> mapFpfMiddle = this.getConversionByFlowProperty(flow);
            HashMap<Long, FlowPropertyFactor> mapFpfProc = this.getConversionByFlowProperty(proc.getQuantitativeReference().getFlow());

            /**
             * Choose the good unit:
             * 1. Use the default unit from process if destination have it
             * 2. Use the alternate unit from unit if destination have it
             * 3. Use the default unit from mg with quantity 0 (impossible to convert)
             *    All exchange with 0 quantity will be detected as not properly link in the frontend.
             *    The user will change the quantity manually
             * */

            Exchange quatitativeReference = proc.getQuantitativeReference();
            FlowPropertyFactor fpfDefault = mapFpfMiddle.getOrDefault(quatitativeReference.getFlowPropertyFactor().getFlowProperty().getId(),null);
            if(fpfDefault!=null)
            {
                fpfExchange = fpfDefault;
                unitExchange = quatitativeReference.getUnit();
                amount =quatitativeReference.getAmountValue();
                unit = "default unit";
                amountDesc = "default amount";
            }
            else
            {
                int i=0;
                FlowPropertyFactor fpfFound=null;
                FlowPropertyFactor fpfProc=null;
                FlowPropertyFactor[] lstFpfProc = new FlowPropertyFactor[0];
                lstFpfProc = mapFpfProc.values().toArray(lstFpfProc);
                while(fpfFound==null && i<lstFpfProc.length)
                {
                    fpfProc = lstFpfProc[i];
                    fpfFound = mapFpfMiddle.getOrDefault(fpfProc.getFlowProperty().getId(),null);
                    i++;
                }

                if(fpfFound!=null)
                {
                    fpfExchange=fpfFound;
                    unitExchange = fpfExchange.getFlowProperty().getUnitGroup().getReferenceUnit();

                    amount = 0D;
                    if(fpfProc.getConversionFactor()!=0d)
                        amount = quatitativeReference.getAmountValue()*
                                 quatitativeReference.getUnit().getConversionFactor()* //Convert amount to the default unit of this flow property
                                 quatitativeReference.getFlowPropertyFactor().getConversionFactor()/ //Convert amount to the default flow property
                                 fpfProc.getConversionFactor(); //Convert amount to de desired flow property

                    unit = "alternate unit";
                    amountDesc = "ratio amount";
                }
                else
                {
                    fpfExchange=flow.getReferenceFactor();
                    unitExchange = fpfExchange.getFlowProperty().getUnitGroup().getReferenceUnit();
                    amount=0d;
                    unit = "unit not found";
                    amountDesc = "impossible to convert";
                }
            }

            desc+=unit+","+amountDesc;

            Exchange exchangeAuto =createInputExchange(proc,flow,amount,desc, unitExchange, fpfExchange);

            lstAutoLinked.get(entityType).put(exchangeAuto.getId(),exchangeAuto);

            return exchangeAuto;
        }
        return null;
    }

    private boolean isLinked(Process p, long IdFlow)
    {
        List<Exchange> exchanges = p.getExchanges();

        int i=0;
        boolean found=false;
        while(i<exchanges.size() && !found)
        {
            Exchange iExchange = exchanges.get(i);
            found = DBUtil.getOLCAId(iExchange.getFlow())==IdFlow;
            i++;
        }

        return found;
    }

    private Double getFactor(Flow flow, String unit)
    {
        return getFactor(flow,unit,null);
    }

    private Double getFactor(Flow flow, String unit, Double defaultFactor)
    {
        long idUnitGroup = -1;

        try
        {
            String sqlRatio = "SELECT ug.id, u.conversion_factor " +
                    "FROM tbl_unit_groups as ug inner join tbl_units as u on(u.f_unit_group=ug.id) " +
                    "where u.name=:unit";
            Query ratioQuery = em.createNativeQuery(sqlRatio);
            ratioQuery.setParameter("unit", unit);
            ratioQuery.setMaxResults(1);
            ratioQuery.setFirstResult(0);

            List<Object[]> result =ratioQuery.getResultList();
            if(result.size()>0)
            {
                Object[] line = result.get(0);
                idUnitGroup = Long.parseLong(line[0].toString(),10);
                Double factorUnit = Double.parseDouble(line[1].toString());
                for (FlowPropertyFactor fpf : flow.getFlowPropertyFactors())
                {
                    if (fpf.getFlowProperty().getUnitGroup().getId() == idUnitGroup)
                        return factorUnit * fpf.getConversionFactor();
                }
            }
        }
        catch (NoResultException e){}

        return defaultFactor;
    }

    //</editor-fold>

    //<editor-fold desc="Managing Flow / Exchange">

    /***********************************************
     *                   Managing Flow / Exchange
     * **********************************************/

    private BIMExchange createDefaultOutputExchange(BIMProcess bimProcess,IEDUnitDimension defaultUnit,HashMap<IEDUnitDimension,Double> lstDimension)
    {
        //Create the Output Exchange
        BIMFlow bimFlow = createOutputFlow(bimProcess,defaultUnit,lstDimension);
        Exchange exchange = new Exchange();
        exchange.setFlow(bimFlow.getFlow());
        exchange.setInput(false);
        exchange.setFlowPropertyFactor(bimFlow.getFlow().getReferenceFactor());
        exchange.setUnit(getLcaUnit(defaultUnit));
        exchange.setAmountFormula(null);
        exchange.setAmountValue(1);
        bimProcess.getProcess().getExchanges().add(exchange);
        save(exchange);

        BIMExchange bimExchange = new BIMExchange();
        bimExchange.setBimEntity(new BIMEntity(bimProcess.getBimEntity()));
        bimExchange.setModel(bimModel);
        bimExchange.setExchange(exchange);
        save(bimExchange);

        bimModel.getExchanges().add(bimExchange);
        save(bimProcess.getProcess());

        return bimExchange;
    }

    private Flow getOutputFlow(Process p)
    {
        Exchange exchange = getOutputExchange(p);
        return exchange!=null?exchange.getFlow():null;
    }

    private Exchange getOutputExchange(Process p)
    {
        return p.getQuantitativeReference();
    }


    private BIMFlow createProductFlow(String name, BIMEntity entity, IEDUnitDimension defaultUnit, HashMap<IEDUnitDimension,Double> lstDimension)
    {
        Flow flow = new Flow();
        flow.setVersion(0);
        flow.setLastChange(0);
        flow.setFormula(null);
        flow.setLocation(null);
        flow.setCasNumber(null);
        flow.setInfrastructureFlow(false);
        flow.setFlowType(FlowType.PRODUCT_FLOW);
        flow.setDescription("");
        flow.setName(name);
        flow.setRefId(UUID.randomUUID().toString());
        BIMCategory categ = getOrCreateBIMCategory(ModelType.FLOW,entity.getBimEntityType());
        flow.setCategory(categ.getCategory());

        // Set Flows propertys
        for(Map.Entry<IEDUnitDimension,Double> entry : lstDimension.entrySet())
        {
            FlowProperty fp = getFlowProperty(entry.getKey());
            FlowPropertyFactor fpf = createFlowPropertyFactor(flow, fp, entry.getValue());

            if(entry.getKey()==defaultUnit)
                flow.setReferenceFlowProperty(fp);

            save(fpf);
        }
        save(flow);

        // Create bimflow
        BIMFlow bimFlow = new BIMFlow();
        bimFlow.setModel(bimModel);
        bimFlow.setBimEntity(new BIMEntity(new BIMEntity(entity)));
        bimFlow.setFlow(flow);
        bimModel.getFlows().add(bimFlow);
        save(bimFlow);

        return bimFlow;
    }

    private BIMFlow createOutputFlow(BIMProcess process, IEDUnitDimension defaultUnit, HashMap<IEDUnitDimension,Double> lstDimension)
    {
        return createProductFlow(process.getProcess().getName(),process.getBimEntity(),defaultUnit, lstDimension);
    }

    private void updateFlowProperty(Flow flow, Double area, Double volume)
    {
        //TODO To adjust
        if(flow.getFlowPropertyFactors().size()<2)
        {
            if(area!=0 && createFlowPropertyFactor(flow, getFlowProperty(IEDUnitDimension.Area), volume == 0D ? 1D :volume/area)!=null);
                save(flow);
        }
    }

    // Create Empty Flow without BimEntity
    public Flow createEmptyFlow(String name, Category category, String unit)
    {
        Flow flow = new Flow();
        flow.setVersion(0);
        flow.setLastChange(0);
        flow.setFormula(null);
        flow.setLocation(null);
        flow.setCasNumber(null);
        flow.setInfrastructureFlow(false);
        flow.setFlowType(FlowType.PRODUCT_FLOW);
        flow.setDescription("");
        flow.setName(name);
        flow.setRefId(UUID.randomUUID().toString());
        flow.setCategory(category);

        FlowProperty fp = getFlowProperty(unit);
        flow.setReferenceFlowProperty(fp);
        createFlowPropertyFactor(flow,fp,1D);

        save(flow);

        return flow;
    }

    public Exchange createInputExchange(Process pOutput, Flow fInput, String unit)
    {
        return createInputExchange(pOutput,fInput,1,unit);
    }

    public Exchange createInputExchange(Process pOutput, Flow fInput, double amount, String unit)
    {
        return createInputExchange(pOutput,fInput,amount,"",unit);
    }

    public Exchange createInputExchange(Process pOutput, Flow fInput, double amount, String desc,
                                        FlowPropertyFactor fpf)
    {
        return createInputExchange(pOutput, fInput, amount, desc,
                                   fpf.getFlowProperty().getUnitGroup().getReferenceUnit() ,fpf);
    }

    public Exchange createInputExchange(Process pOutput, Flow fInput, double amount, String desc,
                                        Unit unit, FlowPropertyFactor fpf)
    {
        Exchange exchangeI = new Exchange();
        pOutput.getExchanges().add(exchangeI);

        exchangeI.setInput(true);
        exchangeI.setFlow(fInput);
        exchangeI.setUnit(unit);
        exchangeI.setAmountFormula(null);
        exchangeI.setAmountValue(amount);
        exchangeI.description=desc;
        exchangeI.setFlowPropertyFactor(fpf);
        save(exchangeI);
        save(pOutput);

        return exchangeI;
    }

    public Exchange createInputExchange(Process pOutput, Flow fInput, double amount, String desc, String unit)
    {
        return createInputExchange(pOutput, fInput, amount, desc, getLcaUnit(unit), fInput.getReferenceFactor());
    }

    //</editor-fold>

    //<editor-fold desc="Managing parameter">

    /***********************************************
     *                   Managing parameter
     * **********************************************/
    private void setQtyParameter(Process p, double area, double volume)
    {
        setParameter(p,"area",area,"in m2");
        setParameter(p,"volume",volume,"in m3");
    }

    public void setParameter(Process p, String name, double value, String description)
    {
        setParameter(p, name, value, description, "");
    }

    private void setParameter(Process p, String name, double value, String description, String formula)
    {
        //TODO doesnt support internal parameter (parameter based on parameters)
        //TODO Add a category
        Parameter param = getParameter(p,name);
        if(param==null)
        {
            param = new Parameter();
            param.setName(name);
            param.setRefId(UUID.randomUUID().toString());
            param.setScope(ParameterScope.PROCESS);
            p.getParameters().add(param);
            save(param);

            lstParam.add(param);
        }

        param.setDescription(!("".equals(description))?description:null);
        param.setFormula("".equals(formula)?null:formula);
        param.setValue(value);
        param.setInputParameter(true);
        save(param);
    }

    private Parameter getParameter(Process p, String name)
    {
        for (Parameter param :p.getParameters())
        {
            if(param.getName()==name)
                return param;
        }
        return null;
    }

    //</editor-fold>

    //<editor-fold desc="Managing category">

    /***********************************************
     *                   Managing category
     * **********************************************/

    public Category getOrCreateCategory(ModelType modelType, String name, Category categParent)
    {
        Category categ = null;

        //Try to find the category
        try
        {
            if(categParent==null)
            {
                categ = (Category) em.createQuery("Select categ from Category categ where categ.category is null and categ.name=:name and categ.modelType = :modelType").
                        setParameter("modelType", modelType).setParameter("name", name).getSingleResult();
            }
            else
            {
                categ = (Category) em.createQuery("Select categ from Category categ where categ.category=:parent and categ.name=:name and categ.modelType = :modelType").
                        setParameter("modelType", modelType).setParameter("name", name).setParameter("parent",categParent).getSingleResult();
            }
        }
        catch (NoResultException e) //Not found
        {
            //Create the category
            categ = new Category();
            categ.setModelType(modelType);
            categ.setCategory(categParent);
            categ.setName(name);
            categ.setRefId(UUID.randomUUID().toString());
            save(categ);
        }

        return categ;
    }

    private Category getOrCreateCategory(ModelType modelType, BIMEntityType bimEntity)
    {
        //Root
        Category parent = getOrCreateCategory(modelType, "_UBUBI",null);

        //Project Root
        parent = getOrCreateCategory(modelType, bimModel.getName(),parent);

        //Entity Type (except if it's the root of the project)
        if(bimEntity!=BIMEntityType.BIMRoot)
            parent = getOrCreateCategory(modelType, bimEntity.toString(),parent);

        return parent;
    }

    private BIMCategory getOrCreateBIMCategory(ModelType modelType, BIMEntityType bimEntity)
    {
        //TODO See if entity manager is beter than that
        BIMCategory bimCateg = getBIMCategoryFromCache(modelType, bimEntity);

        if(bimCateg == null)
        {
            //Try to find the category
            try
            {
                bimCateg = (BIMCategory) em.createQuery("Select bimCateg from BIMCategory bimCateg where bimCateg.modelType=:modelType and bimCateg.bimEntity.bimEntityType=:bimEntity and bimCateg.model=:model").
                        setParameter("modelType", modelType).setParameter("bimEntity", bimEntity).setParameter("model",bimModel).getSingleResult();
            }
            catch (NoResultException e) //Not found
            {
                //Create the category
                bimCateg = new BIMCategory();
                bimCateg.setModel(bimModel);
                bimCateg.setModelType(modelType);
                bimCateg.setBimEntity(new BIMEntity("", bimEntity));
                bimCateg.setCategory(getOrCreateCategory(modelType,bimEntity));

                addBIMCategoryToCache(modelType,bimEntity,bimCateg);
                bimModel.getCategories().add(bimCateg);
                save(bimCateg);
            }
            saveBIMCategoryInCache(modelType, bimEntity, bimCateg);
        }

        return bimCateg;
    }

    private void saveBIMCategoryInCache(ModelType modelType, BIMEntityType bimEntity, BIMCategory category)
    {
        HashMap<BIMEntityType,BIMCategory> lstBIMCateg = this.lstCategCache.getOrDefault(modelType,null);
        if(lstBIMCateg==null)
        {
            lstBIMCateg=new HashMap<>();
            this.lstCategCache.put(modelType,lstBIMCateg);
        }
        lstBIMCateg.put(bimEntity,category);
    }

    private BIMCategory getBIMCategoryFromCache(ModelType modelType, BIMEntityType bimEntity)
    {
        BIMCategory bimCateg = null;
        HashMap<BIMEntityType,BIMCategory> lstBIMCateg = this.lstCategCache.getOrDefault(modelType,null);
        if(lstBIMCateg!=null)
            bimCateg=lstBIMCateg.getOrDefault(bimEntity,null);

        return bimCateg;
    }

    private void addBIMCategoryToCache(ModelType modelType, BIMEntityType bimEntity, BIMCategory bimCategory)
    {
        HashMap<BIMEntityType,BIMCategory> lstBIMCateg = this.lstCategCache.getOrDefault(modelType,null);
        lstBIMCateg.put(bimEntity,bimCategory);
    }

    //</editor-fold>

    //<editor-fold desc="Managing Unit">

    /***********************************************
     *                   Managing Unit
     * **********************************************/
    private HashMap<String,FlowProperty> lstFlowProperty;
    public HashMap<String,FlowProperty> getFlowPropertyByUnit()
    {
        if(lstFlowProperty!=null)
            return lstFlowProperty;

        String query = "SELECT u.name as unit,fp.id " +
                "FROM tbl_flow_properties fp inner join tbl_unit_groups ug on(fp.f_unit_group=ug.id) " +
                "inner join tbl_units u on(ug.f_reference_unit=u.id) " +
                "where u.name in (\"kg\",\"m2\",\"m3\") and fp.name in (\"Mass\",\"Area\",\"Volume\")";
        lstFlowProperty = new HashMap<>();

        List<Object[]> lst=em.createNativeQuery(query).getResultList();
        for(Object[] record:lst)
        {
            Long idFP = ((BigInteger)record[1]).longValue();
            FlowProperty fp = em.find(FlowProperty.class,idFP);

            lstFlowProperty.put(record[0].toString(),fp);
        }
        return lstFlowProperty;
    }

    public HashMap<Long,FlowPropertyFactor> getConversionByFlowProperty(Flow flow)
    {
        HashMap<Long,FlowPropertyFactor> lstConversion = new HashMap<>();

        for(FlowPropertyFactor record:flow.getFlowPropertyFactors())
        {
            lstConversion.put(DBUtil.getOLCAId(record.getFlowProperty()),record);
        }

        return lstConversion;
    }

    private Unit getLcaUnit(String unitName)
    {
        Unit unit = lstUnitCache.getOrDefault(unitName,null);
        if(unit!=null)
            return unit;

        //Try to find the category
        try
        {
            unit = (Unit) em.createQuery("Select unit from Unit unit where unit.name=:unitName").
                    setParameter("unitName", unitName).getSingleResult();
        }
        catch (NoResultException e)
        {
            e.printStackTrace();
            throw e;
        } //Not found

        lstUnitCache.put(unitName,unit);

        return unit;
    }

    private Unit getLcaUnit(IEDUnitDimension dimension)
    {
        return getLcaUnit(getDefaultUnit(dimension));
    }

    private FlowProperty getFlowProperty(IEDUnitDimension dimension)
    {
        FlowProperty flowProperty = lstFlowPropertyCache.getOrDefault(dimension,null);
        if(flowProperty!=null)
            return flowProperty;

        //TODO Make a class to manage unit
        String dimensionName = dimension.toString();
        if(dimension==IEDUnitDimension.Item)
            dimensionName="Number of items";
        if(dimension==IEDUnitDimension.VolumeByTime)
            dimensionName="Volume*time";

        //Try to find the category
        try
        {
            flowProperty = (FlowProperty) em.createQuery("Select flowProperty from FlowProperty flowProperty where flowProperty.name=:fpName").
                                            setParameter("fpName", dimensionName).getSingleResult();
        }
        catch (NoResultException e)
        {
            e.printStackTrace();
            throw e;
        } //Not found

        lstFlowPropertyCache.put(dimension,flowProperty);

        return flowProperty;
    }

    public FlowPropertyFactor createFlowPropertyFactor(Flow flow, FlowProperty fp, Double factor)
    {
        if(flow==null || fp==null || factor==null || factor==0)
            return null;

        FlowPropertyFactor flowPropertyFactor = new FlowPropertyFactor();
        flowPropertyFactor.setFlowProperty(fp);
        flowPropertyFactor.setConversionFactor(factor);

        flow.getFlowPropertyFactors().add(flowPropertyFactor);

        return  flowPropertyFactor;
    }

    private FlowProperty getFlowProperty(String unit)
    {
        FlowProperty flowProperty = null;

        //Try to find the category
        try
        {
            flowProperty = (FlowProperty) em.createQuery("Select flowProperty from Unit u, UnitGroup ug, " +
                    "FlowProperty flowProperty where u MEMBER of ug.units and flowProperty = ug.defaultFlowProperty " +
                    "and u.name=:unitName").
                    setParameter("unitName", unit).getSingleResult();

        }
        catch (NoResultException e)
        {
            System.out.println("[Error] Unit not found: "+(unit!=null?unit:"null"));
            e.printStackTrace();
            throw e;
        } //Not found

        return flowProperty;
    }

    //</editor-fold>
    //</editor-fold>

    private void initAutoLink()
    {
        this.lstAutoLink = new HashMap[BIMEntityType.values().length+1];
        for(BIMEntityType entityType: BIMEntityType.values())
        {
            HashMap<String,Middleground> map = new HashMap<>();
            this.lstAutoLink[entityType.ordinal()] = map;

            Query autoLinkQuery = em.createQuery("Select middle from Middleground middle where middle.entityType=:entityType",
                    Middleground.class);
            autoLinkQuery.setParameter("entityType",entityType.name());
            List<Middleground> results = autoLinkQuery.getResultList();

            for (Middleground m : results)
            {
                map.put(m.getName(), m);
            }
        }

        HashMap<String,Middleground> map = new HashMap<>();
        this.lstAutoLink[BIMEntityType.values().length] = map;

        Query autoLinkQuery = em.createQuery("Select middle from Middleground middle where middle.entityType is null",
                Middleground.class);
        List<Middleground> results = autoLinkQuery.getResultList();

        for (Middleground m : results)
        {
            map.put(m.getName(), m);
        }
    }

    public String getLogDetails()
    {
        String details="";

        int qtyProc=0;
        int qtyAutoLink=0;

        for(Map.Entry<BIMEntityType,HashMap<String,BIMProcess>> entry : lstBuffer.entrySet())
        {
            String key="Qty Null processes";
            if(entry.getKey()!=null)
                key="Qty "+entry.getKey().name()+" processes";

            int qty = entry.getValue().size();
            qtyProc+=qty;

            details+= key+":"+qty+";";
        }
        details+="Qty total processes:"+qtyProc+";";

        String lstExchanges="";
        for(Map.Entry<BIMEntityType,HashMap<Long,Exchange>> entry : lstAutoLinked.entrySet())
        {
            String key="Qty Null linked processes";
            if(entry.getKey()!=null)
                key="Qty "+entry.getKey().name()+" linked processes";

            int qty = entry.getValue().size();
            qtyAutoLink+=qty;

            details+= key+":"+qty+";";

            String separator="";
            for(Exchange exchange:entry.getValue().values())
            {
                lstExchanges+=separator+exchange.getId();
                separator=",";
            }
        }
        details+="Qty total auto linked proccesses:"+qtyAutoLink+";";
        details+="list autolink exchange:"+lstExchanges+";";

        return details;
    }
}

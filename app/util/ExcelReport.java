package util;

import models.LCA.LCIResultSet;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.builder.CompareToBuilder;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.*;
import org.openlca.core.model.ImpactCategory;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.io.*;
import java.math.BigInteger;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;

public class ExcelReport {
    private static final String QUERY_LCIA =
            "SELECT ic.name, ic.reference_unit, sum(qty) AS Value, mean, std,median," +
                    "quantilesTwoPointFive, quantilesFive, quantilesTwentyFive, quantilesSeventyFive, quantilesNinetyFive, quantilesNinetySevenPointFive " +
                    "FROM tbl_LCIResultSet INNER JOIN tbl_ULCIA as u ON tbl_LCIResultSet.id = u.resultSet_id " +
                    "left JOIN tbl_impact_categories ic ON u.impactCategory_id = ic.id  " +
                    "left join tbl_LCIGraph lg on (lg.impactCategory_id=u.impactCategory_id and parent_process_id=-1 and tbl_LCIResultSet.id=lg.resultSet_id) " +
                    "WHERE tbl_LCIResultSet.id = :resultsetId " +
                    "group by u.impactCategory_id " +
                    "ORDER BY ic.name";
    private static final String QUERY_INVENTORY_BY_PROCESS =
            "SELECT f.name as flowName, f.f_category as flowCateg, p.name AS InventoryProcess, " +
                    "p.f_category as processCateg, u.name AS InventoryUnit, sum(tbl_LCI.processScalar) as qty " +
                    "FROM tbl_LCIResultSet INNER JOIN tbl_LCI ON tbl_LCI.resultSet_id = tbl_LCIResultSet.id " +
                    "left JOIN tbl_flows f ON tbl_LCI.flow_id = f.id " +
                    "left JOIN tbl_categories ON f.f_category = tbl_categories.id " +
                    "left JOIN tbl_processes p ON tbl_LCI.process_id = p.id " +
                    "left JOIN tbl_categories AS categories_tbl ON p.f_category = categories_tbl.id " +
                    "left JOIN tbl_units u ON tbl_LCI.unit_id = u.id " +
                    "WHERE tbl_LCIResultSet.id = :resultsetId " +
                    "GROUP BY flowName,flowCateg, InventoryProcess,processCateg " +
                    "Order by flowName,flowCateg, InventoryProcess,processCateg";
    private static final String QUERY_INVENTORY =
            "SELECT tbl_flows.name AS Substance,tbl_flows.f_category as compartment, tbl_units.name AS UnitName," +
                    "SUM(tbl_LCI.processScalar) AS qty " +
                    "FROM tbl_LCIResultSet INNER JOIN tbl_LCI ON tbl_LCI.resultSet_id = tbl_LCIResultSet.id " +
                    "left JOIN tbl_flows ON tbl_LCI.flow_id = tbl_flows.id " +
                    "left JOIN tbl_categories ON tbl_flows.f_category = tbl_categories.id " +
                    "left JOIN tbl_units ON tbl_LCI.unit_id = tbl_units.id " +
                    "WHERE tbl_LCIResultSet.id = :resultsetId " +
                    "GROUP BY Substance,compartment";

    private static final String QUERY_CHARACTERIZED_INVENTORY ="";

    private static final String QUERY_SENSITIVITY =
            "SELECT c.name as impact, p.name as processName, p.f_category as processCategId, f.name as flowName, " +
                    "f.f_category as flowCategId, sensitivity,relativeSensitivity " +
                    "FROM (tbl_SLCIA as slcia left join tbl_exchanges as e on (slcia.exchange_id=e.id)) " +
                    "inner join tbl_impact_categories c on(slcia.impactCategory_id=c.id) " +
                    "left join tbl_flows as f on (e.f_flow=f.id) " +
                    "left join tbl_processes as p on(e.f_owner=p.id) " +
                    "WHERE slcia.resultset_id = :resultsetId " +
                    "order by c.name, relativeSensitivity desc";
    private static final String QUERY_IMPACT_BY_ENTITY_TYPE =
            "SELECT tbl_LCIGraph.process_id, tbl_LCIGraph.impactCategory_id, tbl_processes.name, " +
                    "SUM(tbl_LCIGraph.qty) as graphQty, SUM(tbl_LCIGraph.percent) as graphPercent, sum(uncertainty) as graphUncertainty " +
                    "FROM tbl_LCIGraph INNER JOIN tbl_BIMProcesses ON tbl_LCIGraph.process_id=tbl_BIMProcesses.process_id " +
                    "INNER JOIN tbl_processes ON tbl_processes.id=tbl_LCIGraph.process_id " +
                    "WHERE resultSet_id=:resultsetId AND tbl_BIMProcesses.bimEntityType=:entityType " +
                    "GROUP BY tbl_LCIGraph.process_id, tbl_LCIGraph.impactCategory_id " +
                    "ORDER BY tbl_processes.name, tbl_LCIGraph.process_id, tbl_LCIGraph.impactCategory_id";

    private static final String QUERY_IMPACT_OPERATIONAL_ENERGY =
            "SELECT \"0\" as process_id,lg.impactCategory_id, \"Operational energy\" as name, " +
                    "lg.qty as graphQty, lg.percent as graphPercent, lg.uncertainty as graphUncertainty " +
                    "FROM tbl_BIMProcesses bp inner join tbl_LCIGraph lg on(bp.process_id=lg.process_id)\n" +
                    "where bimEntityType=\"BIMUniformat\" and bimKey=\"G\" and lg.resultSet_id=:resultsetId";

    private static final String QUERY_QTO_BIMTYPE =
            "SELECT f.name, count(distinct e2.f_owner,e2.f_flow) QtyLink, sum(if(p.name=\"area\",value,0)) as area, " +
                    "sum(if(p.name=\"volume\",value,0)) as volume " +
                    "FROM tbl_LCIResultSet rs left join tbl_BIMFlows bf on(rs.idBimModel=bf.model_id) " +
                    "left join tbl_exchanges e on (bf.flow_id=e.f_flow) " +
                    "left join tbl_exchanges e2 on(e.f_flow=e2.f_flow) " +
                    "left join tbl_flows f on (f.id=bf.flow_id) " +
                    "left join tbl_parameters p on (p.f_owner=e2.f_owner) " +
                    "where e.is_input = 0 and bf.bimEntityType=\"BIMObjectType\" and e2.is_input=1 and rs.id=:resultsetId " +
                    "group by f.name " +
                    "order by f.name";

    private static final Integer WIDTH_NUMBER = 90;
    private static final Integer WIDTH_UNIT = 90;
    private static final Integer WIDTH_IMPACT_CATEGORY = 230;
    private static final Integer WIDTH_COMPARTMENT = 160;
    private static final Integer WIDTH_BIMOBJECT = 320;
    private static final Integer WIDTH_CATEGORY = 320;
    private static final Integer WIDTH_PROCESS = 320;
    private static final Integer WIDTH_FLOW = 320;

    private XSSFWorkbook workbook;
    private XSSFCellStyle styleNumber;
    private XSSFCellStyle styleNumberOptional;
    private XSSFCellStyle stylePercent;
    private XSSFCellStyle styleBold;
    private XSSFCellStyle styleUnderlineBold;
    private XSSFCellStyle styleUpperlineBold;
    private XSSFCellStyle styleUpperlineBoldNumber;
    private Long resultSetId;
    private LCIResultSet resultset;

    private List<ImpactCategory> impacts;
    private HashMap<Long, ImpactCategory> mapImpacts;
    private HashMap<Long, Integer> mapIndImpact;

    private columnHandler processCategHandler;
    private columnHandler flowCategHandler;
    private EntityManager em;
    private categoryCache categCache;

    private ByteArrayOutputStream byteArrayOutputStream;

    public ExcelReport(EntityManager em, Long resultSetId) {
        this.resultSetId = resultSetId;
        this.em = em;
        this.categCache = new categoryCache(em);
        resultset = (LCIResultSet) em.find(LCIResultSet.class, resultSetId);

        this.workbook = new XSSFWorkbook();
        initStyle();

        this.processCategHandler = new columnHandler() {
            public String processColumn(Object value) {
                return categCache.getFullCategoryAsString(((BigInteger) value).longValue(), false, true);
            }
        };

        this.flowCategHandler = new columnHandler() {
            public String processColumn(Object value) {
                return categCache.getFullCategoryAsString(((BigInteger) value).longValue(), false, true);
            }
        };

    }

    protected void initStyle() {
        XSSFDataFormat cellDataFormat = workbook.createDataFormat();
        XSSFFont bold = workbook.createFont();
        bold.setBold(true);

        styleNumber = workbook.createCellStyle();
        styleNumber.setDataFormat(cellDataFormat.getFormat("0.0000E+00"));
        styleNumber.setAlignment(HorizontalAlignment.RIGHT);

        stylePercent = workbook.createCellStyle();
        stylePercent.setDataFormat(cellDataFormat.getFormat("0.00000%"));
        stylePercent.setAlignment(HorizontalAlignment.RIGHT);

        styleBold = workbook.createCellStyle();
        styleBold.setFont(bold);

        styleUnderlineBold = workbook.createCellStyle();
        styleUnderlineBold.setBorderBottom(BorderStyle.THICK);
        styleUnderlineBold.setFont(bold);

        styleUpperlineBold = workbook.createCellStyle();
        styleUpperlineBold.setBorderTop(BorderStyle.THICK);
        styleUpperlineBold.setFont(bold);

        styleUpperlineBoldNumber = workbook.createCellStyle();
        styleUpperlineBoldNumber.setBorderTop(BorderStyle.THICK);
        styleUpperlineBoldNumber.setFont(bold);
        styleUpperlineBoldNumber.setDataFormat(cellDataFormat.getFormat("0.0000E+00"));
        styleUpperlineBoldNumber.setAlignment(HorizontalAlignment.RIGHT);


        styleNumberOptional = workbook.createCellStyle();
        styleNumberOptional.setDataFormat(cellDataFormat.getFormat("0.0000E+00"));
        styleNumberOptional.setAlignment(HorizontalAlignment.RIGHT);
    }

    public void generateReport() {
        XSSFSheet worksheet = null;
        columnTemplate[] columnsTemplate = null;

        //ic.name, ic.reference_unit, sum(qty) AS Value, mean, std,quantilesTwoPointFive, quantilesFive, quantilesTwentyFive, quantilesSeventyFive, quantilesNinetyFive, quantilesNinetySevenPointFive " +
        worksheet = workbook.createSheet("LCIA Non-Comparative");
        columnsTemplate = new columnTemplate[]{
                new columnTemplate("Impact Category", WIDTH_IMPACT_CATEGORY),
                new columnTemplate("Unit", WIDTH_UNIT),
                new columnTemplate("Value", WIDTH_NUMBER, CellType.NUMERIC, styleNumber),
                new columnTemplate("Mean", WIDTH_NUMBER, CellType.NUMERIC, styleNumber),
                new columnTemplate("SDT", WIDTH_NUMBER, CellType.NUMERIC, styleNumber),
                new columnTemplate("Median", WIDTH_NUMBER, CellType.NUMERIC, styleNumber),
                new columnTemplate("Q2.5", WIDTH_NUMBER, CellType.NUMERIC, styleNumber),
                new columnTemplate("Q5", WIDTH_NUMBER, CellType.NUMERIC, styleNumber),
                new columnTemplate("Q25", WIDTH_NUMBER, CellType.NUMERIC, styleNumber),
                new columnTemplate("Q75", WIDTH_NUMBER, CellType.NUMERIC, styleNumber),
                new columnTemplate("Q95", WIDTH_NUMBER, CellType.NUMERIC, styleNumber),
                new columnTemplate("Q97.5", WIDTH_NUMBER, CellType.NUMERIC, styleNumber)
        };
        generateSheet(worksheet, QUERY_LCIA, columnsTemplate);

        worksheet = workbook.createSheet("Impact by object type");
        int indRowBimObject = generateCrossSheet(worksheet, QUERY_IMPACT_BY_ENTITY_TYPE, "BIMObjectType") + 1;
        generateTotalRow(worksheet, indRowBimObject, 1, impacts.size());


        worksheet = workbook.createSheet("Impact by material");
        int indRowMaterial = generateCrossSheet(worksheet, QUERY_IMPACT_BY_ENTITY_TYPE, "BIMMaterial") + 1;
        generateUnknownMaterialRow(worksheet, indRowMaterial, 1, impacts.size(), indRowBimObject);
        generateTotalRow(worksheet, indRowMaterial + 1, 1, impacts.size());

        worksheet = workbook.createSheet("Inventory by unit process");
        columnsTemplate = new columnTemplate[]{
                new columnTemplate("Substance",WIDTH_FLOW),
                new columnTemplate("Compartment",WIDTH_COMPARTMENT),
                new columnTemplate("Process",WIDTH_PROCESS),
                new columnTemplate("Process Category", processCategHandler).setWidth(WIDTH_CATEGORY),
                new columnTemplate("Unit",WIDTH_UNIT),
                new columnTemplate("Value",WIDTH_NUMBER,CellType.NUMERIC,styleNumber)
        };
        generateSheetByCompartment(worksheet,QUERY_INVENTORY_BY_PROCESS,columnsTemplate,
                                    new int[]{0,1,2,3,4,5}, new int[]{1}, new int[]{0,1,2,3},new int[]{5});

        worksheet = workbook.createSheet("Inventory");
        columnsTemplate = new columnTemplate[]{
                new columnTemplate("Substance", WIDTH_FLOW),
                new columnTemplate("Compartment", WIDTH_COMPARTMENT),
                new columnTemplate("Unit", WIDTH_UNIT),
                new columnTemplate("Value", WIDTH_NUMBER,CellType.NUMERIC,styleNumber)
        };
        generateSheetByCompartment(worksheet,QUERY_INVENTORY,columnsTemplate,
                                    new int[]{0,1,2,3}, new int[]{1}, new int[]{0,1},new int[]{3});

        worksheet = workbook.createSheet("LCIA Sensitivity");
        columnsTemplate = new columnTemplate[]{
                new columnTemplate("Impact category", WIDTH_IMPACT_CATEGORY),
                new columnTemplate("Process", WIDTH_PROCESS),
                new columnTemplate("Process Category", processCategHandler).setWidth(WIDTH_CATEGORY),
                new columnTemplate("Flow", WIDTH_FLOW),
                new columnTemplate("Flow Category",flowCategHandler).setWidth(WIDTH_CATEGORY),
                new columnTemplate("Sensitivity", WIDTH_NUMBER, CellType.NUMERIC, styleNumber),
                new columnTemplate("Relative", WIDTH_NUMBER, CellType.NUMERIC, stylePercent)
        };
        generateSheet(worksheet, QUERY_SENSITIVITY, columnsTemplate);

        worksheet = workbook.createSheet("QTO by object type");
        columnsTemplate = new columnTemplate[]{
                new columnTemplate("Object type", WIDTH_IMPACT_CATEGORY),
                new columnTemplate("Qty link", CellType.NUMERIC).setWidth(WIDTH_NUMBER),
                new columnTemplate("Area (m2)", WIDTH_NUMBER, CellType.NUMERIC, styleNumber),
                new columnTemplate("Volume (m3)", WIDTH_NUMBER, CellType.NUMERIC, styleNumber)
        };
        generateSheet(worksheet,QUERY_QTO_BIMTYPE,columnsTemplate);
    }

    protected List<ImpactCategory> getImpacts() {
        if (impacts != null)
            return impacts;

        String sql = "SELECT DISTINCT icateg " +
                "FROM LCIA as l inner join l.resultSet as r inner join l.impactCategory as icateg " +
                "where r.id=:resultsetId " +
                "order by icateg.name";
        Query query = em.createQuery(sql, ImpactCategory.class);
        query.setParameter("resultsetId", resultSetId);

        impacts = (List<ImpactCategory>) query.getResultList();

        mapImpacts = new HashMap<>();
        mapIndImpact = new HashMap<>();
        int ind = 0;
        for (ImpactCategory icateg : impacts) {
            mapImpacts.put(icateg.getId(), icateg);
            mapIndImpact.put(icateg.getId(), ind);
            ind++;
        }

        return impacts;
    }

    protected List<Object[]> groupByCompartment(List<Object[]> data,int[] columnNeededInd, int[] compartmentInd,
                                                int[] groupbyInd, int[] sumColumn)
    {
        //TODO Check if it's could be replaced with a lambda expression

        SortedMap<objectIndex,Object[]> groupedData = new TreeMap<>();

        //Map sum collum to output
        int indSumColumn[] = new int[sumColumn.length];
        for(int i=0;i<sumColumn.length;i++)
        {
            indSumColumn[i]=ArrayUtils.indexOf(columnNeededInd,sumColumn[i]);
        }


        for (Object[] line: data)
        {
            //convert categ id to compartment name
            for (int ind:compartmentInd)
            {
                line[ind]=categCache.getCompartment(((BigInteger)line[ind]).longValue());
            }

            //create group key
            Object[] key = new Object[groupbyInd.length];
            for (int i=0;i< groupbyInd.length;i++)
            {
                key[i] = line[groupbyInd[i]];
            }
            objectIndex objInd=new objectIndex(key);

            //Get grouped line
            Object[] groupedValues = groupedData.getOrDefault(objInd,null);
            if(groupedValues==null)
            {
                groupedValues = new Object[columnNeededInd.length];
                //Create the new line (no need to sum column (0+X=X))
                for(int i=0;i<columnNeededInd.length;i++)
                {
                    groupedValues[i]=line[columnNeededInd[i]];
                }
            }
            else
            {
                //Sum collumn
                for(int i=0;i<sumColumn.length;i++)
                {
                    int indSumGroupedValue=indSumColumn[i];
                    if(indSumGroupedValue>=0)
                    {
                        groupedValues[indSumGroupedValue] = (Double)groupedValues[indSumGroupedValue]+(Double)line[sumColumn[i]];
                    }
                }
            }
            groupedData.put(objInd,groupedValues);

        }

        return new ArrayList<Object[]>(groupedData.values());
    }

    protected int generateCrossSheet(XSSFSheet worksheet,String sqlQuery,String entityType)
    {
        //init impacts category map
        getImpacts();

        worksheet.setDisplayZeros(false);

        generateCrossSheetHeader(worksheet,entityType);

        columnTemplate templateData = new columnTemplate("", CellType.NUMERIC, styleNumberOptional);

        Query query = em.createNativeQuery(sqlQuery);
        query.setParameter("resultsetId",resultSetId);
        query.setParameter("entityType",entityType);

        List<Object[]> lstRecord = query.getResultList();
        String idProcess=null;
        int iRow=1;
        XSSFRow row = null;
        for(Object[] iRecord:lstRecord)
        {
            if(!iRecord[0].toString().equals(idProcess))
            {
                iRow++;
                row = worksheet.createRow(iRow);
                row.createCell(0).setCellValue(iRecord[2].toString());
                idProcess=iRecord[0].toString();
            }

            Long idImpact = Long.parseLong(iRecord[1].toString(),10);
            Integer ind=mapIndImpact.getOrDefault(idImpact,-1);
            XSSFCell cell = row.createCell(ind.intValue()+1);
            columnTemplate.applyTemplate(cell,templateData,iRecord[3]);
        }


        //Create operational energy
        iRow++;
        generateEnergyRow(worksheet,iRow);

        return iRow;
    }

    protected void generateCrossSheetHeader(XSSFSheet worksheet, String title)
    {
        List<ImpactCategory> lstImpacts = getImpacts();

        XSSFRow impactRow = worksheet.createRow(0);
        XSSFCell headerCell = impactRow.createCell(0);
        headerCell.setCellStyle(styleBold);
        headerCell.setCellValue(title);

        XSSFRow impactUnitRow = worksheet.createRow(1);
        headerCell = impactUnitRow.createCell(0);
        headerCell.setCellStyle(styleUnderlineBold);
        headerCell.setCellValue("");

        setColumnWidth(worksheet,0, WIDTH_BIMOBJECT);

        int iCol = 1;
        for(ImpactCategory iImpact: lstImpacts)
        {
            setColumnWidth(worksheet,iCol, WIDTH_NUMBER);
            XSSFCell cellImpact = impactRow.createCell(iCol);
            cellImpact.setCellStyle(styleBold);
            cellImpact.setCellValue(iImpact.getName());

            XSSFCell cellImpactUnit = impactUnitRow.createCell(iCol);
            cellImpactUnit.setCellStyle(styleUnderlineBold);
            cellImpactUnit.setCellValue(iImpact.getReferenceUnit());
            iCol++;
        }

        worksheet.createFreezePane(0, 2);
    }

    protected int generateSheet(XSSFSheet worksheet,String sqlQuery,columnTemplate[] columnsTemplate)
    {
        Query query = em.createNativeQuery(sqlQuery);
        query.setParameter("resultsetId", resultSetId);

        return generateSheet(worksheet,query.getResultList(),columnsTemplate);
    }

    protected int generateSheetByCompartment(XSSFSheet worksheet,String sqlQuery,columnTemplate[] columnsTemplate,
                                             int[] columnNeededInd, int[] compartmentInd, int[] groupbyInd,
                                             int[] sumColumn)
    {
        Query query = em.createNativeQuery(sqlQuery);
        query.setParameter("resultsetId", resultSetId);

        List<Object[]> data=query.getResultList();
        data=groupByCompartment(data,columnNeededInd, compartmentInd, groupbyInd, sumColumn);

        return generateSheet(worksheet,data,columnsTemplate);
    }

    protected int generateSheet(XSSFSheet worksheet,List<Object[]> data, columnTemplate[] columnsTemplate)
    {
        createHeader(worksheet,columnsTemplate);

        int iRow = 1;

        for(Object[] iResult :data)
        {
            XSSFRow row = worksheet.createRow(iRow);

            int iCol = 0;
            for(Object iValue:iResult)
            {
                XSSFCell cell = row.createCell(iCol);

                columnTemplate colTemplate = null;
                if(columnsTemplate!=null && iCol<columnsTemplate.length)
                    colTemplate = columnsTemplate[iCol];

                columnTemplate.applyTemplate(cell,colTemplate,iValue);
                iCol++;
            }
            iRow++;
        }

        return iRow;
    }

    protected void createHeader(XSSFSheet worksheet, columnTemplate[] columnsTemplate)
    {
        XSSFRow row = worksheet.createRow(0);

        int i=0;
        for (columnTemplate iCol: columnsTemplate)
        {
            setColumnWidth(worksheet,i,iCol.getWidth());

            XSSFCell cell = row.createCell(i);
            if(iCol != null) cell.setCellValue(iCol.getName());
            cell.setCellStyle(styleUnderlineBold);
            i++;
        }

        worksheet.createFreezePane(0, 1);
        worksheet.setAutoFilter(CellRangeAddress.valueOf("A1:"+getCellAdress(columnsTemplate.length-1,0)));
    }


    protected void generateUnknownMaterialRow(XSSFSheet worksheet,int iRow, int colStart, int colEnd,int iRowTotalObjType)
    {
        XSSFRow row = worksheet.createRow(iRow);
        XSSFCellStyle style = null;
        for(int i=0;i<=colEnd;i++)
        {
            if(i==colStart)
            {
                style = styleNumber;
            }

            XSSFCell cell = row.createCell(i);
            if(style!=null)cell.setCellStyle(style);

            if(i>=colStart && i<=colEnd)
            {
                cell.setCellType(CellType.FORMULA);
                cell.setCellFormula("\'Impact by object type\'!"+getCellAdress(i,iRowTotalObjType)+
                                    "-sum("+getCellAdress(i,2)+":"+getCellAdress(i,iRow-1)+")");
            }
            else if(i==0)
            {
                cell.setCellType(CellType.STRING);
                cell.setCellValue("Unknown material");
            }

        }
    }

    protected void generateTotalRow(XSSFSheet worksheet,int iRow, int colStart, int colEnd)
    {
        XSSFRow row = worksheet.createRow(iRow);
        XSSFCellStyle style = styleUpperlineBold;
        for(int i=0;i<=colEnd;i++)
        {
            if(i==colStart)
            {
                style = styleUpperlineBoldNumber;
            }

            XSSFCell cell = row.createCell(i);
            cell.setCellStyle(style);

            if(i>=colStart && i<=colEnd)
            {
                cell.setCellType(CellType.FORMULA);
                cell.setCellFormula("sum("+getCellAdress(i,2)+":"+getCellAdress(i,iRow-1)+")");
            }
            else if(i==0)
            {
                cell.setCellType(CellType.STRING);
                cell.setCellValue("Total");
            }

        }
    }

    private List<Object[]> EnergyRowBuffer=null;

    protected List<Object[]> getEnergyData()
    {
        if(EnergyRowBuffer==null)
        {
            Query query= em.createNativeQuery(QUERY_IMPACT_OPERATIONAL_ENERGY);
            query.setParameter("resultsetId",resultSetId);

            EnergyRowBuffer = query.getResultList();
        }
        return EnergyRowBuffer;
    }

    protected void generateEnergyRow(XSSFSheet worksheet,int iRow)
    {
        columnTemplate templateData = new columnTemplate("", CellType.NUMERIC, styleNumberOptional);

        XSSFRow row = worksheet.createRow(iRow);
        XSSFCell titleCell = row.createCell(0);
        titleCell.setCellValue("Operational energy");

        List<Object[]> lstRecord = getEnergyData();
        if(lstRecord!=null)
        {
            for (Object[] iRecord : lstRecord)
            {
                Long idImpact = Long.parseLong(iRecord[1].toString(), 10);
                Integer ind = mapIndImpact.getOrDefault(idImpact, -1);
                XSSFCell cell = row.createCell(ind.intValue() + 1);
                columnTemplate.applyTemplate(cell, templateData, iRecord[3]);
            }
        }
    }

    private String getCellAdress(int iColl,int iRow)
    {
        String alpha="ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        int indSecondLetter = (iColl/26)-1;
        int indFirstLetter = iColl%26;

        return (indSecondLetter>0?alpha.charAt(indSecondLetter)+"":"")+alpha.charAt(indFirstLetter)+(iRow+1);
    }

    public ByteArrayInputStream getStream()
    {

        try {
            byteArrayOutputStream = new ByteArrayOutputStream();
            workbook.write(byteArrayOutputStream);
            return new ByteArrayInputStream(byteArrayOutputStream.toByteArray());
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        return null;
    }

    public void save(String filePath) throws IOException
    {
        File dir = new File(filePath).getParentFile();
        if(!dir.exists())
        {
            dir.mkdirs();
        }

        File file = new File(filePath);
        FileOutputStream fileOut = new FileOutputStream(file.getAbsolutePath());
        workbook.write(fileOut);
        fileOut.close();
    }

    public void close()
    {
        try
        {
            workbook.close();
            if(byteArrayOutputStream!=null)
            {
                byteArrayOutputStream.close();
            }
        }
        catch (Exception e){}
    }

    public static FileInputStream getReportFile(String report)
    {
        try {

            FileInputStream fileInputStream = new FileInputStream(report);
            return fileInputStream;
        } catch (FileNotFoundException e) {
            return null;
        }
    }

    private Double ratioPixel2Unit=null;
    protected void setColumnWidth(XSSFSheet worksheet,int iColumn, Integer width)
    {
        if(width==null)
            return;

        if(ratioPixel2Unit==null)
        {
            ratioPixel2Unit = new Double(worksheet.getColumnWidth(iColumn))/worksheet.getColumnWidthInPixels(iColumn);
        }
        int msWidth = (int)Math.round(width * ratioPixel2Unit) ;

        worksheet.setColumnWidth(iColumn,msWidth);
    }

    protected class objectIndex implements Comparable<objectIndex>
    {
        public Object[] getValues() {
            return values;
        }

        public void setValues(Object[] values) {
            this.values = values;
        }

        private Object[] values;
        public objectIndex(Object[] values)
        {
            this.values=values;
        }

        @Override
        public boolean equals(Object o) {

            if (o == this) return true;
            if(o==null) return false;
            if (!(o instanceof objectIndex)) {
                return false;
            }

            objectIndex objIndex = (objectIndex) o;

            return new EqualsBuilder().append(objIndex.values,this.values).isEquals();
        }

        @Override
        public int hashCode() {
            HashCodeBuilder hcb = new HashCodeBuilder(17, 37);

            if(this.values!=null)
            {
                for(Object val:values)
                    hcb.append(val);
            }

            return hcb.toHashCode();
        }


        @Override
        public int compareTo(objectIndex o)
        {
            if(o==null) return 1;
            return new CompareToBuilder().append(this.values,o.values).toComparison();
        }
    }
}

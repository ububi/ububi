/**---------------------------   
 * PROJECT: UBUBI
 * Auth:
 *   Mathieu Dupuis
 * Date: ETE 2017
 *
 * Copyright [2017] [Mathieu Dupuis]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. 
 *
 *--------------------------*/
package util;

import com.fasterxml.jackson.databind.JsonNode;
import models.LCA.ModelLCAType;
import play.db.Database;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.sql.*;
import java.time.Instant;

/**
 * Created by Daniel on 7/11/2017.
 */
public class InsertModelLCADatabase {

    private final String[] DATA_TYPE = new String[]{"lci","lcia","sens_lcia","lcia_unc","graph_lcia"};

    private long idModel;
    private String description;

    private Database database;
    private JsonNode jsonNode;

    private String[] distantFileURL = null;
    private String[] localFilePath = null;

    public InsertModelLCADatabase(JsonNode jsonNode, Database database)
    {
        this.jsonNode = jsonNode;
        this.database = database;
        this.idModel = jsonNode.get("idModel").asLong();
        this.description = jsonNode.get("description").asText();
        init(jsonNode);
    }

    public void startInsertingModelInDatabase() throws IOException,SQLException
    {
        java.util.Date begin = new java.util.Date();
        downloadDistantFiles();
        java.util.Date step2 = new java.util.Date();

        System.out.println("Download file from HDFS:"+ (step2.getTime()-begin.getTime()) +" ms");

        importFileInDatabase();
        java.util.Date end = new java.util.Date();
        System.out.println("Load data in database:"+ (end.getTime()-step2.getTime()) +" ms");

        removeImportedFiles();
    }

    public void removeImportedFiles()
    {
        for (String i:localFilePath)
            new File(i).delete();
    }

    private void downloadDistantFiles() throws IOException
    {
        for(ModelLCAType iModelLcaType:ModelLCAType.values())
        {
            int i=iModelLcaType.ordinal();

            URL website = new URL(this.distantFileURL[i]);
            ReadableByteChannel rbc = Channels.newChannel(website.openStream());
            FileOutputStream fos = new FileOutputStream(localFilePath[i]);
            fos.getChannel().transferFrom(rbc, 0, Long.MAX_VALUE);
            fos.close();
        }
    }

    private void importFileInDatabase() throws SQLException
    {
        long resultSetId = getGeneratedLCIResultSetId();
        Statement statement = database.getConnection().createStatement();

        String loadQuery = "LOAD DATA LOCAL INFILE '" + localFilePath[ModelLCAType.ULCIA.ordinal()] +
                "' INTO TABLE tbl_ULCIA FIELDS TERMINATED BY ','" + " LINES TERMINATED BY '\\n' " +
                "(@impactCategory_id, @mean, @std, @median, " +
                "@quantilesTwoPointFive, @quantilesFive, @quantilesTwentyFive, @quantilesSeventyFive, @quantilesNinetyFive, @quantilesNinetySevenPointFive, @resultSet_id) set " +
                "impactCategory_id=@impactCategory_id, mean=@mean, std=@std, median=@median, " +
                "quantilesTwoPointFive=@quantilesTwoPointFive, quantilesFive=@quantilesFive, " +
                "quantilesTwentyFive=@quantilesTwentyFive, quantilesSeventyFive=@quantilesSeventyFive, quantilesNinetyFive=@quantilesNinetyFive, " +
                "quantilesNinetySevenPointFive=@quantilesNinetySevenPointFive, resultSet_id="+resultSetId+";";
        statement.execute(loadQuery);

        loadQuery = "LOAD DATA LOCAL INFILE '" + localFilePath[ModelLCAType.LCI.ordinal()] +
                "' INTO TABLE tbl_LCI FIELDS TERMINATED BY ','" + " LINES TERMINATED BY '\\n' " +
                "(@flow_id, @totalQuantity, @unit_id, @process_id, @processContribution, " +
                "@processScalar, @resultSet_id) set " +
                "flow_id=@flow_id, totalQuantity=@totalQuantity, unit_id=@unit_id, process_id=@process_id, " +
                "processContribution=@processContribution, processScalar=@processScalar, resultSet_id="+resultSetId+";";
        statement.execute(loadQuery);

        loadQuery = "LOAD DATA LOCAL INFILE '" + localFilePath[ModelLCAType.LCIA.ordinal()] +
                "' INTO TABLE tbl_LCIA FIELDS TERMINATED BY ','" + " LINES TERMINATED BY '\\n' " +
                "(@impactCategory_id, @totalImpact, @process_id, @processContribution, " +
                "@unit, @resultSet_id) set " +
                "impactCategory_id=@impactCategory_id, totalImpact=@totalImpact, process_id=@process_id, processContribution=@processContribution, " +
                "unit=@unit, resultSet_id="+resultSetId+";";
        statement.execute(loadQuery);

        //inputType,exchange_id,impactCategory_id,sensitivity,relativeSensitivity,totalSensitivity,resultSet_id
        loadQuery = "LOAD DATA LOCAL INFILE '" + localFilePath[ModelLCAType.SLCIA.ordinal()] +
                "' INTO TABLE tbl_SLCIA FIELDS TERMINATED BY ','" + " LINES TERMINATED BY '\\n' " +
                "(@inputType, @exchange_id, @impactCategory_id, @sensitivity, @relativeSensitivity, @totalSensitivity) " +
                "set inputType=@inputType, impactCategory_id=@impactCategory_id, exchange_id=@exchange_id, " +
                "relativeSensitivity=@relativeSensitivity, sensitivity=@sensitivity,totalSensitivity=@totalSensitivity, " +
                "resultSet_id="+resultSetId+";";
        statement.execute(loadQuery);

        loadQuery = "LOAD DATA LOCAL INFILE '" + localFilePath[ModelLCAType.GRAPH.ordinal()] +
                "' INTO TABLE tbl_LCIGraph FIELDS TERMINATED BY ','" + " LINES TERMINATED BY '\\n' " +
                "(@impactCategory_id,@idNodeParent,@idNode,@parent_process_id,@process_id,@qty,@percent,@total,@uncertainty) set " +
                "impactCategory_id = @impactCategory_id,idNode = @idNode,idNodeParent = @idNodeParent," +
                "process_id = @process_id, parent_process_id=@parent_process_id, qty = @qty, percent = @percent, total = @total, uncertainty = @uncertainty, " +
                "resultSet_id="+resultSetId+";";
        statement.execute(loadQuery);
    }

    private long getGeneratedLCIResultSetId() throws SQLException
    {
        PreparedStatement preparedStatement = database.getConnection().prepareStatement("INSERT" +
                " INTO tbl_LCIResultSet (idBimModel, calc_time, description,jsonData) VALUES (?, ?, ?,?)", Statement.RETURN_GENERATED_KEYS);
        preparedStatement.setLong(1, this.idModel);
        preparedStatement.setTimestamp(2, Timestamp.from(Instant.now()));
        preparedStatement.setString(3, this.description);
        preparedStatement.setString(4, this.jsonNode.toString());

        int affectedRows = preparedStatement.executeUpdate();
        if (affectedRows == 0)
            throw new SQLException("Creating lciResultSet failed, no rows affected.");

        ResultSet generatedKeys = preparedStatement.getGeneratedKeys();
        if (generatedKeys.next())
            return generatedKeys.getLong(1);

        throw new SQLException("Creating lciResultSet failed, no ID obtained.");
    }

    private void init(JsonNode json)
    {
        int len = ModelLCAType.values().length;
        distantFileURL = new String[len];
        localFilePath = new String[len];

        for(ModelLCAType iModelLcaType : ModelLCAType.values())
        {
            int i = iModelLcaType.ordinal();
            distantFileURL[i]=proccessFileURL(json.get("file_"+DATA_TYPE[i]).asText());
            localFilePath[i]=DATA_TYPE[i]+".txt";
        }
    }

    private String proccessFileURL(String url)
    {
        String urlProcessed = url;

        if(urlProcessed.startsWith("\""))
        {
            urlProcessed = urlProcessed.substring(1);
        }

        if(urlProcessed.endsWith("\""))
        {
            urlProcessed = urlProcessed.substring(0,urlProcessed.length()-2);
        }

        return urlProcessed;
    }

}

package util;

import play.db.jpa.JPA;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import java.util.HashMap;

public class categoryCache
{
    protected static final String CATEGORY_SEPARATOR = "\\";

    protected HashMap<Long, String> mapCateg;
    protected EntityManager em;

    public categoryCache(EntityManager em)
    {
        this.em=em;
        this.mapCateg = new HashMap<>();
    }

    public String getFullCategoryAsString(Long idCateg, Boolean omitRootCategory, Boolean omitFirstSlash)
    {
        if(idCateg==null)
            return "";

        String fullCateg = mapCateg.getOrDefault(idCateg,null);

        if(fullCateg==null)
        {
            org.openlca.core.model.Category category = null;
            try {
                category = JPA.em().find(org.openlca.core.model.Category.class, idCateg);
            }
            catch (NoResultException e)
            {
                return "";
            }
            fullCateg = getFullCategoryAsString(category, "");
        }
        return formatCategory(fullCateg,omitRootCategory,omitFirstSlash);
    }

    protected String formatCategory(String fullCateg, Boolean omitRootCategory, Boolean omitFirstSlash)
    {
        if(fullCateg==null || fullCateg.length()==0)
            return fullCateg;

        String formatedFullCateg = fullCateg;
        if(omitRootCategory)
        {
            int indexOf = formatedFullCateg.indexOf(CATEGORY_SEPARATOR,1);
            if(indexOf>0)
                formatedFullCateg = formatedFullCateg.substring(indexOf);
        }

        if(omitFirstSlash && formatedFullCateg.startsWith(CATEGORY_SEPARATOR))
        {
            formatedFullCateg = formatedFullCateg.substring(1);
        }
        return formatedFullCateg;
    }

    protected String getFullCategoryAsString(org.openlca.core.model.Category category, String currentStringRepresentation)
    {
        if (category == null)
            return currentStringRepresentation;
        else
        {
            String fullCateg = getFullCategoryAsString(category.getCategory(), "\\" + category.getName() + currentStringRepresentation);
            mapCateg.put(category.getId(),fullCateg);
            return fullCateg;
        }
    }

    public String getCompartment(Long idCateg)
    {
        String compartment = getFullCategoryAsString(idCateg,true,true);
        int indSlash = compartment.indexOf(CATEGORY_SEPARATOR,1);;
        if(indSlash>0)
            compartment= compartment.substring(0,indSlash);
        return compartment;
    }

}

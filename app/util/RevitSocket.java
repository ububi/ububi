package util;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;

public class RevitSocket implements Runnable
{
    private Socket socket = null;
    private InputStream is;
    private OutputStream os;
    private String token="";
    private RevitSocketEvent onRegister;
    private RevitSocketEvent onDisconect;


    public RevitSocket(Socket socket, RevitSocketEvent onRegister, RevitSocketEvent onDisconect)
    {
        this.socket = socket;
        this.onRegister = onRegister;
        this.onDisconect = onDisconect;
    }

    public void sendMessage(String message) throws IOException
    {
        String toSend = message;
        byte[] toSendBytes = toSend.getBytes();
        int toSendLen = toSendBytes.length;

        //TODO To change in plugin and there, int is only 32 bit not 40 bit
        byte[] toSendLenBytes = new byte[5];
        toSendLenBytes[0] = (byte)(toSendLen & 0xff);
        toSendLenBytes[1] = (byte)((toSendLen >> 8) & 0xff);
        toSendLenBytes[2] = (byte)((toSendLen >> 16) & 0xff);
        toSendLenBytes[3] = (byte)((toSendLen >> 24) & 0xff);
        toSendLenBytes[4] = (byte)((toSendLen >> 32) & 0xff);
        os.write(toSendLenBytes);
        os.write(toSendBytes);
        os.flush();
    }

    public void selectPartInRevit(long bimKey) throws IOException
    {
        sendMessage(bimKey + "");
    }

    public void closeSocket()
    {
        try
        {
            onDisconect.onAction(token,this);
            if(socket!=null)socket.close();
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }

    public void run()
    {
        try
        {
            is = socket.getInputStream();
            os = socket.getOutputStream();

            // Receiving
            byte[] lenBytes = new byte[4];
            is.read(lenBytes, 0, lenBytes.length);
            int len = ((lenBytes[3] & 0xff) << 24) | ((lenBytes[2] & 0xff) << 16) | ((lenBytes[1] & 0xff) << 8) | (lenBytes[0] & 0xff);
            byte[] receivedBytes = new byte[len];
            is.read(receivedBytes, 0, len);
            token = new String(receivedBytes, 0, len);
            int result = onRegister.onAction(token,this);
            if(result!=0)
                closeSocket();
        }
        catch(Exception e)
        {
            e.printStackTrace();
            closeSocket();
        }

    }
}

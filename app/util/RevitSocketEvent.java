package util;

public interface RevitSocketEvent {
    public int onAction(String token, RevitSocket socket);
}

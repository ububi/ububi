/**---------------------------   
 * PROJECT: UBUBI
 * Auth:
 *   Mathieu Dupuis
 * Date: ETE 2017
 *
 * Copyright [2017] [Mathieu Dupuis]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. 
 *
 *--------------------------*/
package util;

/**
 * Created by mathieu on 1/27/2017.
 */
public class Spark
{

    private static String[] path={"calculate","getFile","projectVersions"};

    public static String getHost()
    {
        return Config.getConfig("spark.host");
    }

    public static String getPort()
    {
        return Config.getConfig("spark.port");
    }

    public static String getPrefix()
    {
        return Config.getConfig("spark.prefix");
    }

    public static String getURL()
    {
        String port = ":"+getPort();
        if(port == ":80" || port==":")
        {
            port ="";
        }

        return "http://"+getHost()+port;
    }

    public static String getURL(String path)
    {
        return getURL()+"/"+path;
    }

    public static String getServiceURL(SparkService services)
    {
        return getURL(path[services.ordinal()]);
    }
}

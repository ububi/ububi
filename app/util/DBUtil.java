package util;

import org.hibernate.proxy.HibernateProxy;
import org.hibernate.proxy.LazyInitializer;

public class DBUtil
{
    //Give the id without loading the object
    public static Long getOLCAId(org.openlca.core.model.AbstractEntity entity)
    {
        if (entity == null)
            return -1l;

        if (entity instanceof HibernateProxy)
        {
            LazyInitializer lazyInitializer = ((HibernateProxy) entity).getHibernateLazyInitializer();
            if (lazyInitializer.isUninitialized())
            {
                return (Long) lazyInitializer.getIdentifier();
            }
        }
        return entity.getId();
    }
}

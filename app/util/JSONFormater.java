/**---------------------------   
 * PROJECT: UBUBI
 * Auth:
 *   Mathieu Dupuis
 * Date: ETE 2017
 *
 * Copyright [2017] [Mathieu Dupuis]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. 
 *
 *--------------------------*/
package util;

import controllers.API.model.routes;
import models.LCA.*;
import org.hibernate.proxy.HibernateProxy;
import org.hibernate.proxy.LazyInitializer;
import org.openlca.core.model.*;
import org.openlca.core.model.Process;
import sun.text.resources.cldr.ti.FormatData_ti_ER;

import javax.persistence.Entity;
import javax.persistence.EntityNotFoundException;
import java.sql.ResultSet;
import java.util.HashMap;
import java.util.List;

/**
 * Created by mathieu on 10/13/2016.
 */
public abstract class JSONFormater<T>
{

    //<editor-fold desc="Formaters">
    //<editor-fold desc="BIMData Formaters">
    public final static JSONFormater<BIMModel> BIMModelFormater = new JSONFormater<BIMModel>()
    {
        @Override
        public String parse(BIMModel data)
        {
            String strData =
                            JSONUtil.parse("id",data.getId())+","+
                            JSONUtil.parse("name",data.getName())+","+
                            JSONUtil.parseDate("creationDate",data.getCreationDate(),"yyyy-MM-dd")+","+

                            JSONUtil.parse("idRootFlow",routes.Flow.getInfo(data.getIdRootFlow()).url())+","+
                            JSONUtil.parse("idRootProcess",routes.Process.getInfo(data.getIdRootProcess()).url())+","+
                            JSONUtil.parseEscapedString("BIMExchanges",routes.BIMModel.getExchanges(data.getId()).url())+","+
                            JSONUtil.parseEscapedString("BIMFlows",routes.BIMModel.getFlows(data.getId()).url())+","+
                            JSONUtil.parseEscapedString("BIMProcesses",routes.BIMModel.getProcesses(data.getId()).url())+","+
                            JSONUtil.parseEscapedString("BIMCategories",routes.BIMModel.getCategories(data.getId()).url());
            return strData;
        }
    };

    public final static JSONFormater<LCIResultSet> LCIResultSetFormater = new JSONFormater<LCIResultSet>()
    {
        @Override
        public String parse(LCIResultSet data) {

            String strData =
                    JSONUtil.parse("id",data.getId())+","+
                    JSONUtil.parseDate("calculationDate",data.getCalc_time(),"yyyy-MM-dd")+","+
                    JSONUtil.parse("description", data.getDescription());
            return strData;
        }
    };

    public final static JSONFormater<LCIGraph> LCIGraphFormater = new JSONFormater<LCIGraph>()
    {
        @Override
        public String parse(LCIGraph data) {

            String strData =
                    JSONUtil.parse("id",data.getId())+","+
                    JSONUtil.parse("idNode",data.getIdNode())+","+
                    JSONUtil.parse("idNodeParent",data.getIdNodeParent())+","+
                    JSONUtil.parse("qty",data.getQty())+","+
                    JSONUtil.parse("percent",data.getPercent())+","+
                    JSONUtil.parse("total",data.getTotal())+","+
                    JSONUtil.parse("uncertainty",data.getUncertainty())+","+
                    JSONUtil.parse("impactCategoryId",data.getImpactCategory().getId())+","+
                    JSONUtil.parse("impactCategoryName",data.getImpactCategory().getName())+","+
                    JSONUtil.parse("referenceUnit",data.getImpactCategory().getReferenceUnit())+","+
                    JSONUtil.parse("parentProcessId", DBUtil.getOLCAId(data.getParent_process())) + "," +


                    JSONUtil.parse("processId",DBUtil.getOLCAId(data.getProcess()))+","+
                    JSONUtil.parse("processName",data.getProcess().getName());
            return strData;
        }
    };

    public final static JSONFormater<BIMFlow> BIMFlowFormater = new JSONFormater<BIMFlow>()
    {
        @Override
        public String parse(BIMFlow data)
        {
            String strData =
                            JSONUtil.parse("id",data.getId())+","+
                            JSONUtil.parseBIMEntity(data.getBimEntity())+","+
                            JSONUtil.parseEscapedString("IdFlow",routes.Flow.getInfo(data.getIdFlow()).url());
            return strData;
        }
    };

    public final static JSONFormater<BIMExchange> BIMExchangeFormater = new JSONFormater<BIMExchange>()
    {
        @Override
        public String parse(BIMExchange data)
        {
            String strData =
                            JSONUtil.parse("id",data.getId())+","+
                            JSONUtil.parseBIMEntity(data.getBimEntity())+","+
                            JSONUtil.parseEscapedString("IdExchange",routes.Exchange.getInfo(data.getIdExchange()).url());
            return strData;
        }
    };

    public final static JSONFormater<BIMProcess> BIMProcessFormater = new JSONFormater<BIMProcess>()
    {
        @Override
        public String parse(BIMProcess data)
        {
            String strData =
                            JSONUtil.parse("id",data.getId())+","+
                            JSONUtil.parseBIMEntity(data.getBimEntity())+","+
                            JSONUtil.parse("idEntity",data.getIdProcess())+","+
                            JSONUtil.parseEscapedString("IdProcess",routes.Process.getInfo(data.getIdProcess()).url());
            return strData;
        }
    };

    public final static JSONFormater<FlowPropertyFactor> FlowPropertyFactorFormater = new JSONFormater<FlowPropertyFactor>() {
        @Override
        protected String parse(FlowPropertyFactor data) {
            if(data==null)
                return "";

            String strData = JSONUtil.parse("id",data.getId())+","+
                             JSONUtil.parse("conversionFactor",data.getConversionFactor())+","+
                             JSONUtil.parse("flowPropertyId",DBUtil.getOLCAId(data.getFlowProperty()))+","+
                             JSONUtil.parseJsonObject("flowProperty",FlowPropertyFormater.parse(data.getFlowProperty()));

            return strData;
        }
    };

    public final static JSONFormater<FlowProperty> FlowPropertyFormater = new JSONFormater<FlowProperty>() {
        @Override
        protected String parse(FlowProperty data) {
            if(data==null)
                return "";
            String strData =
                    JSONUtil.parseRootEntity(data) + "," +
                    JSONUtil.parse("type",data.getFlowPropertyType().name())+","+
                    JSONUtil.parse("unitGroupId",DBUtil.getOLCAId(data.getUnitGroup()))+","+
                    JSONUtil.parseJsonObject("unitGroup",UnitGroupWithDefaultUnitFormater.parse(data.getUnitGroup()));

            return strData;
        }
    };

    public final static JSONFormater<BIMCategory> BIMCategoryFormater = new JSONFormater<BIMCategory>()
    {
        @Override
        public String parse(BIMCategory data)
        {
            String strData =
                            JSONUtil.parse("id",data.getId())+","+
                            JSONUtil.parseBIMEntity(data.getBimEntity())+","+
                            JSONUtil.parseEscapedString("ModelType",data.getModelType().toString())+","+
                            JSONUtil.parseEscapedString("IdCategory",routes.Category.getInfo(data.getIdCategory()).url());
            return strData;
        }
    };
    //</editor-fold>

    public final static JSONFormater<Exchange> ExchangeFormater = new JSONFormater<Exchange>()
    {
        @Override
        public String parse(Exchange data)
        {
            String strData = "";
            if(data.getFlow()==null){
                /* TODO: Figure out why some of those are null */
                System.out.println(data.getId());
            } else {
                 long idFlow = DBUtil.getOLCAId(data.getFlow());
                 strData =
                        JSONUtil.parse("id", data.getId()) + "," +
                        JSONUtil.parse("isInput", data.isInput()) + "," +
                        JSONUtil.parse("flowId", idFlow) + "," +
                        JSONUtil.parse("flow", routes.Flow.getInfo(idFlow)) + "," +
                        JSONUtil.parse("producers", routes.Flow.getFlowProducers(idFlow)) + "," +
                        JSONUtil.parse("amountValue", data.getAmountValue()) + "," +
                        JSONUtil.parse("amountFormula", data.getAmountFormula()) + "," +
                        JSONUtil.parse("defaultProviderId", data.getDefaultProviderId()) + "," +
                        JSONUtil.parseEscapedString("defaultProvider", data.getDefaultProviderId() == 0 ? "" : routes.Process.getInfo(data.getDefaultProviderId()).url()) + "," +
                        JSONUtil.parseEscapedString("provider", routes.Exchange.getProvider(data.getId()).url()) + "," +
                        JSONUtil.parse("isAvoidedProduct", data.isAvoidedProduct()) + "," +
                        JSONUtil.parse("unitId", DBUtil.getOLCAId(data.getUnit())) + "," +
                        JSONUtil.parse("unit", routes.Unit.getInfo(DBUtil.getOLCAId(data.getUnit())).url());
            }
            return strData;
        }
    };

    public final static JSONFormater<Flow> FlowFormater = new JSONFormater<Flow>()
    {
        @Override
        public String parse(Flow data)
        {
            long defaultId=-1;
            try
            {
                defaultId=data.getReferenceFlowProperty().getUnitGroup().getReferenceUnit().getId();
            }
            catch (Exception e)
            {}

            String strData =
                            JSONUtil.parseCategorizedEntity(data) + "," +
                            JSONUtil.parse("producers",routes.Flow.getFlowProducers(data.getId()))+","+
                            JSONUtil.parse("consumers",routes.Flow.getFlowConsumers(data.getId()))+","+
                            JSONUtil.parseEscapedString("flowType", data.getFlowType().toString())+","+
                            JSONUtil.parse("defaultUnit",defaultId)+","+
                            JSONUtil.parse("units",routes.Flow.getUnit(data.getId()))+","+
                            JSONUtil.parse("unitGroups", routes.Flow.getGroupUnits(data.getId()))+ ","+
                            JSONUtil.parse("fullCategory", routes.Flow.getFullCategory(data.getId()));
            return strData;
        }
    };

    public final static JSONFormater<Category> CategoryFormater = new JSONFormater<Category>()
    {
        @Override
        public String parse(Category data)
        {
            Category categ=data.getCategory();
            String strData =
                            JSONUtil.parseRootEntity(data) + "," +
                            JSONUtil.parseEscapedString("modelType", data.getModelType().toString()) + "," +
                            JSONUtil.parseEscapedString("parentCategoryId", categ==null?"":routes.Category.getInfo(DBUtil.getOLCAId(data.getCategory())).url());
            return strData;
        }
    };

    public final static JSONFormater<Process> ProcessFormater = new JSONFormater<Process>()
    {
        @Override
        public String parse(Process data)
        {

            String location = LocationFormater.parse(data.getLocation());
            if("".compareTo(location)==0)
                location="null";
            else
                location="{"+location+"}";

            String strData =
                            JSONUtil.parseCategorizedEntity(data) + "," +
                            JSONUtil.parseEscapedString("processType", data.getProcessType()==null?null:data.getProcessType().toString()) + "," +
                            JSONUtil.parseEscapedString("defaultLocationMethod", data.getDefaultAllocationMethod()==null?null:data.getDefaultAllocationMethod().toString()) + "," +
                            JSONUtil.parse("infrastructure", data.isInfrastructureProcess()) + "," +
                            JSONUtil.parseEscapedString("documentation", data.getDocumentation()==null?null:routes.ProcessDocumentation.getInfo(DBUtil.getOLCAId(data.getDocumentation())).url()) + "," +
                            JSONUtil.parse("exchanges", routes.Process.getExchanges(data.getId()).url()) + "," +
                            JSONUtil.parse("parameters", routes.Process.getParameters(data.getId()).url()) + "," +
                            JSONUtil.parseEscapedString("BIMProcess",routes.BIMProcess.getInfoByProcess(data.getId()).url())+ "," +
                            "\"location\":"+location+","+
                            JSONUtil.parseEscapedString("kmz", data.getLocation()==null || data.getLocation().getKmz()==null || data.getLocation().getKmz().length==0?null:routes.Process.getKmz(data.getId()).url()) + "," +
                            JSONUtil.parseEscapedString("quantitativeReferenceId",data.getQuantitativeReference()==null?null:routes.Exchange.getInfo(DBUtil.getOLCAId(data.getQuantitativeReference())).url());
            return strData;
        }
    };

    public final static JSONFormater<ProcessDocumentation> ProcessDocumentationFormater = new JSONFormater<ProcessDocumentation>()
    {
        @Override
        public String parse(ProcessDocumentation data)
        {
            String strData =
                    JSONUtil.parse("id",data.getId())+","+
                    JSONUtil.parseDate("creationDate",data.getCreationDate(),"yyyy-MM-dd")+","+
                    JSONUtil.parseDate("valid_from",data.getValidFrom(),"yyyy-MM-dd")+","+
                    JSONUtil.parseDate("valid_until",data.getValidUntil(),"yyyy-MM-dd")+","+

                    JSONUtil.parse("geography",data.getGeography())+","+
                    JSONUtil.parse("technology",data.getTechnology())+","+
                    JSONUtil.parse("time",data.getTime())+","+
                    JSONUtil.parse("modeling_constant",data.getModelingConstants())+","+
                    JSONUtil.parse("data_treatment",data.getDataTreatment())+","+
                    JSONUtil.parse("sampling",data.getSampling())+","+

                    JSONUtil.parse("copyright",data.isCopyright())+ ","+

                    JSONUtil.parse("generator", data.getDataGenerator()==null?null:routes.Actor.getInfo(DBUtil.getOLCAId(data.getDataGenerator())).url())+ ","+
                    JSONUtil.parse("documentor", data.getDataDocumentor()==null?null:routes.Actor.getInfo(DBUtil.getOLCAId(data.getDataDocumentor())).url())+ ","+
                    JSONUtil.parse("publication", data.getPublication()==null?null:routes.Source.getInfo(DBUtil.getOLCAId(data.getPublication())).url());
            return strData;
        }
    };

    public final static JSONFormater<Parameter> ParameterFormater = new JSONFormater<Parameter>()
    {
        @Override
        public String parse(Parameter data)
        {
            String strData =
                    JSONUtil.parse("id",data.getId()) + "," +
                    JSONUtil.parse("name",data.getName()) + "," +
                    JSONUtil.parse("description",data.getDescription()) + "," +
                    JSONUtil.parse("source",data.getExternalSource()) + "," +
                    JSONUtil.parse("sourceType",data.getSourceType()) + "," +
                    JSONUtil.parseEscapedString("scope",data.getScope().toString()) + "," +
                    JSONUtil.parse("value",data.getValue()) + "," +
                    JSONUtil.parse("formula", data.getFormula());
            return strData;
        }
    };

    public final static JSONFormater<Unit> UnitFormater = new JSONFormater<Unit>()
    {
        @Override
        public String parse(Unit data)
        {
            String strData =
                    JSONUtil.parseRootEntity(data) + "," +
                    JSONUtil.parse("synonyms",data.getSynonyms())+","+
                    JSONUtil.parse("conversionFactor",data.getConversionFactor())+","+
                    JSONUtil.parse("unitGroup", routes.Unit.getUnitGroup(data.getId()));
            return strData;
        }
    };

    public final static JSONFormater<UnitGroup> UnitGroupFormater = new JSONFormater<UnitGroup>()
    {
        @Override
        public String parse(UnitGroup data)
        {
            return JSONUtil.parseRootEntity(data);
        }
    };

    public final static JSONFormater<UnitGroup> UnitGroupWithDefaultUnitFormater = new JSONFormater<UnitGroup>()
    {
        @Override
        public String parse(UnitGroup data)
        {
            return JSONUtil.parseRootEntity(data)+","+
                   JSONUtil.parse("defaultUnitId",DBUtil.getOLCAId(data.getReferenceUnit()))+","+
                   JSONUtil.parseJsonObject("defaultUnit",UnitFormater.parse(data.getReferenceUnit())) ;
        }
    };

    public final static JSONFormater<Source> SourceFormater = new JSONFormater<Source>()
    {
        @Override
        public String parse(Source data)
        {
            String strData =
                    JSONUtil.parseCategorizedEntity(data) + "," +
                    JSONUtil.parse("year",data.getYear()==null?0:data.getYear()) + "," +
                    JSONUtil.parse("text_reference",data.getTextReference());
            return strData;
        }
    };

    public final static JSONFormater<Actor> ActorFormater = new JSONFormater<Actor>()
    {
        @Override
        public String parse(Actor data)
        {
            String strData =
                    JSONUtil.parseCategorizedEntity(data) + "," +
                    JSONUtil.parse("telefax",data.getTelefax()) + "," +
                    JSONUtil.parse("website",data.getWebsite()) + "," +
                    JSONUtil.parse("address",data.getAddress()) + "," +
                    JSONUtil.parse("zipcode",data.getZipCode()) + "," +
                    JSONUtil.parse("email",data.getEmail()) + "," +
                    JSONUtil.parse("telephone",data.getTelephone()) + "," +
                    JSONUtil.parse("country",data.getCountry()) + "," +
                    JSONUtil.parse("city",data.getCity());
            return strData;
        }
    };

    public final static JSONFormater<Location> LocationFormater = new JSONFormater<Location>()
    {
        @Override
        public String parse(Location data)
        {

            if(data==null)
                return "";

            String strData =
                    JSONUtil.parseRootEntity(data) + "," +
                    JSONUtil.parse("longitude",data.getLongitude()) + "," +
                    JSONUtil.parse("latitude",data.getLatitude()) + "," +
                    JSONUtil.parse("code",data.getCode()) + "," +

                    JSONUtil.parseEscapedString("kmz",data.getKmz()==null || data.getKmz().length==0?null:routes.Location.getKmz(data.getId()).url());
            return strData;
        }
    };

    public final static JSONFormater<ImpactMethod> ImpactMethodFormater = new JSONFormater<ImpactMethod>()
    {
        @Override
        public String parse(ImpactMethod data)
        {
            return JSONUtil.parseCategorizedEntity(data);
        }
    };

    public final static JSONFormater<FlowHistory> FlowHistoryFormater = new JSONFormater<FlowHistory>()
    {
        @Override
        public String parse(FlowHistory data)
        {
            String strData =
                    JSONUtil.parse("id", data.getId()) + "," +
                    JSONUtil.parseEscapedString("refId", data.getRefId()) + "," +
                    JSONUtil.parse("name", data.getName()) + "," +
                    JSONUtil.parse("description", data.getDescription()) + "," +
                    JSONUtil.parse("categoryId", routes.Category.getInfo(data.getIdCategory()).url()) + "," +
                    JSONUtil.parse("version", data.getVersion()) + "," +
                    JSONUtil.parse("lastChange", data.getLastChange()) + "," +

                    JSONUtil.parse("producers",routes.Flow.getFlowProducers(data.getId()))+","+
                    JSONUtil.parse("consumers",routes.Flow.getFlowConsumers(data.getId()))+","+
                    JSONUtil.parseEscapedString("flowType", data.getFlowType().toString())+","+
                    JSONUtil.parse("units",routes.Flow.getUnit(data.getId()))+","+
                    JSONUtil.parse("fullCategory", routes.Flow.getFullCategory(data.getId()));
            return strData;
        }
    };

    public final static JSONFormater<ProcessHistory> ProcessHistoryFormater = new JSONFormater<ProcessHistory>()
    {
        @Override
        public String parse(ProcessHistory data)
        {
            String strData =
                    JSONUtil.parse("id", data.getId()) + "," +
                    JSONUtil.parseEscapedString("refId", data.getRefId()) + "," +
                    JSONUtil.parse("name", data.getName()) + "," +
                    JSONUtil.parse("description", data.getDescription()) + "," +
                    JSONUtil.parse("categoryId", routes.Category.getInfo(data.getIdCategory()).url()) + "," +
                    JSONUtil.parse("version", data.getVersion()) + "," +
                    JSONUtil.parse("lastChange", data.getLastChange()) + "," +

                    JSONUtil.parseEscapedString("processType", data.getProcessType()==null?null:data.getProcessType().toString()) + "," +
                    JSONUtil.parseEscapedString("defaultLocationMethod", data.getDefaultAllocationMethod()==null?null:data.getDefaultAllocationMethod().toString()) + "," +
                    JSONUtil.parse("infrastructure", data.isInfrastructureProcess()) + "," +
                    JSONUtil.parse("processType", data.getProcessType().toString()) + "," +
                    JSONUtil.parseEscapedString("documentation", routes.ProcessDocumentation.getInfo(data.getIdDocumentation()).url()) + "," +
                    JSONUtil.parse("exchanges", routes.Process.getExchanges(data.getId()).url()) + "," +
                    JSONUtil.parse("parameters", routes.Process.getParameters(data.getId()).url()) + "," +
                    JSONUtil.parseEscapedString("location", routes.Location.getInfo(data.getIdLocation()).url()) + "," +
                    JSONUtil.parseEscapedString("kmz", routes.Location.getKmz(data.getIdLocation()).url()) + "," +
                    JSONUtil.parseEscapedString("quantitativeReferenceId",routes.Exchange.getInfo(data.getIdQuantitativeReference()).url());
            return strData;
        }
    };

    public final static JSONFormater<ExchangeHistory> ExchangeHistoryFormater = new JSONFormater<ExchangeHistory>()
    {
        @Override
        public String parse(ExchangeHistory data)
        {
            String strData =
                    JSONUtil.parse("id",data.getId())+","+
                    JSONUtil.parse("isInput",data.isInput())+","+
                    JSONUtil.parse("flowId",data.getIdFlow())+","+
                    JSONUtil.parse("flow",routes.Flow.getInfo(data.getIdFlow()))+","+
                    JSONUtil.parse("producers",routes.Flow.getFlowProducers(data.getIdFlow()))+","+
                    JSONUtil.parse("amountValue",data.getAmountValue())+","+
                    JSONUtil.parse("amountFormula",data.getAmountFormula())+","+
                    JSONUtil.parse("defaultProviderId",data.getDefaultProviderId())+","+
                    JSONUtil.parseEscapedString("defaultProvider",data.getDefaultProviderId()==0?"":routes.Process.getInfo(data.getDefaultProviderId()).url())+","+
                    JSONUtil.parseEscapedString("provider",routes.Exchange.getProvider(data.getId()).url())+","+
                    JSONUtil.parse("isAvoidedProduct",data.isAvoidedProduct())+","+
                    JSONUtil.parse("unitId",data.getIdUnit())+","+
                    JSONUtil.parse("unit",routes.Unit.getInfo(data.getIdUnit()).url());
            return strData;
        }
    };

    //</editor-fold>

    protected JSONFormater(){}
    protected abstract String parse(T data);
    public String parseObject(T data)
    {
        return "{"+parse(data)+"}";
    }
}

package util;

import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.xssf.usermodel.XSSFCell;

import java.math.BigInteger;


public class columnTemplate
{
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public CellStyle getStyle() {
        return style;
    }

    public void setStyle(CellStyle style) {
        this.style = style;
    }

    public CellType getExcelType() {
        return excelType;
    }

    public void setExcelType(CellType excelType) {
        this.excelType = excelType;
    }

    public columnHandler getColHandler() {
        return colHandler;
    }

    public void setColHandler(columnHandler colHandler) {
        this.colHandler = colHandler;
    }

    public Integer getWidth() {
        return width;
    }

    private CellStyle style;
    private CellType excelType;
    private columnHandler colHandler;
    private Integer width;

    public columnTemplate(String name,Integer width, CellType excelType, CellStyle style, columnHandler colHandler)
    {
        this.name = name;
        this.excelType = excelType!=null?excelType:CellType.STRING;
        this.style = style;
        this.colHandler=colHandler;
        this.width=width;
    }

    public columnTemplate(String name, CellType excelType, CellStyle style, columnHandler colHandler) { this(name,null,excelType,style,colHandler); }

    public columnTemplate(String name,Integer width, CellType excelType, CellStyle style){this(name,width,excelType,style,null);}

    public columnTemplate(String name, CellType excelType, CellStyle style){this(name,excelType,style,null);}

    public columnTemplate(String name, columnHandler colHandler){this(name, null,null,colHandler);}

    public columnTemplate(String name, CellStyle style)
    {
        this(name,null,style);
    }

    public columnTemplate(String name, CellType excelType)
    {
        this(name,excelType,null);
    }

    public columnTemplate(String name, Integer width) { this(name, width,null,null); }

    public columnTemplate(String name) { this(name,null,null); }

    public columnTemplate setWidth(Integer width)
    {
        this.width=width;
        return this;
    }

    public static void applyTemplate(XSSFCell cell, columnTemplate colTemplate, Object value)
    {
        Object processValue = value;
        if(colTemplate!=null)
        {
            cell.setCellType(colTemplate.excelType != null ? colTemplate.excelType : CellType.STRING);
            if (colTemplate.style != null) cell.setCellStyle(colTemplate.style);
            if (colTemplate.colHandler!=null)
                processValue = colTemplate.colHandler.processColumn(processValue);
        }

        if(processValue!=null)
        {
            if(colTemplate!=null && colTemplate.excelType == CellType.NUMERIC)
            {
                if(processValue instanceof BigInteger)
                    cell.setCellValue(((BigInteger)processValue).longValue());
                else
                    cell.setCellValue((Double)processValue);
            }
            else
            {
                cell.setCellValue(processValue.toString());
            }
        }
    }

}

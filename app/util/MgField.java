package util;

public enum MgField
{
    flowName,
    unit,
    ref,
    kgbym3,
    kgbym2,
    refDensity,
    rValueIn,
    rValue,
    refRValue,
    thicknessMin,
    thicknessMax,
    indicator,
    entityType,
    skip;
}
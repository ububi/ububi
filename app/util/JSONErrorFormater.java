package util;

public class JSONErrorFormater<T> extends JSONFormater<ErrorWrapper<T>>
{
    JSONFormater<T> formater;
    public JSONErrorFormater(JSONFormater<T> formater)
    {
        this.formater=formater;
        if(this.formater==null)
            this.formater=new JSONFormater<T>()
            {
                @Override
                protected String parse(T data) {
                    return "";
                }
            };
    }

    protected String parse(ErrorWrapper<T> data)
    {
        if(data==null)
            return "";

        String strResult=data.getData()==null?"":formater.parse(data.getData());

        String error=parseError(data);
        if("".compareTo(error)!=0)
        {
            if("".compareTo(strResult)!=0)
                strResult+=",";

            strResult+="error:{"+ error +"}";
        }
        return strResult;
    }

    private String parseError(ErrorWrapper<T> data)
    {
        String error="";
        if(data!=null && data.getErrorCode()>0)
            error = JSONUtil.parse("code",data.getErrorCode())+","+
                    JSONUtil.parse("title",data.getErrorTitle())+","+
                    JSONUtil.parse("description",data.getErrorDescription());
        return error;
    }

    public String parseErrorOnly(ErrorWrapper<T> data)
    {
        String error = parseError(data);

        if("".compareTo(error)!=0)
            error+="{error:{"+error+"}}";

        return error;
    }

    public String parseDataOnly(T data)
    {
        return data==null?"":formater.parseObject(data);
    }

    public JSONFormater<T> getDataFormater()
    {
        return this.formater;
    }
}

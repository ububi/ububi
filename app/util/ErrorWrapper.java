package util;

public class ErrorWrapper<T>
{
    private int errorCode=0;
    private String errorTitle;
    private String errorDescription;
    private T data;

    public int getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(int errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorTitle() {
        return errorTitle;
    }

    public void setErrorTitle(String errorTitle) {
        this.errorTitle = errorTitle;
    }

    public String getErrorDescription() {
        return errorDescription;
    }

    public void setErrorDescription(String errorDescription) {
        this.errorDescription = errorDescription;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public ErrorWrapper() { this(null); }

    public ErrorWrapper(T data)
    {
        this(data,0,null,null);
    }

    public ErrorWrapper(T data,int errorCode, String errorTitle, String errorDescription)
    {
        this.errorCode = errorCode;
        this.errorTitle = errorTitle;
        this.errorDescription = errorDescription;
        this.data = data;
    }
}

package util;

import models.LCA.Middleground;

public class MiddleGroundTemplate
{
    private final static int NOT_IN_TEMPLATE=-1;

    private static FieldHandler[] lstFieldHandler;

    private MgField[] template;
    private int[] mapTemplate;

    public String getEntityType() {
        return entityType;
    }

    private String entityType;

    public MiddleGroundTemplate(MgField[] template){this(template,null);}

    public MiddleGroundTemplate(MgField[] template, String entityType)
    {
        initFieldHandler();

        this.template=template;
        this.mapTemplate = createReverseMap(this.template);
        this.entityType = entityType;
    }

    public boolean setField(Middleground middleground, MgField field, Object[] values)
    {
        //Skip is always at NOT_IN_TEMPLATE
        int ind=mapTemplate[field.ordinal()];
        if(ind!=NOT_IN_TEMPLATE && values.length>ind)
            return setField(middleground, field, values[ind]);

        return false;
    }

    public boolean setAllField(Middleground middleground, Object[] values)
    {
        boolean succeed=true;

        setEntityType(middleground);
        for(MgField field:template)
        {
            succeed &= setField(middleground,field,values);
        }
        return succeed;
    }

    public void setEntityType(Middleground middleground)
    {
        if(entityType!=null)middleground.setEntityType(entityType);
    }

    public int getFieldOffset()
    {
        return this.template.length;
    }

    public Object getField(MgField field, Object[] values){ return getField(field,values,null); }
    public Object getField(MgField field, Object[] values, Object defaultValue)
    {
        int indField = mapTemplate[field.ordinal()];
        if(indField==NOT_IN_TEMPLATE || mapTemplate[field.ordinal()]>= values.length)
            return defaultValue;
        else
            return values[field.ordinal()];
    }

    public Double getDoubleField(MgField field, Object[] values){ return getDoubleField(field,values,null); }
    public Double getDoubleField(MgField field, Object[] values, Double defaultValue)
    {
        Object value = getField(field, values, defaultValue);
        try
        {
            return value==null?null:Double.parseDouble(value.toString());
        }
        catch (NumberFormatException e) {}

        return defaultValue;
    }

    public Integer getIntegerField(MgField field, Object[] values){ return getIntegerField(field, values,null); }
    public Integer getIntegerField(MgField field, Object[] values, Integer defaultValue)
    {
        Object value = getField(field, values, defaultValue);

        try
        {
            return value==null?null:Integer.parseInt(value.toString(),10);
        }
        catch (NumberFormatException e) {}

        return defaultValue;
    }

    public String getStringField(MgField field, Object[] values){ return getStringField(field,values,null); }
    public String getStringField(MgField field, Object[] values, String defaultValue)
    {
        Object value = getField(field, values, defaultValue);
        if(value==null)
            return null;

        return value.toString();
    }

    public boolean contain(MgField field)
    {
        return mapTemplate[field.ordinal()]!=NOT_IN_TEMPLATE;
    }

    private boolean setField(Middleground middleground, MgField field, Object value)
    {
        return lstFieldHandler[field.ordinal()].setField(middleground,value);
    }

    private int[] createReverseMap(MgField[] template)
    {
        int[] mapTemplate =  new int[MgField.values().length];

        for(int i=0;i<mapTemplate.length;i++)
            mapTemplate[i]=NOT_IN_TEMPLATE;

        for(int i=0;i<template.length;i++)
        {
            if(template[i]!= MgField.skip)
            {
                mapTemplate[template[i].ordinal()]=i;
            }
        }

        return mapTemplate;
    }

    private void initFieldHandler() {
        if (lstFieldHandler == null)
        {
            lstFieldHandler = new FieldHandler[MgField.values().length];

            lstFieldHandler[MgField.flowName.ordinal()] = new StringFieldHandler(){ @Override protected void setTypedField(Middleground middleground, String value) {}};
            lstFieldHandler[MgField.unit.ordinal()] = new StringFieldHandler(){ @Override protected void setTypedField(Middleground middleground, String value) { middleground.setUnit(value); }};
            lstFieldHandler[MgField.ref.ordinal()] = new StringFieldHandler(){ @Override protected void setTypedField(Middleground middleground, String value) { middleground.setRef(value); }};
            lstFieldHandler[MgField.refDensity.ordinal()] = new StringFieldHandler(){ @Override protected void setTypedField(Middleground middleground, String value) { middleground.setRefDensity(value); }};
            lstFieldHandler[MgField.refRValue.ordinal()] = new StringFieldHandler(){ @Override protected void setTypedField(Middleground middleground, String value) { middleground.setRefRValue(value); }};
            lstFieldHandler[MgField.entityType.ordinal()] = new StringFieldHandler(){ @Override protected void setTypedField(Middleground middleground, String value) { middleground.setRefRValue(value); }};
            lstFieldHandler[MgField.skip.ordinal()] = new StringFieldHandler(){ @Override protected void setTypedField(Middleground middleground, String value) {}};

            lstFieldHandler[MgField.rValueIn.ordinal()] = new DoubleFieldHandler(){ @Override protected void setTypedField(Middleground middleground, Double value) {
                middleground.setRValueIn(value);
            }};
            lstFieldHandler[MgField.rValue.ordinal()] = new DoubleFieldHandler(){ @Override protected void setTypedField(Middleground middleground, Double value) {
                middleground.setRValue(value);
            }};
            lstFieldHandler[MgField.kgbym3.ordinal()] = new DoubleFieldHandler(){ @Override protected void setTypedField(Middleground middleground, Double value) { middleground.setKgbym3(value); }};
            lstFieldHandler[MgField.kgbym2.ordinal()] = new DoubleFieldHandler(){ @Override protected void setTypedField(Middleground middleground, Double value) { middleground.setKgbym2(value); }};
            lstFieldHandler[MgField.thicknessMin.ordinal()] = new DoubleFieldHandler(){ @Override protected void setTypedField(Middleground middleground, Double value) { middleground.setThicknessMin(value); }};
            lstFieldHandler[MgField.thicknessMax.ordinal()] = new DoubleFieldHandler(){ @Override protected void setTypedField(Middleground middleground, Double value) { middleground.setThicknessMax(value); }};

            lstFieldHandler[MgField.indicator.ordinal()] = new IntegerFieldHandler(){ @Override protected void setTypedField(Middleground middleground, Integer value) {
                middleground.setIndicator(value);
            }};
        }
    }

    protected abstract class FieldHandler<T>
    {
        public abstract boolean setField(Middleground middleground, Object value);
        protected abstract void setTypedField(Middleground middleground, T value);
    }

    protected abstract class StringFieldHandler extends FieldHandler<String>
    {
        @Override
        public boolean setField(Middleground middleground, Object value)
        {
            setTypedField(middleground,value==null?null:value.toString());
            return true;
        }
    }

    protected abstract class DoubleFieldHandler extends FieldHandler<Double>
    {
        @Override
        public boolean setField(Middleground middleground, Object value)
        {
            try
            {
                setTypedField(middleground,value==null || value.toString().trim().length()==0?
                              null:
                              Double.parseDouble(value.toString().replace(',','.')));
                return true;
            }
            catch (NumberFormatException e) {}

            return false;
        }
    }

    protected abstract class IntegerFieldHandler extends FieldHandler<Integer>
    {
        @Override
        public boolean setField(Middleground middleground, Object value)
        {
            try
            {
                setTypedField(middleground,value==null || value.toString().trim().length()==0?
                              null:
                              Integer.parseInt(value.toString(),10));
                return true;
            }
            catch (NumberFormatException e) {}

            return false;
        }
    }
}

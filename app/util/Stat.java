package util;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class Stat
{
    private HashMap<String,Long> lstTimer;

    private Long activeTimer=null;
    private String activeAction=null;

    public Stat()
    {
        this(null);
    }

    public Stat (String action)
    {
        lstTimer = new HashMap<>();
        activeTimer= new Date().getTime();
        activeAction=action;
    }

    public void stop()
    {
        changeAction(null);
    }

    public void changeAction(String action)
    {
        Long currentTimer= new Date().getTime();
        if(activeAction!=null)
        {
            Long sumTime = lstTimer.getOrDefault(activeAction,0l);
            sumTime+=currentTimer-activeTimer;
            lstTimer.put(activeAction,sumTime);
        }
        activeTimer=currentTimer;
        activeAction=action;
    }

    public void show()
    {
        stop();
        Long total=0l;
        System.out.println("Actions : Duration");
        System.out.println("--------------------------------------------------------------------");
        for(Map.Entry<String,Long> entry: lstTimer.entrySet())
        {
            System.out.println(entry.getKey()+" : "+entry.getValue()+" ms");
            total+=entry.getValue();
        }
        System.out.println("--------------------------------------------------------------------");
        System.out.println("Total : "+total+" ms");
    }
}

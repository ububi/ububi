/**---------------------------   
 * PROJECT: UBUBI
 * Auth:
 *   Mathieu Dupuis
 * Date: ETE 2017
 *
 * Copyright [2017] [Mathieu Dupuis]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. 
 *
 *--------------------------*/
package util;

import com.jcraft.jsch.*;

import java.io.*;

/**
 * Created by mathieu on 1/31/2017.
 *
 * Code from http://www.jcraft.com/jsch/examples/ScpFrom.java.html
 */
public class SCPClient
{
    private JSch sshClient;
    private Session session;

    public SCPClient()
    {
        sshClient = new JSch();
    }

    public void connect(String host, String user, String password, int port) throws JSchException
    {
        session=sshClient.getSession(user, host, port);
        session.setPassword(password);
        session.setConfig("StrictHostKeyChecking", "no");
        session.connect();
    }

    public void getFile(String remoteFile, String localFile) throws JSchException, IOException
    {
        if(session == null || !session.isConnected())throw new IOException("Client not connected.");

        FileOutputStream fos=null;
        try
        {
            String prefix=null;
            if(new File(localFile).isDirectory())
            {
                prefix=localFile+File.separator;
            }

            // exec 'scp -f rfile' remotely
            String command="scp -f "+remoteFile;
            Channel channel=session.openChannel("exec");
            ((ChannelExec)channel).setCommand(command);

            // get I/O streams for remote scp
            OutputStream out=channel.getOutputStream();
            InputStream in=channel.getInputStream();

            channel.connect();

            byte[] buf=new byte[1024];

            // send '\0'
            buf[0]=0; out.write(buf, 0, 1); out.flush();

            while(true)
            {
                int c=checkAck(in);
                if(c!='C')
                {
                    break;
                }

                // read '0644 '
                in.read(buf, 0, 5);

                //Read file size
                long filesize=0L;
                while(true)
                {
                    if(in.read(buf, 0, 1)<0)
                        break;  // error

                    if(buf[0]==' ')
                        break;

                    filesize=filesize*10L+(long)(buf[0]-'0');
                }

                //Read file name
                String file=null;
                for(int i=0;;i++)
                {
                    in.read(buf, i, 1);
                    if(buf[i]==(byte)0x0a)
                    {
                        file=new String(buf, 0, i);
                        break;
                    }
                }

                //System.out.println("filesize="+filesize+", file="+file);

                // send '\0'
                buf[0]=0; out.write(buf, 0, 1); out.flush();

                // read a content of lfile
                fos=new FileOutputStream(prefix==null ? localFile : prefix+file);
                int foo;
                while(true)
                {
                    if(buf.length<filesize)
                        foo=buf.length;
                    else
                        foo=(int)filesize;

                    foo=in.read(buf, 0, foo);
                    if(foo<0)
                        break;  // error

                    fos.write(buf, 0, foo);
                    filesize-=foo;
                    if(filesize==0L) break;
                }
                fos.close();
                fos=null;

                if(checkAck(in)==0)
                {
                    // send '\0'
                    buf[0] = 0;
                    out.write(buf, 0, 1);
                    out.flush();
                }
            }
            channel.disconnect();
        }
        catch(Exception e)
        {
            System.out.println(e);
            try{if(fos!=null)fos.close();}catch(Exception ee){}
        }
    }

    private int checkAck(InputStream in) throws IOException
    {
        int b=in.read();
        // b may be 0 for success,
        //          1 for error,
        //          2 for fatal error,
        //          -1
        if(b==0) return b;
        if(b==-1) return b;

        if(b==1 || b==2){
            StringBuffer sb=new StringBuffer();
            int c;
            do {
                c=in.read();
                sb.append((char)c);
            }
            while(c!='\n');
            if(b==1){ // error
                System.out.print(sb.toString());
            }
            if(b==2){ // fatal error
                System.out.print(sb.toString());
            }
        }
        return b;
    }

    public void disconnect()
    {
        session.disconnect();
        session=null;
    }
}

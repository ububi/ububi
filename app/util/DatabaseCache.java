/**---------------------------   
 * PROJECT: UBUBI
 * Auth:
 *   Mathieu Dupuis
 * Date: ETE 2017
 *
 * Copyright [2017] [Mathieu Dupuis]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. 
 *
 *--------------------------*/
package util;

import org.openlca.core.model.Flow;
import org.openlca.core.model.ImpactCategory;
import org.openlca.core.model.Process;
import org.openlca.core.model.Unit;

import javax.persistence.EntityManager;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by cydtr on 6/24/2017.
 */
public class DatabaseCache {

    private EntityManager entityManager;

    private Map<Long, ImpactCategory> impactCategoryCache;
    private Map<Long, Flow> flowCache;
    private Map<Long, Unit> unitCache;
    private Map<Long, Process> processCache;

    public DatabaseCache(EntityManager entityManager){
        this.entityManager = entityManager;
        impactCategoryCache = new HashMap<Long, ImpactCategory>();
        flowCache = new HashMap<Long, Flow>();
        unitCache = new HashMap<Long, Unit>();
        processCache = new HashMap<Long, Process>();
    }

    public ImpactCategory getImpactCategory(Long impactCategoryId){
        if (impactCategoryCache.containsKey(impactCategoryId)){
            return impactCategoryCache.get(impactCategoryId);
        } else{
            ImpactCategory impactCategory = (ImpactCategory)entityManager.createQuery("select ic from ImpactCategory ic where ic.id = :icId")
                    .setParameter("icId", impactCategoryId)
                    .getSingleResult();
            impactCategoryCache.put(impactCategoryId, impactCategory);
            return impactCategory;
        }
    }

    public Flow getFlow(Long flowId){
        if (flowCache.containsKey(flowId)){
            return flowCache.get(flowId);
        } else{
            Flow flow = (Flow)entityManager.createQuery("select f from Flow f where f.id = :flowId")
                    .setParameter("flowId", flowId)
                    .getSingleResult();
            flowCache.put(flowId, flow);
            return flow;
        }
    }

    public Unit getUnit(Long unitId){
        if (unitCache.containsKey(unitId)){
            return unitCache.get(unitId);
        } else {
            Unit unit = (Unit)entityManager.createQuery("select u from Unit u where u.id = :unitId")
                    .setParameter("unitId", unitId)
                    .getSingleResult();
            unitCache.put(unitId, unit);
            return unit;
        }
    }

    public Process getProcess(Long processId){
        if (processCache.containsKey(processId)) {
            return processCache.get(processId);
        } else {
            Process process = (Process)entityManager.createQuery("select p from Process p where p.id = :processId")
                    .setParameter("processId", processId)
                    .getSingleResult();
            processCache.put(processId, process);
            return process;
        }
    }
}

/**---------------------------   
 * PROJECT: UBUBI
 * Auth:
 *   Mathieu Dupuis
 * Date: ETE 2017
 *
 * Copyright [2017] [Mathieu Dupuis]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. 
 *
 *--------------------------*/
package util;

import models.LCA.BIMEntity;
import controllers.API.model.routes;
import org.hibernate.proxy.HibernateProxy;
import org.hibernate.proxy.LazyInitializer;
import org.openlca.core.model.CategorizedEntity;
import org.openlca.core.model.RootEntity;
import play.api.mvc.Call;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by mathieu on 10/13/2016.
 */
public class JSONUtil
{
    public static String getNullValue(String key)
    {
        return "\""+key+"\":null";
    }

    public static String parse(String key, String value)
    {
        if(value==null)
            return JSONUtil.parseEscapedString(key, null);
        return parseEscapedString(key, escapeValue(value));
    }

    private static String escapeValue(String value)
    {
        return value.replaceAll("\"","\\\\\"").replaceAll("\n","\\\\n").replaceAll("\r","");
    }

    public static String parse(String key, long value)
    {
        return "\""+key+"\":"+value;
    }

    public static String parse(String key, double value)
    {
        return "\""+key+"\":"+value;
    }

    public static String parse(String key, boolean value)
    {
        return "\""+key+"\":"+(value?"true":"false");
    }

    public static String parse(String key, Call value)
    {
        if(value==null)
            return escapeValue(key);

        return "\""+key+"\":\""+value.url()+"\"";
    }

    public static String parseJsonObject(String key, String jsonObjectContent)
    {
        String value = jsonObjectContent;
        if(value==null || "".compareTo(value)==0)
            value="null";
        else
            value="{"+value+"}";

        return "\""+key+"\":"+value;
    }

    public static String parseEscapedString(String key, String value)
    {
        if(value==null)
            return getNullValue(key);
        return "\""+key+"\":\""+value+"\"";
    }

    public static String parseDate(String key, Date value, String format)
    {
        if(value==null)
            return getNullValue(key);
        SimpleDateFormat dt1 = new SimpleDateFormat(format);
        return parseEscapedString(key, dt1.format(value));
    }

    public static String parseBIMEntity(BIMEntity value)
    {
        if(value==null)
            return getNullValue("BIMKey") + "," +
                    getNullValue("BIMEntityType");

        return JSONUtil.parse("BIMKey", value.getBimKey()) + "," +
               JSONUtil.parseEscapedString("BIMEntityType", value.getBimEntityType().toString());
    }

    public static String parseRootEntity(RootEntity value)
    {
        if(value==null)
            return parse("id",-1) + "," +
                    getNullValue("refId") + "," +
                    getNullValue("name") + "," +
                    getNullValue("description");

        return JSONUtil.parse("id", value.getId()) + "," +
               JSONUtil.parseEscapedString("refId", value.getRefId()) + "," +
               JSONUtil.parse("name", value.getName()) + "," +
               JSONUtil.parse("description", value.getDescription());
    }

    public static String parseCategorizedEntity(CategorizedEntity value)
    {
        if(value==null)
            return JSONUtil.parseRootEntity(null) + "," +
                    getNullValue("categoryId") + "," +
                    getNullValue("version") + "," +
                    getNullValue("lastChange");

        return JSONUtil.parseRootEntity(value) + "," +
                JSONUtil.parse("categoryId", value.getCategory()==null?null:routes.Category.getInfo(DBUtil.getOLCAId(value.getCategory())).url()) + "," +
                JSONUtil.parse("version", value.getVersion()) + "," +
                JSONUtil.parse("lastChange", value.getLastChange());
    }

    public static String parseArray(String key, Iterable lst, JSONFormater formater)
    {
        StringBuilder sb = new StringBuilder();
        String separator = "";
        sb.append("\""+key+"\":[");

        if(lst!=null)
        {
            for (Object obj : lst)
            {
                sb.append(separator);
                separator = ",";
                sb.append(formater.parseObject(obj));
            }
        }
        sb.append("]");

        return sb.toString();
    }
}

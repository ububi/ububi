package util;

import play.mvc.Http;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;

public class RevitLink implements Runnable
{
    private static RevitLink revitLink=null;
    private static final String TokenSessionKey="RevitToken";

    private HashMap<String, RevitSocket> socketMap=new HashMap<String, RevitSocket>();
    private ServerSocket serverSocket = null;

    private RevitSocketEvent onRegister;
    private RevitSocketEvent onDisconnect;

    private RevitLink(int port)
    {
        try
        {
            serverSocket = new ServerSocket(port);
            onRegister = new RevitSocketEvent() {
                @Override
                public int onAction(String token, RevitSocket socket)
                {
                    if(socketMap.containsKey(token))
                    {
                        RevitSocket tmpSocket = socketMap.getOrDefault(token,null);
                        if(tmpSocket==null)
                        {
                            socketMap.replace(token, socket);
                            return 0; //Ok
                        }
                        else
                            return 2; //Token already use
                    }
                    else
                        return 1; //Token not exist
                }
            };

            onDisconnect = new RevitSocketEvent() {
                @Override
                public int onAction(String token, RevitSocket socket)
                {
                    if(token!=null)
                    {
                        socketMap.remove(token);
                    }
                    return 0;
                }
            };
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }

    @Override
    public void run()
    {
        try
        {
            while (true)
            {
                Socket socket = serverSocket.accept();
                RevitSocket tmp = new RevitSocket(socket,onRegister,onDisconnect);
                Thread thread = new Thread(tmp);
                thread.start();
            }
        }
        catch (IOException e)
        {
            e.printStackTrace();
            revitLink = null;
        }
    }

    private static void init()
    {
        if(revitLink!=null)
            return;

        revitLink = new RevitLink(Integer.parseInt(util.Config.getConfig("revitlink.port")));
        Thread thread = new Thread(revitLink);
        thread.start();
    }

    private static String getSessionToken(Http.Session session)
    {
        init();
        String token = session.getOrDefault(TokenSessionKey,null);
        return token;
    }


    public static String getToken(Http.Session session)
    {
        init();
        String token = getSessionToken(session);

        if(token==null || "".compareTo(token)==0 || !revitLink.socketMap.containsKey(token))
        {
            token = createNewToken(session);
        }
        return token;
    }

    public static RevitSocket getSocket(Http.Session session)
    {
        init();
        String token = getSessionToken(session);
        return revitLink.socketMap.getOrDefault(token,null);
    }

    public static String createNewToken(Http.Session session)
    {
        init();
        String token = getSessionToken(session);

        //Close old socket if it exist
        if(token==null || "".compareTo(token)==0)
        {
            RevitSocket socket = revitLink.socketMap.getOrDefault(token,null);
            if(socket!=null)
            {
                socket.closeSocket();
                revitLink.socketMap.remove(token);
            }
        }

        //Create a new token
        RevitSocket revitSocket = null;

        token = randomAlphaNumeric(5);
        while(revitLink.socketMap.containsKey(token))
            token = randomAlphaNumeric(5);

        revitLink.socketMap.put(token, null);
        session.put(TokenSessionKey,token);

        return token;
    }

    /**
     * Generate a alphanumeric pseudo-random string
     * @param count --> length of the string to generate
     * @return the random string (AKA token)
     */
    private static String randomAlphaNumeric(int count)
    {
        final String ALPHA_NUMERIC_STRING = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        StringBuilder builder = new StringBuilder();
        while (count-- != 0) {
            int character = (int)(Math.random()*ALPHA_NUMERIC_STRING.length());
            builder.append(ALPHA_NUMERIC_STRING.charAt(character));
        }
        return builder.toString();
    }


}

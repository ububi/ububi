/**---------------------------   
 * PROJECT: UBUBI
 * Auth:
 *   Mathieu Dupuis
 * Date: ETE 2017
 *
 * Copyright [2017] [Mathieu Dupuis]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. 
 *
 *--------------------------*/
package controllers;

		import play.db.jpa.JPAApi;
		import play.mvc.Controller;
		import play.mvc.Result;

		import javax.inject.Inject;
		import javax.persistence.EntityManager;
		import java.io.PrintWriter;
		import java.io.StringWriter;

/**
 * Created by mathieu on 8/28/2016.
 */
public abstract class UBUBIController extends Controller
{
	public final static String EMPTY_REST_RESULT="{}";
	private final String EXCEPTION_REST_RESULT="{\"exception\":\"%s\"}";

	protected JPAApi bd;

	@Inject
	public UBUBIController(JPAApi api) {
		bd = api;
	}

	protected Result jsonResult(Result httpResponse) {
		return httpResponse.as("application/json");
	}

	/**
	 * Formats an exception for usage by the Javascript API.
	 * Currently displays the Strack Trace, but can be changed if this is a security concern.
	 *
	 * Can be used like this:
	 *
	 * try {
	 *
	 *     //Error-prone code
	 * } Catch(Exception e) {
	 *     return new CompletableFuture<Result>().completedFuture(jsonResult(exception(e)));
	 * }
	 *
	 * @param underlyingException The exception that needs to be converted into a result
	 * @return The result containing the exception in a JSON-formatted string.
	 */
	protected Result exception(Exception underlyingException){
		StringWriter sw = new StringWriter();
		underlyingException.printStackTrace(new PrintWriter(sw));
		String exceptionAsString = sw.toString();
		return internalServerError(String.format(EXCEPTION_REST_RESULT, exceptionAsString.toString()));
	}
}

/**---------------------------   
 * PROJECT: UBUBI
 * Auth:
 *   Mathieu Dupuis
 * Date: ETE 2017
 *
 * Copyright [2017] [Mathieu Dupuis]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. 
 *
 *--------------------------*/
package controllers.Web;

import controllers.API.RevitSocketLink;
import controllers.UBUBIController;
import models.LCA.BIMModel;
import play.db.jpa.JPA;
import play.db.jpa.JPAApi;

import play.db.jpa.Transactional;
import play.mvc.Http;
import play.mvc.Result;
import util.JSONUtil;
import util.RevitLink;
import views.html.*;

import javax.inject.Inject;
import java.sql.PreparedStatement;
import java.sql.ResultSet;


public class Application extends UBUBIController
{
    @Inject
    public Application(JPAApi api) {
        super(api);
    }

    public Result index() {
        return ok(index.render());
    }

    public Result devZone() {
        return ok(devZone.render());
    }

    public Result client()
    {
        String revitToken = RevitLink.getToken(session());
        return ok(client.render(revitToken));
    }

    @Transactional
    public Result results(Long idModel) {
        BIMModel model = JPA.em().find(BIMModel.class,idModel);
        return ok(results.render(model.getName(), (int)idModel.intValue()));
    }

}

/**---------------------------   
 * PROJECT: UBUBI
 * Auth:
 *   Mathieu Dupuis
 * Date: ETE 2017
 *
 * Copyright [2017] [Mathieu Dupuis]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. 
 *
 *--------------------------*/
package controllers.Web;

import controllers.Web.routes;
import controllers.UBUBIController;
import play.db.jpa.JPAApi;
import play.mvc.Result;
import play.routing.JavaScriptReverseRouter;

import javax.inject.Inject;


/**
 * Created by mathieu on 11/05/2016.
 */
public class Router extends UBUBIController
{
    @Inject
    public Router(JPAApi api)
    {
        super(api);
    }
    
    public Result jsRouter()
    {
        return ok(JavaScriptReverseRouter.create("jsWeb",
                routes.javascript.Application.client(),
                routes.javascript.Application.devZone(),
                routes.javascript.Application.results(),
                routes.javascript.Application.index()
        )).as("text/javascript");
    }
}

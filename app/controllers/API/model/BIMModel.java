/**---------------------------   
 * PROJECT: UBUBI
 * Auth:
 *   Mathieu Dupuis
 * Date: ETE 2017
 *
 * Copyright [2017] [Mathieu Dupuis]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. 
 *
 *--------------------------*/
package controllers.API.model;

import controllers.API.Spark;
import controllers.API.model.generic.DataController;
import controllers.UBUBIController;
import javafx.collections.transformation.SortedList;
import models.LCA.BIMProcess;
import util.JSONFormater;
import util.JSONUtil;
import models.LCA.*;
import play.db.jpa.JPA;
import play.db.jpa.JPAApi;
import play.db.jpa.Transactional;
import play.mvc.Result;
import util.Stat;

import javax.inject.Inject;
import javax.persistence.EntityGraph;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import java.math.BigInteger;
import java.util.*;
import java.util.concurrent.CompletionStage;

/**
 * Created by mathieu on 10/12/2016.
 */
public class BIMModel extends UBUBIController
{
    @Inject
    private Spark spark;

    @Inject
    public BIMModel(JPAApi api)
    {
        super(api);
    }

    @Transactional(readOnly = true)
    public Result getInfo(Long id)
    {
        models.LCA.BIMModel returnVal = JPA.em().find(models.LCA.BIMModel.class, id);
        if (returnVal == null)
            return jsonResult(notFound(EMPTY_REST_RESULT));

        return jsonResult(ok(JSONFormater.BIMModelFormater.parseObject(returnVal)));
    }

    @Transactional(readOnly = true)
    public Result all()
    {
        List<models.LCA.BIMModel> lstVal = null;
        try
        {
            Query query = JPA.em().createQuery("Select m from BIMModel m");
            lstVal = query.getResultList();
        }
        catch (NoResultException e) {}

        StringBuilder sb = new StringBuilder();
        sb.append("{");
        sb.append(JSONUtil.parseArray("lst",lstVal,JSONFormater.BIMModelFormater));
        sb.append("}");

        return jsonResult(ok(sb.toString()));
    }

    @Transactional(readOnly = true)
    public Result getFlows(Long idModel)
    {
        models.LCA.BIMModel returnVal = JPA.em().find(models.LCA.BIMModel.class, idModel);
        if (returnVal == null)
            return jsonResult(notFound());

        return jsonResult(ok("{"+JSONUtil.parseArray("lst",returnVal.getFlows(),JSONFormater.BIMFlowFormater)+"}"));
    }

    @Transactional(readOnly = true)
    public Result getFlow(Long idModel, Long idFlow)
    {
        BIMFlow flow=null;
        try
        {
            flow = (BIMFlow) JPA.em().createQuery("Select bf from BIMModel bm inner join bm.flows bf where bm.id=:idModel and bf.idFlow=:idFlow").
                    setParameter("idModel", idModel).setParameter("idFlow", idFlow).getSingleResult();
        }
        catch (NoResultException e)
        {
            return jsonResult(notFound());
        }

        return jsonResult(ok(JSONFormater.BIMFlowFormater.parseObject(flow)));
    }


    @Transactional(readOnly = true)
    public Result getExchanges(Long idModel)
    {
        models.LCA.BIMModel returnVal = JPA.em().find(models.LCA.BIMModel.class, idModel);
        if (returnVal == null)
            return jsonResult(notFound());
        return jsonResult(ok("{"+JSONUtil.parseArray("lst",returnVal.getExchanges(),JSONFormater.BIMExchangeFormater)+"}"));
    }

    @Transactional(readOnly = true)
    public Result getExchange(Long idModel, Long idExchange)
    {
        BIMExchange exchange=null;
        try
        {
            exchange = (BIMExchange) JPA.em().createQuery("Select be from BIMModel bm inner join bm.exchanges be where bm.id=:idModel and be.idExchange=:idExchange").
                        setParameter("idModel", idModel).setParameter("idExchange", idExchange).getSingleResult();
        }
        catch (NoResultException e)
        {
            return jsonResult(notFound());
        }

        return jsonResult(ok(JSONFormater.BIMExchangeFormater.parseObject(exchange)));
    }

    @Transactional(readOnly = true)
    public Result getProcesses(Long idModel)
    {
        models.LCA.BIMModel returnVal = JPA.em().find(models.LCA.BIMModel.class, idModel);
        if (returnVal == null)
            return jsonResult(notFound());

        return jsonResult(ok("{"+JSONUtil.parseArray("lst",returnVal.getProcesses(),JSONFormater.BIMProcessFormater)+"}"));
    }

    @Transactional(readOnly = true)
    public Result getProcess(Long idModel, Long idProcess)
    {
        models.LCA.BIMProcess process=null;
        try
        {
            process = (models.LCA.BIMProcess) JPA.em().createQuery("Select bp from BIMModel bm inner join bm.processes bp where bm.id=:idModel and bp.idProcess=:idProcess").
                        setParameter("idModel", idModel).setParameter("idProcess", idProcess).getSingleResult();
        }
        catch (NoResultException e)
        {
            return jsonResult(notFound());
        }

        return jsonResult(ok(JSONFormater.BIMProcessFormater.parseObject(process)));

    }

    @Transactional(readOnly = true)
    public Result getForeGroundProcess2(Long idModel)
    {
        //Test
        return jsonResult(ok(""));
    }

    @Transactional(readOnly = true)
    public Result getForeGroundProcess(Long idModel)
    {
        String query_notLink = "select tmp.process_id as idObjType, tmp.lstMat, group_concat(distinct eObjIn.f_owner separator \";\") as idObj, " +
                "group_concat(distinct eUniIn.f_owner separator \";\") as idUni " +
                "from (SELECT bp.process_id, " +
                "count(distinct e.id) as QtyObjTypeLink, " +
                "count(distinct if(not isNull(f.id),e.id,null)) as QtyObjTypeLinkMat, " +
                "count(distinct if(isNull(f.id) and e.resulting_amount_value<>0,e.id,null)) as QtyObjTypeLinkBG, " +
                "count(distinct if(isNull(f2.id) and not isNull(e3.id) and e3.resulting_amount_value<>0,e3.id,null)) as QtyMatLinkBG, " +
                "ifnull(group_concat(distinct if(not(isNull(f2.id) and not isNull(e3.id) and e3.resulting_amount_value<>0),e2.f_owner,null) separator \";\"),\"\") as lstMat " +
                "FROM tbl_BIMProcesses bp left join tbl_exchanges e on(e.f_owner=bp.process_id and e.is_input=1) " +
                "left join tbl_BIMFlows f on (f.flow_id=e.f_flow and f.bimEntityType=\"BIMMaterial\") " +
                "left join tbl_exchanges e2 on(e2.f_flow=flow_id and e2.is_input=0) " +
                "left join tbl_exchanges e3 on(e2.f_owner=e3.f_owner and e3.is_input=1) " +
                "left join tbl_BIMFlows f2 on(f2.flow_id=e3.f_flow) " +
                "where bp.bimEntityType=\"BIMObjectType\" and bp.model_id=:BIMModelId " +
                "group by process_id " +
                "having QtyObjTypeLinkBG=0 and (QtyObjTypeLink=0 or QtyObjTypeLinkMat>QtyMatLinkBG)) as tmp " +
                "left join tbl_processes pType on(pType.id=tmp.process_id) " +
                "left join tbl_exchanges eType on(eType.id=pType.f_quantitative_reference ) " +
                "left join tbl_exchanges eObjIn on(eObjIn.f_flow=eType.f_flow and eObjIn.is_input=1) " +
                "left join tbl_processes pObj on(pObj.id=eObjIn.f_owner) " +
                "left join tbl_exchanges eObjOut on(pObj.f_quantitative_reference=eObjOut.id) " +
                "left join tbl_exchanges eUniIn on(eUniIn.f_flow=eObjOut.f_flow and eUniIn.is_input=1) " +
                "group by idObjType";

        Query query = JPA.em().createNativeQuery(query_notLink);
        query.setParameter("BIMModelId", idModel);

        Set<String> setId = new HashSet<>();
        Set<String> setIdUni = new HashSet<>();
        List<Object[]> lst = query.getResultList();

        for(Object[] row:lst)
        {
            String idObjType = row[0]==null?"":row[0].toString();
            String idsMat = row[1]==null?"":row[1].toString();
            String idsObj = row[2]==null?"":row[2].toString();
            String idsUni = row[3]==null?"":row[3].toString();

            String[] lstMat = idsMat.split(";");
            String[] lstObj = idsObj.split(";");
            String[] lstUni = idsUni.split(";");

            setId.add(idObjType);
            addAll(setId,lstMat);
            addAll(setId,lstObj);
            addAll(setIdUni,lstUni);
        }

        Set<String> setIdUniFinal = new HashSet<>();
        for(String uniformat:setIdUni)
            findUniformatParent(uniformat,setIdUniFinal,JPA.em());

        setId.addAll(setIdUniFinal);

        String separator="";
        StringBuilder sb = new StringBuilder();
        sb.append("{\"lst\":[");
        for(String id: setId)
        {
            if(id!=null && id.length()>0)
            {
                sb.append(separator);
                sb.append(id);
                separator = ",";
            }
        }
        sb.append("]}");


        return jsonResult(ok(sb.toString()));
    }

    private void findUniformatParent(String id, Set<String> lstUniformat, EntityManager em)
    {
        if(lstUniformat.contains(id))
            return;

        if(id==null || id.length()==0)
            return;

        String query_uniformat="SELECT eIn.f_owner " +
                "FROM tbl_processes p left join tbl_exchanges e on(p.f_quantitative_reference=e.id) " +
                "left join tbl_exchanges eIn on(eIn.f_flow=e.f_flow and eIn.is_input=1) " +
                "where p.id=:idProc";

        Query query = em.createNativeQuery(query_uniformat);
        query.setParameter("idProc",id);

        List<BigInteger> lstId=query.getResultList();

        for(BigInteger row:lstId)
        {
            if(row!=null)
                findUniformatParent(row.toString(),lstUniformat,em);
        }

        lstUniformat.add(id);
    }

    private void addAll(Set<String> setId, String[] arrId)
    {
        for(String item:arrId) {
            setId.add(item);
        }
    }

    @Transactional(readOnly = true)
    public Result getCategories(Long idModel)
    {
        models.LCA.BIMModel returnVal = JPA.em().find(models.LCA.BIMModel.class, idModel);
        if (returnVal == null)
            return jsonResult(notFound());
        return jsonResult(ok("{"+JSONUtil.parseArray("lst",returnVal.getCategories(),JSONFormater.BIMCategoryFormater)+"}"));
    }

    @Transactional(readOnly = true)
    public Result getCategory(Long idModel, Long idCategory)
    {
        BIMCategory category=null;
        try
        {
            category = (BIMCategory) JPA.em().createQuery("Select bc from BIMModel bm inner join bm.categories bc where bm.id=:idModel and bc.idCategory=:idCategory").
                        setParameter("idModel", idModel).setParameter("idCategory", idCategory).getSingleResult();
        }
        catch (NoResultException e)
        {
            return jsonResult(notFound());
        }

        return jsonResult(ok(JSONFormater.BIMCategoryFormater.parseObject(category)));
    }

    @Transactional
    public Result calculateResult(Long idModel, Long idImpactMethod, String description) {

        return spark.calculateResult(idModel, idImpactMethod, description);
    }
}

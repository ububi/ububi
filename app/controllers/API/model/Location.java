/**---------------------------   
 * PROJECT: UBUBI
 * Auth:
 *   Mathieu Dupuis
 * Date: ETE 2017
 *
 * Copyright [2017] [Mathieu Dupuis]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. 
 *
 *--------------------------*/
package controllers.API.model;

import controllers.API.model.generic.DataController;
import play.db.jpa.JPA;
import play.db.jpa.JPAApi;
import play.db.jpa.Transactional;
import play.mvc.Result;
import util.JSONFormater;

import javax.inject.Inject;
import java.io.ByteArrayInputStream;
import java.io.InputStream;

/**
 * Created by mathieu on 11/6/2016.
 */
public class Location extends DataController<org.openlca.core.model.Location>
{
    @Inject
    public Location(JPAApi api)
    {
        super(api);
        init(org.openlca.core.model.Location.class, JSONFormater.LocationFormater, null);
    }

    @Transactional(readOnly = true)
    public Result getKmz(Long id)
    {
        org.openlca.core.model.Location returnVal = JPA.em().find(org.openlca.core.model.Location.class,id);
        if (returnVal == null || returnVal.getKmz()==null)
            return notFound();

        InputStream is = new ByteArrayInputStream(returnVal.getKmz());
        return ok(is).as("application/OCTET-STREAM").withHeader("Content-Disposition","attachment; filename=location.kmz");
    }
}

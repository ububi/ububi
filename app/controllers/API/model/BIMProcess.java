/**---------------------------
 * PROJECT: UBUBI
 * Auth:
 *   Mathieu Dupuis
 * Date: ETE 2017
 *
 * Copyright [2017] [Mathieu Dupuis]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *--------------------------*/
package controllers.API.model;

import controllers.UBUBIController;
import models.LCA.*;
import util.JSONFormater;
import util.JSONUtil;

import play.db.jpa.JPA;
import play.db.jpa.JPAApi;
import play.db.jpa.Transactional;
import play.mvc.Result;
import javax.inject.Inject;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import java.util.List;

public class BIMProcess extends UBUBIController
{
    @Inject
    public BIMProcess(JPAApi api)
    {
        super(api);
    }

    @Transactional(readOnly = true)
    public Result getInfo(Long id)
    {
        models.LCA.BIMProcess returnVal = null;

        try {
            returnVal = JPA.em().find(models.LCA.BIMProcess.class, id);
        }
        catch(NoResultException e) {}

        if (returnVal == null)
            return jsonResult(notFound(EMPTY_REST_RESULT));

        return jsonResult(ok(JSONFormater.BIMProcessFormater.parseObject(returnVal)));
    }

    @Transactional(readOnly = true)
    public Result all()
    {
        List<models.LCA.BIMProcess> lstVal = null;
        try
        {
            Query query = JPA.em().createQuery("Select p from BIMProcess p");
            lstVal = query.getResultList();
        }
        catch (NoResultException e) {}

        StringBuilder sb = new StringBuilder();
        sb.append("{");
        sb.append(JSONUtil.parseArray("lst",lstVal,JSONFormater.BIMProcessFormater));
        sb.append("}");

        return jsonResult(ok(sb.toString()));
    }

    @Transactional(readOnly = true)
    public Result getModel(Long idProcess)
    {
        models.LCA.BIMProcess returnVal = null;

        try {
            returnVal=JPA.em().find(models.LCA.BIMProcess.class, idProcess);
        }
        catch (NoResultException e) {}

        if (returnVal == null)
            return jsonResult(notFound(EMPTY_REST_RESULT));

        return jsonResult(ok(JSONFormater.BIMModelFormater.parseObject(returnVal.getModel())));
    }

    @Transactional(readOnly = true)
    public Result getProcess(Long idProcess)
    {
        models.LCA.BIMProcess returnVal = null;

        try
        {
            returnVal=JPA.em().find(models.LCA.BIMProcess.class, idProcess);
        }
        catch (NoResultException e) {}

        if (returnVal == null)
            return jsonResult(notFound(EMPTY_REST_RESULT));

        return jsonResult(ok(JSONFormater.ProcessFormater.parseObject(returnVal.getProcess())));
    }

    @Transactional(readOnly = true)
    public Result getInfoByProcess(Long idProcess)
    {
        Query query = JPA.em().createQuery("Select p from BIMProcess p where idProcess=:idProcess");
        query.setParameter("idProcess", idProcess);

        models.LCA.BIMProcess returnVal = null;

        try {
            returnVal=(models.LCA.BIMProcess) query.getSingleResult();
        }
        catch (NoResultException e)
        {}

        if (returnVal == null)
            return jsonResult(notFound());

        return jsonResult(ok(JSONFormater.BIMProcessFormater.parseObject(returnVal)));
    }
}
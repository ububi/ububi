/**---------------------------   
 * PROJECT: UBUBI
 * Auth:
 *   Mathieu Dupuis
 * Date: ETE 2017
 *
 * Copyright [2017] [Mathieu Dupuis]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. 
 *
 *--------------------------*/
package controllers.API.model;

import controllers.UBUBIController;
import play.db.jpa.JPAApi;
import play.mvc.Result;
import play.routing.JavaScriptReverseRouter;

import javax.inject.Inject;


/**
 * Created by mathieu on 11/05/2016.
 */
public class Router extends UBUBIController
{
    @Inject
    public Router(JPAApi api)
    {
        super(api);
    }

    public Result jsRouter()
    {
        return ok(JavaScriptReverseRouter.create("jsModel",

                routes.javascript.Results.getData(),
                routes.javascript.Results.getDataByNodeId(),
                routes.javascript.ResultSets.all(),
                routes.javascript.ResultSets.getResultSetFromModelId(),

                routes.javascript.BIMModel.all(),
                routes.javascript.BIMModel.getInfo(),
                routes.javascript.BIMModel.getCategories(),
                routes.javascript.BIMModel.getExchanges(),
                routes.javascript.BIMModel.getProcesses(),
                routes.javascript.BIMModel.getFlows(),
                routes.javascript.BIMModel.getCategory(),
                routes.javascript.BIMModel.getExchange(),
                routes.javascript.BIMModel.getProcess(),
                routes.javascript.BIMModel.getFlow(),
                routes.javascript.BIMModel.calculateResult(),
                routes.javascript.BIMModel.getForeGroundProcess(),
                routes.javascript.BIMProcess.all(),

                routes.javascript.Process.getInfo(),
                routes.javascript.Process.getInfoWithHistory(),
                routes.javascript.Process.getExchanges(),
                routes.javascript.Process.getParameters(),
                routes.javascript.Process.getGlobalParameters(),
                routes.javascript.Process.setParameterFormula(),
                routes.javascript.Process.getKmz(),
                routes.javascript.Process.getFlowPropertyFactor(),

                routes.javascript.Flow.getInfo(),
                routes.javascript.Flow.getInfoWithHistory(),
                routes.javascript.Flow.search(),
                routes.javascript.Flow.getFlowConsumers(),
                routes.javascript.Flow.getFlowProducers(),
                routes.javascript.Flow.getFullCategory(),
                routes.javascript.Flow.getUnit(),
                routes.javascript.Flow.getGroupUnits(),

                routes.javascript.Location.getInfo(),
                routes.javascript.Location.getKmz(),

                routes.javascript.Exchange.getInfo(),
                routes.javascript.Exchange.getProvider(),
                routes.javascript.Exchange.update(),
                routes.javascript.Exchange.insert(),
                routes.javascript.Exchange.delete(),

                routes.javascript.Unit.getInfo(),
                routes.javascript.Unit.all(),

                routes.javascript.ProcessDocumentation.getInfo(),

                routes.javascript.Source.getInfo(),

                routes.javascript.Actor.getInfo(),

                routes.javascript.Category.getInfo(),
                routes.javascript.MiddleGround.create()

        )).as("text/javascript");
    }
}

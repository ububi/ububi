/**---------------------------   
 * PROJECT: UBUBI
 * Auth:
 *   Mathieu Dupuis
 * Date: ETE 2017
 *
 * Copyright [2017] [Mathieu Dupuis]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. 
 *
 *--------------------------*/
package controllers.API.model.generic;

import com.fasterxml.jackson.databind.JsonNode;
import controllers.UBUBIController;
import models.LCA.LCIResultSet;
import play.db.Database;
import util.JSONFormater;
import util.JSONUtil;
import play.db.jpa.JPA;
import play.db.jpa.JPAApi;
import play.db.jpa.Transactional;
import play.mvc.Result;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import java.sql.*;
import java.util.List;

/**
 * Created by mathieu on 10/12/2016.
 */
public abstract class DataController<T extends org.openlca.core.model.AbstractEntity> extends UBUBIController
{
    private JSONFormater<T> formater;
    private Class<T> dataClass;
    private String allQuery;

    @Inject
    public DataController(JPAApi api)
    {
        super(api);
    }

    protected void init(Class<T> dataClass, JSONFormater<T> formater, String allQuery)
    {
        this.formater=formater;
        if(formater==null)
            throw new NullPointerException();

        this.dataClass=dataClass;
        this.allQuery=allQuery;
    }

    @Transactional(readOnly = true)
    public Result getInfo(Long id)
    {
        T returnVal = JPA.em().find(dataClass, id);
        if (returnVal == null)
            return jsonResult(notFound());

        return jsonResult(ok(formater.parseObject(returnVal)));
    }

    @Transactional(readOnly = true)
    public Result all()
    {
        if(allQuery==null)
            return jsonResult(unauthorized());

        List<T> lstVal = null;
        try
        {
            Query query = JPA.em().createQuery(allQuery);
            lstVal = query.getResultList();
        }
        catch (NoResultException e) {}

        StringBuilder sb = new StringBuilder();
        sb.append("{");
        sb.append(JSONUtil.parseArray("lst",lstVal,formater));
        sb.append("}");

        return jsonResult(ok(sb.toString()));
    }

    /*
    * Override this methode to allow to create object
    *
    * return the object to insert, or null if unautorized
    * */
    protected T allowInsert(JsonNode json, EntityManager em)
    {
        return null;
    }

    @Transactional
    public Result insert()
    {
        JsonNode json = request().body().asJson();

        T val = allowInsert(json,JPA.em());
        if(val==null)
            return jsonResult(unauthorized());

        JPA.em().persist(val);
        return jsonResult(ok(formater.parseObject(val)));
    }

    /*
    * Override this methode to allow to update object
    *
    * return the modified object, or null if unautorized
    * */
    protected T allowUpdate(JsonNode modifiedObject, T sourceObj,EntityManager em)
    {
        return null;
    }

    @Transactional
    public Result update(long id)
    {
        JsonNode modifiedObject = request().body().asJson();

        T sourceObj = JPA.em().find(dataClass, id);
        if (sourceObj == null)
            return jsonResult(notFound());

        T val = allowUpdate(modifiedObject,sourceObj,JPA.em());
        if(val==null)
            return jsonResult(unauthorized());

        JPA.em().merge(val);
        return jsonResult(ok(formater.parseObject(val)));
    }

    /*
    * Override this methode to allow to delete object
    * */
    protected boolean allowDelete(T obj, EntityManager em)
    {
        return false;
    }

    @Transactional
    public Result delete(long id)
    {
        T returnVal = JPA.em().find(dataClass, id);
        if (returnVal == null)
            return jsonResult(notFound());

        if(!allowDelete(returnVal,JPA.em()))
        {
            return jsonResult(unauthorized(EMPTY_REST_RESULT));
        }

        JPA.em().remove(returnVal);
        return jsonResult(ok(EMPTY_REST_RESULT));
    }

}

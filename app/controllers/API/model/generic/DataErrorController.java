package controllers.API.model.generic;

import com.fasterxml.jackson.databind.JsonNode;
import controllers.UBUBIController;
import play.db.jpa.JPA;
import play.db.jpa.JPAApi;
import play.db.jpa.Transactional;
import play.mvc.Result;
import util.ErrorWrapper;
import util.JSONErrorFormater;
import util.JSONFormater;
import util.JSONUtil;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import java.util.List;

public abstract class DataErrorController<T extends org.openlca.core.model.AbstractEntity> extends UBUBIController
{
    private JSONErrorFormater<T> formater;
    private Class<T> dataClass;
    private String allQuery;

    @Inject
    public DataErrorController(JPAApi api)
    {
        super(api);
    }

    protected void init(Class<T> dataClass, JSONFormater<T> formater, String allQuery)
    {

        if(formater==null)
            throw new NullPointerException();
        this.formater=new JSONErrorFormater<>(formater);
        this.dataClass=dataClass;
        this.allQuery=allQuery;
    }

    @Transactional(readOnly = true)
    public Result getInfo(Long id)
    {
        T returnVal = JPA.em().find(dataClass, id);
        if (returnVal == null)
            return jsonResult(notFound());

        return jsonResult(ok(formater.parseDataOnly(returnVal)));
    }

    @Transactional(readOnly = true)
    public Result all()
    {
        if(allQuery==null)
            return jsonResult(unauthorized());

        List<T> lstVal = null;
        try
        {
            Query query = JPA.em().createQuery(allQuery);
            lstVal = query.getResultList();
        }
        catch (NoResultException e) {}

        StringBuilder sb = new StringBuilder();
        sb.append("{");
        sb.append(JSONUtil.parseArray("lst",lstVal,formater.getDataFormater()));
        sb.append("}");

        return jsonResult(ok(sb.toString()));
    }

    /*
    * Override this methode to allow to create object
    *
    * return the object to insert, or null if unautorized
    * */
    protected ErrorWrapper<T> allowInsert(JsonNode json, EntityManager em)
    {
        return null;
    }

    @Transactional
    public Result insert()
    {
        JsonNode json = request().body().asJson();

        ErrorWrapper<T> val = allowInsert(json,JPA.em());
        if(val==null || val.getData()==null)
            return jsonResult(unauthorized());

        JPA.em().persist(val.getData());
        return jsonResult(ok(formater.parseObject(val)));
    }

    /*
    * Override this methode to allow to update object
    *
    * return the modified object, or null if unautorized
    * */
    protected ErrorWrapper<T> allowUpdate(JsonNode modifiedObject, ErrorWrapper<T> sourceObj,EntityManager em)
    {
        return null;
    }

    @Transactional
    public Result update(long id)
    {
        JsonNode modifiedObject = request().body().asJson();

        ErrorWrapper<T> sourceObj = new ErrorWrapper<>(JPA.em().find(dataClass, id));
        if (sourceObj == null)
            return jsonResult(notFound());

        ErrorWrapper<T> val = allowUpdate(modifiedObject,sourceObj,JPA.em());
        if(val==null)
            return jsonResult(unauthorized());

        JPA.em().merge(val.getData());
        return jsonResult(ok(formater.parseObject(val)));
    }

    /*
    * Override this methode to allow to delete object
    * */
    protected boolean allowDelete(ErrorWrapper<T> obj, EntityManager em)
    {
        return false;
    }

    @Transactional
    public Result delete(long id)
    {
        ErrorWrapper<T> returnVal = new ErrorWrapper<>(JPA.em().find(dataClass, id));
        if (returnVal == null)
            return jsonResult(notFound());



        if(!allowDelete(returnVal,JPA.em()))
        {
            return jsonResult(unauthorized(EMPTY_REST_RESULT));
        }

        JPA.em().remove(returnVal.getData());
        return jsonResult(ok(EMPTY_REST_RESULT));
    }

}

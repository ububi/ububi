/**---------------------------   
 * PROJECT: UBUBI
 * Auth:
 *   Mathieu Dupuis
 * Date: ETE 2017
 *
 * Copyright [2017] [Mathieu Dupuis]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. 
 *
 *--------------------------*/
package controllers.API.model;

import controllers.API.model.generic.DataController;
import play.db.jpa.JPA;
import play.db.jpa.Transactional;
import play.mvc.Result;
import util.JSONFormater;
import play.db.jpa.JPAApi;
import util.JSONUtil;

import javax.inject.Inject;

/**
 * Created by mathieu on 10/12/2016.
 */
public class Category extends DataController<org.openlca.core.model.Category>
{
    @Inject
    public Category(JPAApi api)
    {
        super(api);
        init(org.openlca.core.model.Category.class,JSONFormater.CategoryFormater, null);
    }

    @Transactional
    public Result getFullCategory(Long id)
    {
        org.openlca.core.model.Category category = JPA.em().find(org.openlca.core.model.Category.class, id);
        if (category == null)
            return jsonResult(notFound(EMPTY_REST_RESULT));

        String fullCateg=getFullCategory(category, false, false);

        return jsonResult(ok("{"+ JSONUtil.parse("FullCategory",fullCateg) +"}"));
    }

    @Transactional
    public String getFullCategory(Long id, Boolean omitRootCategory,  Boolean omitFirstSlash){
        org.openlca.core.model.Category category = JPA.em().find(org.openlca.core.model.Category.class, id);
        if (category == null)
            return null;
        return getFullCategory(category, omitRootCategory, omitFirstSlash);
    }

    @Transactional
    public String getFullCategory(org.openlca.core.model.Category category, Boolean omitRootCategory, Boolean omitFirstSlash){
        return getFullCategoryAsString(category, "", omitRootCategory, omitFirstSlash);
    }

    /**
     * Gets the full category string representation of a category. For example, for the category "lake", the
     * default string representation is "\Elementary Flows\water\lake".
     * @param category The child category.
     * @param currentStringRepresentation is used in the recursion. When using this method, should be empty.
     * @param omitRootCategory If true, the first category will be removed. The "lake" category will become "\water\lake"
     * @param omitFirstSlash If true, the first slash will be removed. The "lake" category will become "Elementary Flows\water\lake"
     * @return
     */
    private String getFullCategoryAsString(org.openlca.core.model.Category category, String currentStringRepresentation, Boolean omitRootCategory, Boolean omitFirstSlash) {
        if (category == null || (category.getCategory() == null && omitRootCategory)){
            return (omitFirstSlash ? "" : "\\") + currentStringRepresentation;
        }
        else{
            currentStringRepresentation = category.getName() + (currentStringRepresentation.equals("") ? "" : "\\") + currentStringRepresentation;
            return getFullCategoryAsString(category.getCategory(), currentStringRepresentation, omitRootCategory, omitFirstSlash);
        }
    }

}

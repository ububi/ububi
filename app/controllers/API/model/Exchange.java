/**---------------------------   
 * PROJECT: UBUBI
 * Auth:
 *   Mathieu Dupuis
 * Date: ETE 2017
 *
 * Copyright [2017] [Mathieu Dupuis]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. 
 *
 *--------------------------*/
package controllers.API.model;

import com.fasterxml.jackson.databind.JsonNode;
import controllers.API.model.generic.DataController;
import controllers.API.model.generic.DataErrorController;
import models.LCA.BIMEntityType;
import org.openlca.core.model.*;
import org.openlca.core.model.Category;
import org.openlca.core.model.Process;
import org.openlca.core.model.Unit;
import play.db.jpa.JPA;
import play.db.jpa.Transactional;
import play.mvc.Result;
import util.ErrorWrapper;
import util.JSONFormater;
import play.db.jpa.JPAApi;
import util.JSONUtil;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;

/**
 * Created by mathieu on 10/12/2016.
 */
public class Exchange extends DataErrorController<org.openlca.core.model.Exchange>
{
    @Inject
    public Exchange(JPAApi api)
    {
        super(api);
        init(org.openlca.core.model.Exchange.class,JSONFormater.ExchangeFormater, null);
    }

    @Override
    protected ErrorWrapper<org.openlca.core.model.Exchange> allowUpdate(JsonNode modifiedObject, ErrorWrapper<org.openlca.core.model.Exchange> obj, EntityManager em)
    {
        org.openlca.core.model.Exchange sourceObj = obj.getData();
        //Modify the default Provider
        if(modifiedObject.get("defaultProviderId")!=null)
        {
            Long newDefaultProviderId = modifiedObject.get("defaultProviderId").asLong();
            if(newDefaultProviderId!=sourceObj.getDefaultProviderId())
            {
                if (newDefaultProviderId == null)
                    newDefaultProviderId = 0L;

                if (newDefaultProviderId != 0L)
                {
                    org.openlca.core.model.Process proc = JPA.em().find(Process.class, newDefaultProviderId);
                    if (proc.getQuantitativeReference().getFlow().getId() != sourceObj.getFlow().getId())
                        return null;
                }

                sourceObj.setDefaultProviderId(newDefaultProviderId);
            }
        }

        //TODO update formula
        //Modify the amount formula
        /*if(modifiedObject.get("amountFormula")!=null)
        {
            sourceObj.setAmountFormula(modifiedObject.get("amountValue").asText());
        }*/

        //Modify the amount
        if(modifiedObject.get("amountValue")!=null)
        {
            sourceObj.setAmountValue(modifiedObject.get("amountValue").doubleValue());
        }

        //Modify the unit
        if(modifiedObject.get("unitId")!=null)
        {
            Unit unit = em.find(Unit.class,modifiedObject.get("unitId").asLong());
            if(sourceObj.getUnit().getId()!=unit.getId())
            {
                sourceObj.setUnit(unit);

                String sqlQuery = "Select tbl_flow_property_factors.* from " +
                        "(((tbl_units inner join tbl_unit_groups on (tbl_units.f_unit_group = tbl_unit_groups.id)) " +
                        "inner join tbl_flow_properties on (tbl_flow_properties.f_unit_group=tbl_unit_groups.id)) " +
                        "inner join tbl_flow_property_factors on (tbl_flow_properties.id=f_flow_property)) " +
                        "where tbl_flow_property_factors.f_flow=:flowId and tbl_units.id=:unitId";

                FlowPropertyFactor fpf = (FlowPropertyFactor) em.createNativeQuery(sqlQuery, FlowPropertyFactor.class)
                        .setParameter("flowId", sourceObj.getFlow().getId()).setParameter("unitId", unit.getId())
                        .getSingleResult();
                sourceObj.setFlowPropertyFactor(fpf);
            }
        }

        return obj;
    }

    @Override
    protected ErrorWrapper<org.openlca.core.model.Exchange> allowInsert(JsonNode json, EntityManager em)
    {
        ErrorWrapper<org.openlca.core.model.Exchange> errEx = new ErrorWrapper<>();

        //TODO Add busness case rule
        long idProcess = json.get("processId").asLong();
        org.openlca.core.model.Process proc = em.find(org.openlca.core.model.Process.class,idProcess);

        long idFlow = json.get("flowId").asLong();
        org.openlca.core.model.Flow flow = em.find(org.openlca.core.model.Flow.class,idFlow);

        if(!json.get("isInput").asBoolean() && flow.getFlowType() == FlowType.PRODUCT_FLOW)
            return null;

        org.openlca.core.model.Exchange ex = new org.openlca.core.model.Exchange();
        errEx.setData(ex);

        ex.setInput(json.get("isInput").asBoolean());

        ex.setDefaultProviderId(json.get("defaultProviderId")!=null?json.get("defaultProviderId").asLong():0);
        ex.setAmountFormula(json.get("amountFormula")!=null?json.get("amountFormula").asText():null);
        ex.setAmountValue(json.get("amountValue").doubleValue());
        ex.setFlow(flow);
        ex.setAvoidedProduct(json.get("isAvoidedProduct")!=null?json.get("isAvoidedProduct").asBoolean():false);

        Unit unit = em.find(Unit.class,json.get("unitId").asLong());
        ex.setUnit(unit);
        //TODO to change in IED2LCA
        String sqlQuery = "Select tbl_flow_property_factors.* from " +
                "(((tbl_units inner join tbl_unit_groups on (tbl_units.f_unit_group = tbl_unit_groups.id)) " +
                "inner join tbl_flow_properties on (tbl_flow_properties.f_unit_group=tbl_unit_groups.id)) " +
                "inner join tbl_flow_property_factors on (tbl_flow_properties.id=f_flow_property)) " +
                "where tbl_flow_property_factors.f_flow=:flowId and tbl_units.id=:unitId";

        FlowPropertyFactor fpf = (FlowPropertyFactor) em.createNativeQuery(sqlQuery,FlowPropertyFactor.class)
                .setParameter("flowId",flow.getId()).setParameter("unitId",unit.getId())
                .getSingleResult();
        ex.setFlowPropertyFactor(fpf);
        proc.getExchanges().add(ex);

        return errEx;
    }

    @Override
    protected boolean allowDelete(ErrorWrapper<org.openlca.core.model.Exchange> objWrapper, EntityManager em)
    {
        //TODO add business rule
        /*Process sourceProc=null;
        BIMEntityType bet = BIMEntityType.Unknown;
        try
        {
            sourceProc = (Process)em.createNativeQuery("",Process.class).
                            setParameter("idFlow",obj.getId()).getSingleResult();
        }
        catch (NoResultException e)
        {
            return false;
        }

        //Dont allow to delete the output flow produce by the process
        if(sourceProc.getQuantitativeReference().getId()==obj.getId())
            return false;

        switch (bet)
        {
            case BIMRoot:
            case BIMObject:
            case BIMUniformat:
                return false;
            case Unknown:
            case BIMMaterial:
                return true;
            case BIMObjectType:
                //Verifify that is not
                break;
        }*/
        org.openlca.core.model.Exchange obj=objWrapper.getData();
        Boolean canDelete = !(!obj.isInput() && obj.getFlow().getFlowType()==FlowType.PRODUCT_FLOW);

        if(canDelete)
        {
            em.createNativeQuery("delete from tbl_BIMExchanges where exchange_id=:IdExchange").setParameter("IdExchange",obj.getId()).executeUpdate();
        }

        return canDelete;
    }

    @Transactional
    public Result getProvider(long id)
    {
        org.openlca.core.model.Exchange exchange = JPA.em().find(org.openlca.core.model.Exchange.class, id);
        if (exchange == null)
            return jsonResult(notFound(EMPTY_REST_RESULT));

        //TODO Add some inteligence to select the right provider

        org.openlca.core.model.Process defaultProd=null;
        if(exchange.getDefaultProviderId()==0)
        {
            try
            {
                defaultProd = (Process)JPA.em().createNativeQuery("SELECT p.* " +
                        "FROM tbl_processes as p inner join tbl_exchanges as e on (e.f_owner=p.id) " +
                        "where f_flow=:idFlow and e.is_input=:input limit 1",Process.class).
                        setParameter("idFlow",exchange.getFlow().getId()).setParameter("input",0).
                        getSingleResult();
            }
            catch (NoResultException e)
            {
                return jsonResult(noContent());
            }
        }
        else
            defaultProd = JPA.em().find(Process.class,exchange.getDefaultProviderId());

        return jsonResult(ok(JSONFormater.ProcessFormater.parseObject(defaultProd)));
    }

}
/**---------------------------   
 * PROJECT: UBUBI
 * Auth:
 *   Mathieu Dupuis
 * Date: ETE 2017
 *
 * Copyright [2017] [Mathieu Dupuis]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. 
 *
 *--------------------------*/
package controllers.API.model;

import controllers.API.model.generic.DataController;
import play.db.jpa.JPAApi;
import util.JSONFormater;

import javax.inject.Inject;

/**
 * Created by mathieu on 11/6/2016.
 */
public class ProcessDocumentation extends DataController<org.openlca.core.model.ProcessDocumentation>
{
    @Inject
    public ProcessDocumentation(JPAApi api)
    {
        super(api);
        init(org.openlca.core.model.ProcessDocumentation.class, JSONFormater.ProcessDocumentationFormater, null);
    }
}

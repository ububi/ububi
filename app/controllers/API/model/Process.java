/**---------------------------   
 * PROJECT: UBUBI
 * Auth:
 *   Mathieu Dupuis
 * Date: ETE 2017
 *
 * Copyright [2017] [Mathieu Dupuis]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. 
 *
 *--------------------------*/
package controllers.API.model;

import controllers.API.model.generic.DataController;
import models.LCA.LCIResultSet;
import models.LCA.ProcessHistory;
import org.openlca.core.model.FlowPropertyFactor;
import org.openlca.core.model.Parameter;
import org.openlca.core.model.ParameterScope;
import org.openlca.expressions.FormulaInterpreter;
import org.openlca.expressions.InterpreterException;
import play.db.Database;
import util.JSONFormater;
import util.JSONUtil;
import play.db.jpa.JPA;
import play.db.jpa.JPAApi;
import play.db.jpa.Transactional;
import play.mvc.Result;

import javax.inject.Inject;
import javax.persistence.Query;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.sql.*;
import java.util.List;

/**
 * Created by mathieu on 10/12/2016.
 */
public class Process extends DataController<org.openlca.core.model.Process>
{

    @Inject
    private Database database;

    @Inject
    public Process(JPAApi api)
    {
        super(api);
        init(org.openlca.core.model.Process.class,JSONFormater.ProcessFormater, null);
    }

    @Transactional(readOnly = true)
    public Result getInfoWithHistory(Long id, Long idResultSet) throws SQLException
    {
        org.openlca.core.model.Process returnVal = JPA.em().find(org.openlca.core.model.Process.class,id);
        if (returnVal == null)
            return jsonResult(notFound());

        PreparedStatement preparedStatement = this.database.getConnection().prepareStatement("SELECT * FROM `tbl_processes_history` WHERE id=?", Statement.RETURN_GENERATED_KEYS);
        preparedStatement.setLong(1, id);

        ResultSet rs = preparedStatement.executeQuery();
        if (!rs.next() ) {
            return jsonResult(notFound());
        }

        LCIResultSet resultSet = JPA.em().find(LCIResultSet.class, idResultSet);
        Timestamp timestamp = resultSet.getCalc_time();

        while (rs.next()) {
            if(rs.getTimestamp("dt_datetime").before(timestamp)){
                if(rs.getString("action").compareTo("insert") == 0){
                    return jsonResult(ok(JSONFormater.ProcessFormater.parseObject(returnVal)));
                }
                else {

                    long historyId = rs.getLong("id");

                    ProcessHistory data = JPA.em().find(ProcessHistory.class, historyId);

                    return jsonResult(ok(JSONFormater.ProcessHistoryFormater.parseObject(data)));
                }
            }
        }

        return jsonResult(notFound());
    }

    @Transactional(readOnly = true)
    public Result getExchanges(Long id)
    {
        org.openlca.core.model.Process returnVal = JPA.em().find(org.openlca.core.model.Process.class,id);
        if (returnVal == null)
            return jsonResult(notFound());
        return jsonResult(ok("{"+JSONUtil.parseArray("lst",returnVal.getExchanges(),JSONFormater.ExchangeFormater)+"}"));
    }

    @Transactional(readOnly = true)
    public Result getParameters(Long id)
    {
        org.openlca.core.model.Process returnVal = JPA.em().find(org.openlca.core.model.Process.class,id);
        if (returnVal == null)
            return jsonResult(notFound());
        return jsonResult(ok("{"+JSONUtil.parseArray("lst",returnVal.getParameters(),JSONFormater.ParameterFormater)+"}"));
    }

    @Transactional(readOnly = true)
    public Result getGlobalParameters() {

        List<Parameter> globalParameters = getGlobalParametersFromDB();
        if (globalParameters == null || globalParameters.size() == 0)
            return jsonResult(notFound());

        return jsonResult(ok("{"+JSONUtil.parseArray("lst",globalParameters,JSONFormater.ParameterFormater)+"}"));
    }

    @Transactional(readOnly = true)
    public Result getKmz(Long id)
    {
        org.openlca.core.model.Process returnVal = JPA.em().find(org.openlca.core.model.Process.class,id);
        if (returnVal == null)
            return notFound(EMPTY_REST_RESULT);
        if(returnVal.getLocation()==null || returnVal.getLocation().getKmz()==null)
            return noContent();

        InputStream is = new ByteArrayInputStream(returnVal.getLocation().getKmz());
        return ok(is).as("application/OCTET-STREAM").withHeader("Content-Disposition","attachment; filename=location.kmz");
    }

    @Transactional
    public Result setParameterFormula(Long idProcess, String newFormula, String parameterName, Double value) {

        org.openlca.core.model.Process retrievedProcess = JPA.em().find(org.openlca.core.model.Process.class,idProcess);

        String newFormulaWithValue = replaceNewFormulaWithRealValue(retrievedProcess, newFormula);
        double calculatedValue = evaluateValueWithFormula(newFormulaWithValue);

        for(Parameter parameter : retrievedProcess.getParameters()) {
            if(parameter.getName().equalsIgnoreCase(parameterName)) {
                setValue(parameter, newFormula, value, calculatedValue);
                setFormula(newFormula, parameter);
                JPA.em().persist(parameter);
                break;
            }
        }

        return jsonResult(ok("{"+JSONUtil.parseArray("lst",retrievedProcess.getParameters(),JSONFormater.ParameterFormater)+"}"));
    }

    @Transactional
    public Result getFlowPropertyFactor(Long id)
    {
        org.openlca.core.model.Process proc = JPA.em().find(org.openlca.core.model.Process.class,id);
        List<FlowPropertyFactor> lstfpf = proc.getQuantitativeReference().getFlow().getFlowPropertyFactors();
        return jsonResult(ok("{"+JSONUtil.parseArray("lst",lstfpf,JSONFormater.FlowPropertyFactorFormater)+"}"));
    }

    private List<Parameter> getGlobalParametersFromDB() {

        Query query = JPA.em().createQuery("Select p from Parameter p where p.scope = :global");
        query.setParameter("global", ParameterScope.GLOBAL);

        return (List<Parameter>) query.getResultList();
    }

    private void setValue(Parameter parameter, String newFormula, double value, double calculatedValue) {

        if(newFormula.trim().length() != 0)
            parameter.setValue(calculatedValue);
        else
            parameter.setValue(value);
    }

    private void setFormula(String newFormula, Parameter parameter) {

        if(newFormula.trim().length() == 0)
            parameter.setFormula("");
        else
            parameter.setFormula(newFormula);
    }

    private String replaceNewFormulaWithRealValue(org.openlca.core.model.Process retrievedProcess, String newFormula) {

        newFormula = replaceNewFormulaWithRealValue(retrievedProcess.getParameters(), newFormula);

        List<Parameter> globalParameters = getGlobalParametersFromDB();
        newFormula = replaceNewFormulaWithRealValue(globalParameters, newFormula);

        return newFormula;
    }

    private String replaceNewFormulaWithRealValue(List<Parameter> parameters, String newFormula) {

        for(Parameter parameter : parameters) {
            if(newFormula.toLowerCase().contains(parameter.getName().toLowerCase())) {
                newFormula = newFormula.replaceAll("(?i)"+parameter.getName(), String.valueOf(parameter.getValue()));
            }
        }

        return newFormula;
    }

    private double evaluateValueWithFormula(String newFormula) {

        try {
            return new FormulaInterpreter().eval(newFormula);
        } catch(InterpreterException ex) {
            System.out.println("Error in method evaluateValueWithFormula with error: " + ex.getMessage());
            ex.getStackTrace();
            return 0;
        }
    }

}

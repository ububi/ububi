package controllers.API.model;

import controllers.UBUBIController;
import models.LCA.BIMEntityType;
import models.LCA.Log;
import models.LCA.Middleground;
import org.openlca.core.model.*;
import org.openlca.core.model.Category;
import org.openlca.core.model.Flow;
import org.openlca.core.model.Process;
import play.db.jpa.JPA;
import play.db.jpa.JPAApi;
import play.db.jpa.Transactional;
import play.mvc.*;
import util.*;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.math.BigInteger;
import java.util.*;

public class MiddleGround extends UBUBIController
{
    @Inject
    public MiddleGround(JPAApi api) {
        super(api);
    }

    @Transactional()
    public Result create()
    {
        Http.MultipartFormData body = request().body().asMultipartFormData();
        Map<String, String[]> resultMap = body.asFormUrlEncoded();

        BIMEntityType BimEntity = "M".equals(resultMap.get("entityType")[0])?BIMEntityType.BIMMaterial:BIMEntityType.BIMObjectType;

        Boolean skipFirstLine=!("1".equals(resultMap.getOrDefault("includeHeader",new String[]{""})[0]));

        //Get middleground file
        Http.MultipartFormData.FilePart middlegroundFormPart = body.getFile("middlegroundfile");
        if (middlegroundFormPart == null)
            return ok("{" + JSONUtil.parseEscapedString("Status","Error")+","+ JSONUtil.parse("Desc","Missing middle ground file")+"}");
        File file = (File)middlegroundFormPart.getFile();

        /*MiddleGroundTemplate template = new MiddleGroundTemplate(new MgField[]{
                                                                 MgField.flowName,
                                                                 MgField.unit,
                                                                 MgField.ref,
                                                                 MgField.indicator},
                                                                 BimEntity.name());*/

        MiddleGroundTemplate template = new MiddleGroundTemplate(new MgField[]{
                                                                 MgField.flowName,
                                                                 MgField.unit,
                                                                 MgField.ref,
                                                                 MgField.indicator,
                                                                 MgField.kgbym3,
                                                                 MgField.kgbym2,
                                                                 MgField.refDensity,
                                                                 MgField.rValueIn,
                                                                 MgField.rValue,
                                                                 MgField.refRValue},
                                                                 BimEntity.name());

        StringBuilder lstErrors = new StringBuilder();
        String status ="OK";
        if(!create(template,skipFirstLine,file,JPA.em(),lstErrors))
            status="Error";

        return ok("{" + JSONUtil.parseEscapedString("Status",status)+","+ JSONUtil.parse("Desc",lstErrors.toString())+"}");

    }

    public boolean create(MiddleGroundTemplate template,boolean skipFirstLine,File file, EntityManager em)
    {
        return create(template, skipFirstLine, file, em, null);
    }

    public boolean create(MiddleGroundTemplate template,boolean skipFirstLine,File file, EntityManager em, StringBuilder lstErrors)
    {
        Date begin = new Date();

        StringBuilder errors = lstErrors;
        if(errors==null)
            errors = new StringBuilder();
        int offsetLevel1 = template.getFieldOffset();

        Log trace = new Log();
        trace.setAction("Import MiddleGround");
        try {
            trace.setIdBegin(((BigInteger)em.createNativeQuery("SELECT SEQ_COUNT FROM SEQUENCE where SEQ_NAME=\"entity_seq\"").getSingleResult()).longValue());
        }
        catch (Exception e){
            e.printStackTrace();
        }

        Map<Integer, org.openlca.core.model.Process> processes = new HashMap<>();
        Map<Integer, org.openlca.core.model.Flow> flows = new HashMap<>();
        EntityTransaction et = em.getTransaction();

        try {
            IED2LCA ied2lca = new IED2LCA(em);

            org.openlca.core.model.Category catProc = ied2lca.getOrCreateCategory(ModelType.PROCESS, "_Middleground", null);
            Category catFlow = ied2lca.getOrCreateCategory(ModelType.FLOW, "_Middleground", null);

            Process procLevel = null;
            org.openlca.core.model.Flow flowLevel = null;

            FileReader fileReader = new FileReader(file);
            BufferedReader bufferedReader = new BufferedReader(fileReader);

            Query query=null;
            String line = "";
            String unit = "";
            String UuidParent = "";

            if(skipFirstLine && (line = bufferedReader.readLine()) != null) //Skip first line
                while ((line = bufferedReader.readLine()) != null)
                {
                    String[] data = line.split("\t");

                    if (data.length >= offsetLevel1 && !"data gap".equals(template.getStringField(MgField.flowName,data,"").toLowerCase().trim()) && !"".equals(template.getStringField(MgField.unit,data,"").toLowerCase().trim())) {

                        int level = 1;
                        boolean eor = false; // End of row

                        do {

                            // CHECK if uuid_src already exists, create Middleground
                            String uuid_src = data[offsetLevel1 + (level - 1) * 2].trim();
                            String name = data[(offsetLevel1+1) + (level - 1) * 2].replace("\"", "").trim();
                            unit = template.getStringField(MgField.unit,data,"").toLowerCase().trim();
                            boolean newMiddleGround=true;

                            if (level == 1) {
                                processes = new HashMap<>();
                                flows = new HashMap<>();
                                UuidParent = null;
                            }

                            if ("".equals(uuid_src) || "".equals(name))
                            {
                                String errDesc=String.format("[Error] Invalid Middleground uuid:%s name:%s", uuid_src, name);
                                errors.append(errDesc+"\n");
                                System.out.println(errDesc);

                                //Skip this iteration & skip this source file line
                                eor=true;
                                continue;
                            }

                            Middleground mdGroundExist=null;
                            Query mdExistQuery = em.createQuery("Select m from Middleground m where m.uuidSrc=:uuid", Middleground.class);
                            mdExistQuery.setParameter("uuid",uuid_src);
                            mdExistQuery.setMaxResults(1);
                            mdExistQuery.setFirstResult(0);
                            List<Middleground> resultsExist = mdExistQuery.getResultList();

                            newMiddleGround=resultsExist.size() == 0;

                            if (newMiddleGround)
                            {
                                //Create Flow and Process for this level
                                procLevel = ied2lca.createTemplateProcess(name,catProc,catFlow,unit);
                                flowLevel = procLevel.getQuantitativeReference().getFlow();

                                //Create input in the parent process
                                if (level != 1)
                                {
                                    UuidParent=data[offsetLevel1 + (level - 1) * 2 - 2];
                                    ied2lca.createInputExchange(processes.get(level - 1), flowLevel, unit);
                                }



                                // Check if end of row
                                eor = offsetLevel1 + (level) * 2 > data.length - 2;

                                // Store level
                                processes.put(level, procLevel);
                                flows.put(level, flowLevel);

                                Middleground mdground=new Middleground();
                                mdground.setUuidSrc(uuid_src);
                                mdground.setName(name);
                                mdground.setLevel(level);

                                mdground.setUuidParent(UuidParent);
                                mdground.setIdProcess(procLevel.getId());
                                mdground.setUuidProcess(procLevel.getRefId());
                                mdground.setIdFlow(flowLevel.getId());
                                mdground.setUuidFlow(flowLevel.getRefId());

                                template.setField(mdground,MgField.unit,data);
                                if(eor)
                                {
                                    template.setAllField(mdground, data);

                                    manageUnit(mdground,procLevel,ied2lca,em);
                                }

                                em.persist(mdground);
                            }
                            else
                            {
                                //Middleground already exist
                                //Load the Middleground
                                Middleground mdg = resultsExist.get(0);
                                if(!name.trim().equals(mdg.getName().trim()))
                                {
                                    String errDesc=String.format("[Error] Same UUID (%s) but different name \"%s\" vs \"%s\"",
                                            uuid_src,mdg.getName(),name);
                                    errors.append(errDesc+"\n");
                                    System.out.println(errDesc);

                                    //Skip this line
                                    eor=true;
                                    continue;
                                }
                                procLevel=JPA.em().find(Process.class,mdg.getIdProcess());
                                processes.put(level, procLevel);
                                flowLevel=JPA.em().find(Flow.class,mdg.getIdFlow());
                                flows.put(level, flowLevel);
                            }

                            // Check if end of row
                            if (offsetLevel1 + (level) * 2 > data.length - 2)
                            {
                                if(template.contain(MgField.flowName)) {
                                    String flowname =template.getStringField(MgField.flowName,data);
                                    // Retrieve the Reference Product Flow
                                    Query refFlowQuery = em.createQuery("SELECT flow " +
                                            "FROM Flow flow " +
                                            "WHERE CAST(trim(flow.name) as binary) = CAST(trim(:flowname) as binary) and " +
                                            "flow.flowType=:flowtype", Flow.class);
                                    refFlowQuery.setParameter("flowname", flowname);
                                    refFlowQuery.setParameter("flowtype", FlowType.PRODUCT_FLOW);

                                    refFlowQuery.setFirstResult(0);
                                    refFlowQuery.setMaxResults(1);
                                    Flow flow = null;

                                    try {
                                        flow = (Flow) refFlowQuery.getSingleResult();

                                        //If middle ground was created, check if already linked
                                        boolean alreadyCreated = false;
                                        if (!newMiddleGround) {
                                            Query alreadyLink = em.createNativeQuery("Select id from tbl_exchanges where is_input=1 and f_owner=:process and f_flow=:flow");
                                            alreadyLink.setParameter("process", procLevel.getId());
                                            alreadyLink.setParameter("flow", flowLevel.getId());
                                            alreadyLink.setMaxResults(1);
                                            alreadyLink.setFirstResult(0);
                                            try {
                                                String exist = alreadyLink.getSingleResult().toString();
                                                alreadyCreated = true;
                                            } catch (NoResultException e) {
                                            }
                                        }

                                        if (!alreadyCreated)
                                            ied2lca.createInputExchange(processes.get(level), flow, unit);
                                    } catch (NoResultException e) {
                                        Process p = processes.get(level);
                                        String errDesc = String.format("[Error] Impossible to found flow \"%s\") for process \"%s\" (%s)",
                                                flowname, p.getName(), p.getRefId());
                                        errors.append(errDesc + "\n");
                                        System.out.println(errDesc);
                                    }
                                    eor = true;
                                }
                            }
                            level++;
                        } while (!eor);
                    }
                }

            fileReader.close();
        } catch (IOException e) {

            e.printStackTrace();
            et.rollback();
            return false;
        }

        em.flush();
        em.joinTransaction();

        try {
            trace.setIdEnd((Long)em.createQuery("SELECT max(e.id) FROM Exchange e").getSingleResult());
        }
        catch (Exception e){
            e.printStackTrace();
        }
        Date end = new Date();
        trace.setDescrition(errors+"\n[Info] Duration:"+(end.getTime()-begin.getTime())+"ms");
        em.persist(trace);
        return true;
    }

    protected void manageUnit(Middleground mg, Process proc, IED2LCA ied2lca,EntityManager em)
    {
        String refDensity = mg.getRefDensity()==null?"":"Ref: "+mg.getRefDensity();
        setParameter(proc,"kgbym2",mg.getKgbym2(),refDensity,ied2lca);
        setParameter(proc,"kgbym3",mg.getKgbym3(),refDensity,ied2lca);

        String refRvalue = mg.getRefRValue()==null?"":" Ref: "+mg.getRefRValue();
        setParameter(proc,"RValue",mg.getRValue(), "Unit: hr*ft2*F/BTU "+refRvalue,ied2lca);
        setParameter(proc,"RValueIn",mg.getRValueIn(),"Unit: hr*ft2*F/BTU in "+refRvalue,ied2lca);

        setParameter(proc,"ThicknessMin",mg.getThicknessMin(),"",ied2lca);
        setParameter(proc,"ThicknessMax",mg.getThicknessMax(),"",ied2lca);

        String unit = mg.getUnit();
        if(unit==null)unit="";

        HashMap<String,FlowProperty> lstFlowProperty = ied2lca.getFlowPropertyByUnit();
        Flow flow = proc.getQuantitativeReference().getFlow();
        HashMap<Long, FlowPropertyFactor> lstConversion = ied2lca.getConversionByFlowProperty(flow);

        FlowPropertyFactor fpfm2=lstConversion.getOrDefault(lstFlowProperty.getOrDefault("m2",null),null);
        FlowPropertyFactor fpfm3=lstConversion.getOrDefault(lstFlowProperty.getOrDefault("m3",null),null);
        FlowPropertyFactor fpfkg=lstConversion.getOrDefault(lstFlowProperty.getOrDefault("kg",null),null);

        switch (unit)
        {
            case "kg":
                if(mg.getKgbym2()!=null && mg.getKgbym2()!=0D && fpfm2==null)
                    ied2lca.createFlowPropertyFactor(flow,lstFlowProperty.getOrDefault("m2",null),mg.getKgbym2());
                if(mg.getKgbym3()!=null && mg.getKgbym3()!=0D && fpfm3==null)
                    ied2lca.createFlowPropertyFactor(flow,lstFlowProperty.getOrDefault("m3",null),mg.getKgbym3());

                break;
            case "m2":
                if(mg.getKgbym2()!=null && mg.getKgbym2()!=0D && fpfkg==null)
                    ied2lca.createFlowPropertyFactor(flow,lstFlowProperty.getOrDefault("kg",null),1D/mg.getKgbym2());

                break;
            case "m3":
                if(mg.getKgbym3()!=null && mg.getKgbym3()!=0D && fpfkg==null)
                    ied2lca.createFlowPropertyFactor(flow,lstFlowProperty.getOrDefault("kg",null),1D/mg.getKgbym3());

                break;
        }

        em.persist(flow);
        em.persist(proc);
    }

    protected void setParameter(Process proc, String name , Double value, String comment, IED2LCA ied2lca)
    {
        if(value!=null && value!=0)
            ied2lca.setParameter(proc,name,value,comment);
    }

    @Transactional(readOnly = true)
    public Result test()
    {

        return ok("");
    }

}

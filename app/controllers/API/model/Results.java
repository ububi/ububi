package controllers.API.model;

import controllers.API.Spark;
import models.LCA.*;
import controllers.UBUBIController;
import play.db.jpa.JPA;
import play.db.jpa.JPAApi;
import play.db.jpa.Transactional;
import play.mvc.Result;
import util.JSONFormater;
import util.JSONUtil;

import javax.inject.Inject;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import java.util.List;

/**
 * Created by Daniel on 5/11/2017.
 * Edited by JP on 7/20/2017
 */
public class Results extends UBUBIController {

    @Inject
    public Results(JPAApi api)
    {
        super(api);
    }

    @Transactional(readOnly = true)
    public Result getData(int resultSetId) {

        List<models.LCA.LCIGraph> lstVal = null;
        try
        {
            Query query = JPA.em().createQuery("Select m from LCIGraph m where parent_process_id=-1 and resultSet_id=" + resultSetId);
            lstVal = query.getResultList();
        }
        catch (NoResultException e) {}

        StringBuilder sb = new StringBuilder();
        sb.append("{");
        sb.append(JSONUtil.parseArray("lst", lstVal, JSONFormater.LCIGraphFormater));
        sb.append("}");

        return jsonResult(ok(sb.toString()));
    }

    @Transactional(readOnly = true)
    public Result getDataByNodeId(int idNodeParent, int resultSetId) {
        List<models.LCA.LCIGraph> lstVal = null;
        try
        {
            Query query = JPA.em().createQuery("Select m from LCIGraph m where parent_process_id=" + idNodeParent + " and resultSet_id=" + resultSetId + "and percent > 0");
            lstVal = query.getResultList();
        }
        catch (NoResultException e) {}

        StringBuilder sb = new StringBuilder();
        sb.append("{");
        sb.append(JSONUtil.parseArray("lst", lstVal, JSONFormater.LCIGraphFormater));
        sb.append("}");

        return jsonResult(ok(sb.toString()));
    }
}

/**---------------------------   
 * PROJECT: UBUBI
 * Auth:
 *   Mathieu Dupuis
 * Date: ETE 2017
 *
 * Copyright [2017] [Mathieu Dupuis]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. 
 *
 *--------------------------*/
package controllers.API.model;

import controllers.API.model.generic.DataController;
import org.openlca.core.model.UnitGroup;
import play.db.jpa.JPA;
import play.db.jpa.Transactional;
import play.mvc.Result;
import util.JSONFormater;
import play.db.jpa.JPAApi;
import util.JSONUtil;

import javax.inject.Inject;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import java.util.List;

/**
 * Created by mathieu on 10/12/2016.
 */
public class Unit extends DataController<org.openlca.core.model.Unit>
{
    @Inject
    public Unit(JPAApi api)
    {
        super(api);
        init(org.openlca.core.model.Unit.class,JSONFormater.UnitFormater, "Select u from Unit u");
    }

    @Transactional(readOnly = true)
    public Result getUnitGroup(Long id) {
        Query q = JPA.em().createQuery("Select ug From UnitGroup as ug join ug.units as u where u.id=:id ", org.openlca.core.model.UnitGroup.class).setParameter("id", id);
        UnitGroup ug;
        try {
            ug = (UnitGroup) q.getSingleResult();
        } catch(NoResultException e) {
            return jsonResult(notFound(EMPTY_REST_RESULT));
        }
        return jsonResult(ok(JSONFormater.UnitGroupFormater.parseObject(ug)));
    }
}

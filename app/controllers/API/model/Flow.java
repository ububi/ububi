/**---------------------------   
 * PROJECT: UBUBI
 * Auth:
 *   Mathieu Dupuis
 * Date: ETE 2017
 *
 * Copyright [2017] [Mathieu Dupuis]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. 
 *
 *--------------------------*/
package controllers.API.model;

import com.fasterxml.jackson.databind.JsonNode;
import controllers.API.model.generic.DataController;
import models.LCA.FlowHistory;
import models.LCA.LCIResultSet;
import models.LCA.ProcessHistory;
import org.openlca.core.model.*;
import org.openlca.core.model.Category;
import org.openlca.core.model.Process;
import play.data.DynamicForm;
import play.data.Form;
import play.db.Database;
import util.JSONFormater;
import util.JSONUtil;
import play.db.jpa.JPA;
import play.db.jpa.JPAApi;
import play.db.jpa.Transactional;
import play.mvc.Result;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import java.sql.*;
import java.util.List;

/**
 * Created by mathieu on 10/12/2016.
 */
public class Flow extends DataController<org.openlca.core.model.Flow>
{
    @Inject
    private controllers.API.model.Category categoryController;

    @Inject
    private Database database;

    @Inject
    public Flow(JPAApi api)
    {
        super(api);
        init(org.openlca.core.model.Flow.class,JSONFormater.FlowFormater, null);
    }

    @Transactional(readOnly = true)
    public Result getInfoWithHistory(Long id, Long idResultSet) throws SQLException {
        org.openlca.core.model.Flow returnVal = JPA.em().find(org.openlca.core.model.Flow.class, id);
        if (returnVal == null)
            return jsonResult(notFound());

        PreparedStatement preparedStatement = this.database.getConnection().prepareStatement("SELECT * FROM `tbl_flows_history` WHERE id=?", Statement.RETURN_GENERATED_KEYS);
        preparedStatement.setLong(1, id);

        ResultSet rs = preparedStatement.executeQuery();
        if (!rs.next()) {
            return jsonResult(notFound());
        }

        LCIResultSet resultSet = JPA.em().find(LCIResultSet.class, idResultSet);
        Timestamp timestamp = resultSet.getCalc_time();

        while (rs.next()) {
            if (rs.getTimestamp("dt_datetime").before(timestamp)) {
                if (rs.getString("action").compareTo("insert") == 0) {
                    return jsonResult(ok(JSONFormater.FlowFormater.parseObject(returnVal)));
                } else {
                    long historyId = rs.getLong("id");

                    FlowHistory data = JPA.em().find(FlowHistory.class, historyId);
                    return jsonResult(ok(JSONFormater.FlowHistoryFormater.parseObject(data)));
                }
            }
        }

        return jsonResult(notFound());
    }

    @Transactional(readOnly = true)
    public Result search()
    {
        JsonNode data = request().body().asJson();
        JsonNode jsKeyword = data.get("keyword");

        if(jsKeyword==null)
            return jsonResult(unauthorized(EMPTY_REST_RESULT));

        String keyword = jsKeyword.asText();
        if(keyword.length()<4)
            return jsonResult(unauthorized(EMPTY_REST_RESULT));

        String filtreFlowType=null;
        String sql_query="Select * from tbl_flows where name like :searchPattern";

        String jsonFlowType = data.get("flow_type")!=null?data.get("flow_type").asText():"";
        if(!"ALL".equals(jsonFlowType))
        {

            if(FlowType.ELEMENTARY_FLOW.toString().equals(jsonFlowType) &&
               FlowType.PRODUCT_FLOW.toString().equals(jsonFlowType))
            {
                jsonFlowType=FlowType.PRODUCT_FLOW.toString();
            }

            filtreFlowType=jsonFlowType;
            sql_query+=" and flow_type = :flow_type";
        }

        String searchPattern = "%"+keyword+"%";
        List<org.openlca.core.model.Flow> lst=null;
        try
        {
            Query query=JPA.em().createNativeQuery(sql_query, org.openlca.core.model.Flow.class);
            query.setParameter("searchPattern", searchPattern);
            if(filtreFlowType!=null)
                query.setParameter("flow_type",filtreFlowType);

            lst = (List<org.openlca.core.model.Flow>) query.getResultList();
        }
        catch (NoResultException e)
        {
            return jsonResult(ok(EMPTY_REST_RESULT));
        }
        return jsonResult(ok("{"+JSONUtil.parseArray("lst",lst,JSONFormater.FlowFormater)+"}"));
    }

    @Transactional(readOnly = true)
    public Result getFlowProducers(Long idFlow)
    {
        List<Process> lst = getFlows(idFlow, false);
        return jsonResult(ok("{"+JSONUtil.parseArray("lst",lst,JSONFormater.ProcessFormater)+"}"));
    }

    @Transactional(readOnly = true)
    public Result getFlowConsumers(Long idFlow)
    {
        List<Process> lst = getFlows(idFlow, true);
        return jsonResult(ok("{"+JSONUtil.parseArray("lst",lst,JSONFormater.ProcessFormater)+"}"));
    }

    private List<Process> getFlows(Long idFlow, boolean isInput)
    {
        List<Process> lst = null;
        //Try to find list of producer process
        try
        {
            lst = (List<Process>) JPA.em().createNativeQuery("SELECT distinct p.* " +
                    "FROM tbl_processes as p inner join tbl_exchanges as e on (e.f_owner=p.id) " +
                    "where f_flow=:idFlow and e.is_input=:input",Process.class).
                    setParameter("idFlow",idFlow).setParameter("input",isInput?1:0).
                    getResultList();
        }
        catch (NoResultException e)
        {} //Not found

        return lst;
    }


    @Transactional
    public Result getFullCategory(Long id)
    {
        org.openlca.core.model.Flow flow = JPA.em().find(org.openlca.core.model.Flow.class, id);
        if (flow == null)
            return jsonResult(notFound(EMPTY_REST_RESULT));

        String fullCateg = categoryController.getFullCategory(flow.getCategory(), false, false);

        return jsonResult(ok("{"+ JSONUtil.parse("FullCategory",fullCateg) +"}"));
    }


    @Transactional
    public Result getUnit(Long id)
    {
        //TODO To split to manage all unit in unit group.
        org.openlca.core.model.Flow flow = JPA.em().find(org.openlca.core.model.Flow.class, id);
        if (flow == null)
            return jsonResult(notFound(EMPTY_REST_RESULT));

        Query q = JPA.em().createQuery("Select ug.referenceUnit From Flow as f inner join f.flowPropertyFactors as fpf "+
                                       "inner join fpf.flowProperty as fp inner join fp.unitGroup as ug " +
                                       "Where f.id=:idFlow", org.openlca.core.model.Unit.class).setParameter("idFlow",id);
        List<org.openlca.core.model.Unit> units=q.getResultList();

        return jsonResult(ok("{"+JSONUtil.parseArray("lst",units,JSONFormater.UnitFormater)+"}"));
    }

    @Transactional
    public Result getGroupUnits(Long id) {
        org.openlca.core.model.Flow flow = JPA.em().find(org.openlca.core.model.Flow.class, id);
        if (flow == null)
            return jsonResult(notFound(EMPTY_REST_RESULT));

        Query q = JPA.em().createQuery("Select u From Flow as f inner join f.flowPropertyFactors as fpf " +
                "inner join fpf.flowProperty as fp inner join fp.unitGroup as ug inner join ug.units as u " +
                "Where f.id=:idFlow order by u.name", org.openlca.core.model.Unit.class).setParameter("idFlow", id);
        List<org.openlca.core.model.Unit> units = q.getResultList();

        return jsonResult(ok("{" + JSONUtil.parseArray("lst", units, JSONFormater.UnitFormater) + "}"));
    }

}

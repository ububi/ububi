package controllers.API.model;

import controllers.API.Spark;
import controllers.UBUBIController;
import models.LCA.*;
import play.db.jpa.JPA;
import play.db.jpa.JPAApi;
import play.db.jpa.Transactional;
import play.mvc.Result;
import util.JSONFormater;
import util.JSONUtil;

import javax.inject.Inject;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import java.util.List;

/**
 * Created by arasivaneswaran on 2017-07-24.
 */
public class ResultSets extends UBUBIController
{
    @Inject
    private Spark spark;

    @Inject
    public ResultSets(JPAApi api)
    {
        super(api);
    }

    @Transactional(readOnly = true)
    public Result all()
    {
        List<models.LCA.LCIResultSet> lstVal = null;
        try
        {
            Query query = JPA.em().createQuery("Select m from LCIResultSet m");
            lstVal = query.getResultList();
        }
        catch (NoResultException e) {}

        StringBuilder sb = new StringBuilder();
        sb.append("{");
        sb.append(JSONUtil.parseArray("lst", lstVal, JSONFormater.LCIResultSetFormater));
        sb.append("}");

        return jsonResult(ok(sb.toString()));
    }

    @Transactional(readOnly = true)
    public Result getResultSetFromModelId(int idModel)
    {
        List<models.LCA.LCIResultSet> lstVal = null;
        try
        {
            Query query = JPA.em().createQuery("Select m from LCIResultSet m where idBimModel =" + idModel);
            lstVal = query.getResultList();
        }
        catch (NoResultException e) {}

        StringBuilder sb = new StringBuilder();
        sb.append("{");
        sb.append(JSONUtil.parseArray("lst", lstVal, JSONFormater.LCIResultSetFormater));
        sb.append("}");

        return jsonResult(ok(sb.toString()));
    }
}

package controllers.API.model;

import controllers.API.model.generic.DataController;
import org.openlca.core.model.Unit;
import play.db.jpa.JPA;
import play.db.jpa.JPAApi;
import play.db.jpa.Transactional;
import play.mvc.Result;
import util.JSONFormater;
import util.JSONUtil;

import javax.inject.Inject;
import javax.persistence.Query;
import java.util.List;

public class UnitGroup extends DataController<org.openlca.core.model.UnitGroup> {
    @Inject
    public UnitGroup(JPAApi api) {
        super(api);
        init(org.openlca.core.model.UnitGroup.class,JSONFormater.UnitGroupFormater, null);
    }

    @Transactional(readOnly = true)
    public Result getUnits(Long id) {
        Query q = JPA.em().createQuery("Select u From UnitGroup as ug inner join ug.units as u " +
                "Where ug.id=:idUnitGroup", org.openlca.core.model.Unit.class).setParameter("idUnitGroup", id);
        List<Unit> units = q.getResultList();
        if (units.isEmpty())
            return jsonResult(notFound(EMPTY_REST_RESULT));

        return jsonResult(ok("{" + JSONUtil.parseArray("lst", units, JSONFormater.UnitFormater) + "}"));
    }
}

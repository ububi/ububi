/**---------------------------   
 * PROJECT: UBUBI
 * Auth:
 *   Mathieu Dupuis
 * Date: ETE 2017
 *
 * Copyright [2017] [Mathieu Dupuis]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. 
 *
 *--------------------------*/
package controllers.API;

import controllers.UBUBIController;
import play.Application;
import play.api.Play;
import play.db.jpa.JPAApi;
import play.mvc.*;
import scala.App;
import util.*;
import org.ububiGroup.ububi.BuildInfo;

import javax.inject.Inject;

/**
 * Created by mathieu on 6/18/2016.
 */
public class Service extends UBUBIController
{
    @Inject
    public Service(JPAApi api) {
        super(api);
    }

    public Result info()
    {
        //If you see error, it's working. It just the IDE that doesn't understand
        String strData = "{"+
                         JSONUtil.parse("name", BuildInfo.name())+","+
                         JSONUtil.parse("version", BuildInfo.version())+","+
                         JSONUtil.parse("sbtVersion", BuildInfo.sbtVersion())+","+
                         JSONUtil.parse("scalaVersion", BuildInfo.scalaVersion())+","+
                         JSONUtil.parse("exportDir", util.Config.getConfig("exportDir"))+","+
                         JSONUtil.parse("sshHost", util.Config.getConfig("ssh.host"))+","+
                         JSONUtil.parse("sparkHost", util.Spark.getHost())+","+
                         JSONUtil.parse("sparkPort", util.Spark.getPort())+
                         "}" ;
        return ok(strData);


    }

}

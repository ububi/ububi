/**---------------------------   
 * PROJECT: UBUBI
 * Auth:
 *   Mathieu Dupuis
 * Date: ETE 2017
 *
 * Copyright [2017] [Mathieu Dupuis]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. 
 *
 *--------------------------*/
package controllers.API;

import java.io.File;
import java.io.IOException;
import java.io.*;
import java.math.BigInteger;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import akka.actor.ActorSystem;
import akka.stream.ActorMaterializer;
import akka.stream.ActorMaterializerSettings;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.jcraft.jsch.JSchException;
import models.LCA.BIMModel;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;

import org.asynchttpclient.AsyncHttpClientConfig;
import org.asynchttpclient.DefaultAsyncHttpClientConfig;
import play.api.Environment;
import play.db.Database;
import org.openlca.core.model.ImpactMethod;
import play.libs.ws.WSResponse;
import play.libs.ws.ahc.AhcWSClient;
import play.mvc.Http.MultipartFormData.FilePart;
import play.mvc.Http.MultipartFormData.DataPart;

import akka.stream.javadsl.FileIO;
import akka.stream.javadsl.Source;
import controllers.UBUBIController;
import play.db.jpa.JPA;
import play.db.jpa.JPAApi;

import play.db.jpa.Transactional;
import play.libs.ws.WSClient;
import play.mvc.Http;
import play.mvc.Result;
import util.Config;
import util.SCPClient;
import util.SparkService;
import util.*;

import javax.inject.Inject;
import javax.persistence.*;

public class Spark extends UBUBIController {

    @Inject
    WSClient ws;

    @Inject
    private Database database;

    @Inject
    private controllers.API.model.Category categoryController;

    private final String EXCHANGES_FILENAME = "exchangesDiff.spark";
    private final String FLOWS_FILENAME = "flowsDiff.spark";
    private final String PARAMETERS_FILENAME = "parametersDiff.spark";
    private final String PROCESSES_FILENAME = "processesDiff.spark";
    private final String UNITS_FILENAME = "units.spark";

    @Inject
    public Spark(JPAApi api) {
        super(api);
    }

    @Inject
    private Environment environment;

    /*
    LastSync second since 1970 (date.getTime()/1000)
     */
    @Transactional
    public Result DifferentialSync(Long LastSync) {
        return ok(DBDifferentialSync(LastSync, JPA.em()).toString());
    }


    private Long DBDifferentialSync(Long LastSync, EntityManager em) {
        Long returnValue = -1l;

        try {
            StoredProcedureQuery spDiffSync = em.createStoredProcedureQuery("UBUBI.ExportDiff");

            spDiffSync.registerStoredProcedureParameter("ReservedID", Long.class, ParameterMode.OUT);
            spDiffSync.registerStoredProcedureParameter("BDName", String.class, ParameterMode.IN).setParameter("BDName", getDbName());
            spDiffSync.registerStoredProcedureParameter("LastSync", Long.class, ParameterMode.IN).setParameter("LastSync", LastSync);

            spDiffSync.execute();

            returnValue = (Long) spDiffSync.getOutputParameterValue("ReservedID");
        } catch (Exception e) {
            e.printStackTrace();
        }

        return returnValue;
    }

    @Transactional
    public Result CompleteSync() {
        Long returnValue = -1l;

        try {

            StoredProcedureQuery spDiffSync = JPA.em().createStoredProcedureQuery("UBUBI.ExportAll");

            spDiffSync.registerStoredProcedureParameter(1, Long.class, ParameterMode.OUT);
            spDiffSync.registerStoredProcedureParameter(2, String.class, ParameterMode.IN).setParameter(2, getDbName());

            spDiffSync.execute();

            returnValue = (Long) spDiffSync.getOutputParameterValue(1);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return ok(returnValue.toString());
    }

    @Transactional
    public Result GetExcelFile(Long id) throws SQLException, IOException{

        String filename = "UBUBI-Result-"+id+".xlsx";
        String filepath = "reports"+File.separator+Config.getConfig("spark.prefix")+File.separator+filename;

        //Verify if already exist
        InputStream inputStream = ExcelReport.getReportFile(filepath);

        // If the report is not already present on the server.
        if(inputStream == null){
            ExcelReport report = new ExcelReport(JPA.em(),id);
            report.generateReport();
            report.save(filepath);
            inputStream = report.getStream();
            report.close();
        }

        // Set the name of the file.
        response().setHeader("Content-disposition", "attachment; filename=" + filename);

        // Return the file as an xlxs file by using the MIME Type associated with the xlsx file.
        return ok(inputStream).as("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
    }

    @Transactional(readOnly = true)
    public Result Calculate(Long id, Long idMethod, String description)
    {
        return calculateResult(id,idMethod,description);
    }

    public Result calculateResult(Long id, Long idMethod, String description)
    {
        WSClient client=getHttpClient();

        int idMult= Config.getIntConfig("spark.idMult");
        Long sparkId = id*idMult;

        try {
            // Getting the BIMModel from the id
            BIMModel model = JPA.em().find(BIMModel.class, id);

            //If not found, sending an error.
            if (model == null)
                return jsonResult(notFound(EMPTY_REST_RESULT));

            String projectVersionsServiceUrl = util.Spark.getServiceURL(SparkService.projectVersions) + "/" + sparkId;

            // Gets the latest project version for this project ID
            WSResponse projectVersionsResponse = client.url(projectVersionsServiceUrl).get().toCompletableFuture().get();
            JsonNode versionsArray = projectVersionsResponse.asJson().get("versions");
            int lastVersion = 1;
            for (JsonNode objNode : versionsArray) {
                lastVersion = objNode.asInt()+1;
            }


            String calculateServiceUrl = util.Spark.getServiceURL(SparkService.calculate);
            List<Http.MultipartFormData.Part> lstParam = new ArrayList<>();

            //Get the last time we sync spark with mysql
            long lastSync = 0;
            String DBName=getDbName();
            Query qLastSync = JPA.em().createNativeQuery("SELECT LastSync FROM UBUBI.LastSync where BDName='"+DBName+"'");
            try {
                lastSync = ((BigInteger) qLastSync.getSingleResult()).longValue();
            }
            catch (NoResultException e) {}

            System.out.println("Last Sync: + "+ lastSync);

            //Write the last time we sync spark with mysql
            if(JPA.em().createNativeQuery("update UBUBI.LastSync set LastSync=UNIX_TIMESTAMP(NOW()) where BDName='"+DBName+"'").executeUpdate()==0)
                JPA.em().createNativeQuery("insert UBUBI.LastSync(BDName,LastSync) values('"+DBName+"',UNIX_TIMESTAMP(NOW()))").executeUpdate();

            // Generate and copy files generated by the DB
            String pathPrefix = getSyncFile(lastSync,JPA.em());

            //Mandatory post parameters
            lstParam.add(new DataPart("ProcessId", model.getIdRootProcess() + ""));
            lstParam.add(new DataPart("quantity", "1"));
            lstParam.add(new DataPart("ProjectId", sparkId + ""));
            lstParam.add(new DataPart("LCIA", "true"));
            lstParam.add(new DataPart("MonteCarlo", "true"));
            lstParam.add(new DataPart("Montecarlo_Iterations", Config.getConfig("spark.mcIter")));
            lstParam.add(new DataPart("LCIAMethod", idMethod.toString()));
            lstParam.add(new DataPart("prefix", util.Spark.getPrefix()));
            lstParam.add(new DataPart("version", lastVersion+""));

            //Optional post parameters
            lstParam.add(new DataPart("DBTemplateId", "1"));

            // Sends the file copied locally
            lstParam.add(createFilePart("exchanges_deltas", pathPrefix, EXCHANGES_FILENAME));
            lstParam.add(createFilePart("parameters_deltas", pathPrefix, PARAMETERS_FILENAME));
            lstParam.add(createFilePart("processes_deltas", pathPrefix, PROCESSES_FILENAME));
            lstParam.add(createFilePart("flows_deltas", pathPrefix, FLOWS_FILENAME));
            lstParam.add(createFilePart("units_deltas", pathPrefix, UNITS_FILENAME));  //TODO Change it

            System.out.println("---------------Start-----------------");
            System.out.println("Post params:");
            for(Http.MultipartFormData.Part i : lstParam)
            {
                String key="unknown";
                String value=i.toString();

                if(i instanceof DataPart)
                {
                    DataPart tmp =(DataPart) i;
                    key = tmp.getKey();
                    value = tmp.getValue();
                }
                else if (i instanceof FilePart)
                {
                    FilePart tmp =(FilePart) i;
                    key = tmp.getKey();
                    value = tmp.getFilename();
                }
                System.out.println(key+"="+value);
            }

            Date start = new Date();
            JsonNode calculateResult = client.url(calculateServiceUrl).post(Source.from(lstParam)).
                    toCompletableFuture().get().asJson();
            Date end = new Date();

            System.out.println("Result:"+calculateResult.toString());
            System.out.println("Calculation time (ms):"+(end.getTime()-start.getTime()));

            JsonNode state=calculateResult.get("state");
            if(state==null || state.asText().toUpperCase().compareTo("\"SUCCESS\"")!=0)
            {
                System.out.println("Error calculate="+calculateResult.toString());
                return jsonResult(ok(calculateResult.toString()));
            }

            //Save the model
            ObjectNode calculateResultObj = (ObjectNode)calculateResult;
            calculateResultObj.put("idModel",id);
            calculateResultObj.put("description",description);

            start = new Date();
            JsonNode saveResult = client.url(routes.Spark.saveResult().absoluteURL(request())).
                    post(calculateResultObj).toCompletableFuture().get().asJson();
            end = new Date();

            System.out.println("Save Result:"+saveResult.toString());
            System.out.println("Save Result time (ms):"+(end.getTime()-start.getTime()));
            System.out.println("----------------End------------------");
            return jsonResult(ok(saveResult.toString()));
        } catch (Exception e) {

            System.out.println("Error : " + e.getMessage());
            e.printStackTrace();

            return jsonResult(exception(e));
        }
        finally {
            try {
                client.close();
            }
            catch (Exception e){}
        }
    }

    @Transactional
    public Result saveResult()
    {
        InsertModelLCADatabase insertModelLCADatabase=null;
        JsonNode json = request().body().asJson();

        try
        {
            insertModelLCADatabase = new InsertModelLCADatabase(json, database);
            insertModelLCADatabase.startInsertingModelInDatabase();
        }
        catch (Exception e)
        {
            if (insertModelLCADatabase != null)
                insertModelLCADatabase.removeImportedFiles();
            System.out.println("Error : " + e.getMessage());
            e.printStackTrace();

            return jsonResult(exception(e));
        }
        return jsonResult(ok("{\"state\":\"success\"}"));
    }

    @Transactional
    public List<String> getTree() throws SQLException {
        List<String> tree = new ArrayList<>();

        PreparedStatement stmt = database.getConnection().prepareStatement("select flow.name as substance, flow.f_category as flow_category_id, p.name as process_name, p.f_category as process_category_id, u.name as unit, lci.processScalar as scalar from tbl_LCI lci " +
                "inner join tbl_flows flow on lci.flow_id = flow.id " +
                "inner join tbl_flow_property_factors fpf on flow.id = fpf.f_flow " +
                "inner join tbl_flow_properties fp on fpf.f_flow_property = fp.id " +
                "inner join tbl_unit_groups ug on fp.f_unit_group = ug.id " +
                "inner join tbl_units u on ug.f_reference_unit = u.id " +
                "inner join tbl_processes p on lci.process_id = p.id " +
                "where lci.resultSet_id=?;");

        ResultSet resultSet = stmt.executeQuery();

        while(resultSet.next()){
            tree.add(resultSet.getString("process_name"));
        }

        return tree;
    }

    @Transactional(readOnly = true)
    public Result getAllImpactMethod() {

        Query query = JPA.em().createQuery("Select m from ImpactMethod m order by m.name");

        List<ImpactMethod> impactMethods = query.getResultList();
        if (impactMethods == null || impactMethods.size() == 0)
            return jsonResult(notFound());

        return jsonResult(ok("{"+JSONUtil.parseArray("lst",impactMethods,JSONFormater.ImpactMethodFormater)+"}"));
    }

    public Result Test()
    {
        return jsonResult(ok(EMPTY_REST_RESULT));
    }

    public Result Test2(Long ms)
    {
        return jsonResult(ok(EMPTY_REST_RESULT));
    }

    private FilePart createFilePart(String name, String pathPrefix, String filename) {
        return new FilePart(name, filename, "text/plain", FileIO.fromFile(new File(pathPrefix + filename)));
    }

    private String getSyncFile(Long lastSync, EntityManager em) throws JSchException, IOException {

        Long Id = DBDifferentialSync(lastSync, em);

        String path = util.Config.getConfig("exportDir") + "/";
        String sshHost = util.Config.getConfig("ssh.host");

        //if mysql server is local
        if ("".compareTo(sshHost) == 0) {
            return path + Id.toString();
        }

        path += Id.toString();

        //get temp folder
        String prefixPath = System.getProperty("java.io.tmpdir");

        if(!prefixPath.endsWith(File.separator))
            prefixPath+=File.separator;

        prefixPath+= Id.toString();

        SCPClient scpClient = new SCPClient();

        scpClient.connect(sshHost,
                util.Config.getConfig("ssh.user"),
                util.Config.getConfig("ssh.password"),
                util.Config.getIntConfig("ssh.port"));

        scpClient.getFile(path + EXCHANGES_FILENAME, prefixPath + EXCHANGES_FILENAME);
        scpClient.getFile(path + FLOWS_FILENAME, prefixPath + FLOWS_FILENAME);
        scpClient.getFile(path + PARAMETERS_FILENAME, prefixPath + PARAMETERS_FILENAME);
        scpClient.getFile(path + PROCESSES_FILENAME, prefixPath + PROCESSES_FILENAME);
        scpClient.getFile(path + UNITS_FILENAME, prefixPath + UNITS_FILENAME);

        scpClient.disconnect();

        return prefixPath;
    }

    private static String getDbName() {
        String bd = Config.getConfig("db.default.url");
        int pos = bd.lastIndexOf("/");
        return bd.substring(pos + 1);
    }

    private WSClient getHttpClient()
    {
        String name = "wsclient";
        ActorSystem system = ActorSystem.create(name);
        ActorMaterializerSettings settings = ActorMaterializerSettings.create(system);
        ActorMaterializer materializer = ActorMaterializer.create(settings, system, name);

        // Set up AsyncHttpClient directly from config
        int customTimeout=-1;
        AsyncHttpClientConfig asyncHttpClientConfig = new DefaultAsyncHttpClientConfig.Builder()
                .setMaxRequestRetry(0)
                .setShutdownQuietPeriod(0)
                .setShutdownTimeout(0)
                .setConnectTimeout(-1)
                .setReadTimeout(-1)
                .setRequestTimeout(-1).build();


        // Set up WSClient instance directly from asynchttpclient.
        WSClient client = new AhcWSClient(asyncHttpClientConfig,materializer);
        return client;
    }
}

/**---------------------------   
 * PROJECT: UBUBI
 * Auth:
 *   Mathieu Dupuis
 * Date: ETE 2017
 *
 * Copyright [2017] [Mathieu Dupuis]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. 
 *
 *--------------------------*/
package controllers.API;

import controllers.UBUBIController;
import models.LCA.BIMProcess;
import models.LCA.Log;
import play.mvc.BodyParser;
import play.mvc.Http;
import util.IED2LCA;
import play.db.jpa.JPA;
import play.db.jpa.JPAApi;
import play.db.jpa.Transactional;
import play.mvc.Result;
import play.mvc.Http.MultipartFormData.FilePart;
import play.mvc.Http.MultipartFormData;

import javax.inject.Inject;
import java.io.File;
import java.io.IOException;
import java.math.BigInteger;
import java.util.Date;
import java.util.Map;

/**
 * Created by mathieu on 7/12/2016.
 */
public class IED extends UBUBIController {
    @Inject
    public IED(JPAApi api) {
        super(api);
    }

    @Transactional
    public Result upload()
    {
        Date begin = new Date();

        File iedFile = null;
        MultipartFormData body = request().body().asMultipartFormData();
        Map<String, String[]> resultMap = body.asFormUrlEncoded();

        String modelName = resultMap.get("name")[0];
        boolean isEdit = Boolean.valueOf(resultMap.get("isEdit")[0]);


        //get materials file
        FilePart materialFormPart = body.getFile("ied");
        if (materialFormPart == null)
            return ok("Missing ied file");
        iedFile = (File)materialFormPart.getFile();

        Log trace = new Log();
        trace.setAction("Convert IED");
        try {

            trace.setIdBegin(((BigInteger)JPA.em().createNativeQuery("SELECT SEQ_COUNT FROM SEQUENCE where SEQ_NAME=\"entity_seq\"").getSingleResult()).longValue());
        }
        catch (Exception e){
            e.printStackTrace();
        }

        IED2LCA converter = new IED2LCA(JPA.em());

        try
        {
            long modelId = Long.parseLong(resultMap.get("modelId")[0]);

            converter.convert(modelId,modelName,iedFile.getPath());
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        Date end = new Date();

        System.out.println("[Info] IED Model created: " + converter.getBimModel().getId()+ " in "+
                            (end.getTime()-begin.getTime())+" ms");

        JPA.em().flush();
        JPA.em().joinTransaction();

        try {
            trace.setIdEnd((Long)JPA.em().createQuery("SELECT max(e.id) FROM Exchange e").getSingleResult());
        }
        catch (Exception e){
            e.printStackTrace();
        }

        trace.setDescrition("Model:"+converter.getBimModel().getId()+";Duration:"+(end.getTime()-begin.getTime())+"ms;"+converter.getLogDetails());
        JPA.em().persist(trace);

        return ok(converter.getBimModel().getId()+"");
    }


    public Result delete()
    {

        return ok();
    }

    @Transactional
    public Result uploadForm(long id)
    {
        return ok();
    }

}

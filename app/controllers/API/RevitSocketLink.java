package controllers.API;

import com.fasterxml.jackson.databind.JsonNode;
import controllers.UBUBIController;
import play.db.jpa.JPA;
import play.db.jpa.JPAApi;
import play.db.jpa.Transactional;
import play.mvc.Result;
import util.JSONUtil;
import util.RevitLink;
import util.RevitSocket;

import javax.inject.Inject;
import javax.persistence.NoResultException;

public class RevitSocketLink extends UBUBIController
{

    @Inject
    public RevitSocketLink(JPAApi api)
    {
        super(api);
    }

    /**
     * Sends the id to the socket.  Revit needs to be synced with the server socket here.
     * @return OK if succesful, FAILED if not.
     */
    @Transactional
    public Result selectId()
    {
        String token=RevitLink.getToken(session());
        long processId=-1;
        try {
            JsonNode json = request().body().asJson();
            processId = json.get("id").asLong();
        }
        catch (Exception e)
        {
            return jsonResult(ok("{\"error\":\"1\",\"message\":\"Invalid or missing process id.\"}"));
        }

        long bimKey = -1;

        RevitSocket socket = RevitLink.getSocket(session());
        if(socket!=null)
        {
            bimKey = getBimKey(processId);
            if(bimKey==-1)
                return jsonResult(ok("{\"error\":\"2\",\"message\":\"Process id not found or not linked to a building part.\"}"));

            try
            {
                socket.selectPartInRevit(bimKey);
            }
            catch (Exception e)
            {
                return jsonResult(ok("{\"error\":\"3\"," + JSONUtil.parseEscapedString("message", e.getMessage()) + "}"));
            }
        }
        else
            return jsonResult(ok("{\"error\":\"4\",\"message\":\"Revit not connected\"}"));

        return jsonResult(ok("{\"error\":\"0\",\"message\":\"Token:"+token+" Id:"+processId+"\"}"));
    }

    /**
     * @param processId
     * @return The id used in Revit
     */
    public long getBimKey(long processId)
    {
        long id = -1;
        try
        {
            id = Long.parseLong(JPA.em().createNativeQuery("SELECT bimKey from tbl_BIMProcesses where process_id = :processId and bimEntityType = \"BIMObject\"").
                                setParameter("processId",processId).
                                getSingleResult().toString(),10);
        }
        catch (NoResultException e)
        {}

        return id;
    }

    /**
     * Opens a socket and stores it into a hash map with a random token as the key
     *
     * @return token
     */
    @Transactional
    public Result requestToken()
    {
        String token = RevitLink.getToken(session());
        return ok("{"+ JSONUtil.parse("token",token)+"}");
    }

    @Transactional
    public Result renewToken()
    {
        String token = RevitLink.createNewToken(session());
        return ok("{"+ JSONUtil.parse("token",token)+"}");
    }
}

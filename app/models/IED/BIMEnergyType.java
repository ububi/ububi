package models.IED;

import org.ububiGroup.IEDManager.model.generic.IEDUnitDimension;

/**
 * Created by Sebastien on 15/07/2017.
 */


//faire un get de dimmention dans l'enum pour l'avoir dans la creation du processus
public enum BIMEnergyType
{
    GazNaturel,
    Mazoute,
    Elecricite,
    Propane,
    Other;

    public static IEDUnitDimension returnEnergyDimention(BIMEnergyType energy)
    {

        switch(energy){
            case GazNaturel: return IEDUnitDimension.VolumeByTime;
            case Mazoute: return IEDUnitDimension.VolumeByTime;
            case Elecricite: return IEDUnitDimension.Item;
            case Propane: return IEDUnitDimension.VolumeByTime;
            case Other: return IEDUnitDimension.Item;
            default: return IEDUnitDimension.Item;

        }
    }

}


package models.IED;

/**
 * Created by Sebastien on 24/07/2017.
 */

import javax.persistence.*;
import java.util.HashMap;

public class BIMUniformatClassification {

    public static final BIMUniformatClassification EMPTY_UNIFORMAT = new BIMUniformatClassification("","",-1);
    private static HashMap<String,BIMUniformatClassification> mapUniformat;

    private String key;
    private String name;
    private int rank;

    public String getKey() {
        return key;
    }

    private void setKey(String key) {
        this.key = key;
    }

    public String getName() {
        return name;
    }

    private void setName(String name) {
        this.name = name;
    }

    public int getRank() {
        return rank;
    }

    private void setRank(int rank) {
        this.rank = rank;
    }

    private BIMUniformatClassification(String key, String name, int rank) {
        this.key = key;
        this.name = name;
        this.rank = rank;

    }

    public static BIMUniformatClassification getUniformat(String key)
    {
        return getUniformat(key,null);
    }

    public static BIMUniformatClassification getUniformat(String key, BIMUniformatClassification defaultValue)
    {
        if(mapUniformat==null)
            init();
        return mapUniformat.getOrDefault(key,defaultValue);
    }

    public static String getUniformatName(String key)
    {
        return getUniformatName(key,"");
    }

    public static String getUniformatName(String key,String defaultName)
    {
        String name = defaultName;
        BIMUniformatClassification uniformat = getUniformat(key);
        if(uniformat!=null)
        {
            name = uniformat.getName();
        }

        return name;
    }

    private static void init()
    {
        if(mapUniformat!=null)
            return;

        mapUniformat=new HashMap<>();
        BIMUniformatClassification tmp = null;

        tmp=new BIMUniformatClassification("A","Substructure",1); mapUniformat.put(tmp.key,tmp);
        tmp=new BIMUniformatClassification("A10","Foundations",2); mapUniformat.put(tmp.key,tmp);
        tmp=new BIMUniformatClassification("A1010","Standard Foundations",3); mapUniformat.put(tmp.key,tmp);
        tmp=new BIMUniformatClassification("A1020","Special Foundations",3); mapUniformat.put(tmp.key,tmp);
        tmp=new BIMUniformatClassification("A1030","Slab on Grade",3); mapUniformat.put(tmp.key,tmp);

        tmp=new BIMUniformatClassification("A20","Basement Construction",2); mapUniformat.put(tmp.key,tmp);
        tmp=new BIMUniformatClassification("A2010","Basement Excavation",3); mapUniformat.put(tmp.key,tmp);
        tmp=new BIMUniformatClassification("A2020","Basement Walls",3); mapUniformat.put(tmp.key,tmp);


        tmp=new BIMUniformatClassification("B","Shell",1); mapUniformat.put(tmp.key,tmp);
        tmp=new BIMUniformatClassification("B10","Superstructure",2); mapUniformat.put(tmp.key,tmp);
        tmp=new BIMUniformatClassification("B1010","Floor Construction",3); mapUniformat.put(tmp.key,tmp);
        tmp=new BIMUniformatClassification("B1020","Roof Construction",3); mapUniformat.put(tmp.key,tmp);

        tmp=new BIMUniformatClassification("B20","Exterior Enclosure",3); mapUniformat.put(tmp.key,tmp);
        tmp=new BIMUniformatClassification("B2010","Exterior Walls",3); mapUniformat.put(tmp.key,tmp);
        tmp=new BIMUniformatClassification("B2020","Exterior Windows",3); mapUniformat.put(tmp.key,tmp);
        tmp=new BIMUniformatClassification("B2030","Exterior Doors",3); mapUniformat.put(tmp.key,tmp);

        tmp=new BIMUniformatClassification("B30","Roofing",2); mapUniformat.put(tmp.key,tmp);
        tmp=new BIMUniformatClassification("B3010","Roof Coverings",3); mapUniformat.put(tmp.key,tmp);
        tmp=new BIMUniformatClassification("B3020","Roof Openings",3); mapUniformat.put(tmp.key,tmp);


        tmp=new BIMUniformatClassification("C","Interiors",1); mapUniformat.put(tmp.key,tmp);
        tmp=new BIMUniformatClassification("C10","Interior Construction",2); mapUniformat.put(tmp.key,tmp);
        tmp=new BIMUniformatClassification("C1010","Partitions",3); mapUniformat.put(tmp.key,tmp);
        tmp=new BIMUniformatClassification("C1020","Interior Doors",3); mapUniformat.put(tmp.key,tmp);
        tmp=new BIMUniformatClassification("C1030","Fittings",3); mapUniformat.put(tmp.key,tmp);

        tmp=new BIMUniformatClassification("C20","Stairs",2); mapUniformat.put(tmp.key,tmp);
        tmp=new BIMUniformatClassification("C2010","Stair Construction",3); mapUniformat.put(tmp.key,tmp);
        tmp=new BIMUniformatClassification("C2020","Stair Finishes",3); mapUniformat.put(tmp.key,tmp);

        tmp=new BIMUniformatClassification("C30","Interior Finishes",2); mapUniformat.put(tmp.key,tmp);
        tmp=new BIMUniformatClassification("C3010","Wall Finishes",3); mapUniformat.put(tmp.key,tmp);
        tmp=new BIMUniformatClassification("C3020","Floor Finishes",3); mapUniformat.put(tmp.key,tmp);
        tmp=new BIMUniformatClassification("C3030","Ceiling Finishes",3); mapUniformat.put(tmp.key,tmp);


        tmp=new BIMUniformatClassification("D","Services",1); mapUniformat.put(tmp.key,tmp);
        tmp=new BIMUniformatClassification("D10","Conveying",2); mapUniformat.put(tmp.key,tmp);
        tmp=new BIMUniformatClassification("D1010","Elevators and Lifts",3); mapUniformat.put(tmp.key,tmp);
        tmp=new BIMUniformatClassification("D1020","Escalators and Moving Walks",3); mapUniformat.put(tmp.key,tmp);
        tmp=new BIMUniformatClassification("D1090","Other Conveying Systems",3); mapUniformat.put(tmp.key,tmp);

        tmp=new BIMUniformatClassification("D20","Plumbing",2); mapUniformat.put(tmp.key,tmp);
        tmp=new BIMUniformatClassification("D2010","Plumbing Fixtures",3); mapUniformat.put(tmp.key,tmp);
        tmp=new BIMUniformatClassification("D2020","Domestic Water Distribution",3); mapUniformat.put(tmp.key,tmp);
        tmp=new BIMUniformatClassification("D2030","Sanitary Waste",3); mapUniformat.put(tmp.key,tmp);
        tmp=new BIMUniformatClassification("D2040","Rain Water Drainage",3); mapUniformat.put(tmp.key,tmp);
        tmp=new BIMUniformatClassification("D2090","Other Plumbing Systems",3); mapUniformat.put(tmp.key,tmp);

        tmp=new BIMUniformatClassification("D30","HVAC",2); mapUniformat.put(tmp.key,tmp);
        tmp=new BIMUniformatClassification("D3010","Energy Supply",3); mapUniformat.put(tmp.key,tmp);
        tmp=new BIMUniformatClassification("D3020","Heat Generating Systems",3); mapUniformat.put(tmp.key,tmp);
        tmp=new BIMUniformatClassification("D3030","Cooling Generating Systems",3); mapUniformat.put(tmp.key,tmp);
        tmp=new BIMUniformatClassification("D3040","Distribution Systems",3); mapUniformat.put(tmp.key,tmp);
        tmp=new BIMUniformatClassification("D3050","Terminal and Package Units",3); mapUniformat.put(tmp.key,tmp);
        tmp=new BIMUniformatClassification("D3060","Controls and Instrumentation",3); mapUniformat.put(tmp.key,tmp);
        tmp=new BIMUniformatClassification("D3070","Systems Testing and Balancing",3); mapUniformat.put(tmp.key,tmp);
        tmp=new BIMUniformatClassification("D3090","Other HVAC Systems and Equipment",3); mapUniformat.put(tmp.key,tmp);

        tmp=new BIMUniformatClassification("D40","Fire Protection",2); mapUniformat.put(tmp.key,tmp);
        tmp=new BIMUniformatClassification("D4010","Sprinklers",3); mapUniformat.put(tmp.key,tmp);
        tmp=new BIMUniformatClassification("D4020","Standpipes",3); mapUniformat.put(tmp.key,tmp);
        tmp=new BIMUniformatClassification("D4030","Fire Protection Specialties",3); mapUniformat.put(tmp.key,tmp);
        tmp=new BIMUniformatClassification("D4090","Other Fire Protection Systems",3); mapUniformat.put(tmp.key,tmp);

        tmp=new BIMUniformatClassification("D50","Electrical",2); mapUniformat.put(tmp.key,tmp);
        tmp=new BIMUniformatClassification("D5010","Electrical Service and Distribution",3); mapUniformat.put(tmp.key,tmp);
        tmp=new BIMUniformatClassification("D5020","Lighting and Branch Wiring",3); mapUniformat.put(tmp.key,tmp);
        tmp=new BIMUniformatClassification("D5030","Communications and Security",3); mapUniformat.put(tmp.key,tmp);
        tmp=new BIMUniformatClassification("D5090","Other Electrical Systems",3); mapUniformat.put(tmp.key,tmp);


        tmp=new BIMUniformatClassification("E","Equipment and Furnishings",1); mapUniformat.put(tmp.key,tmp);
        tmp=new BIMUniformatClassification("E10","Equipment",2); mapUniformat.put(tmp.key,tmp);
        tmp=new BIMUniformatClassification("E1010","Commercial Equipment",3); mapUniformat.put(tmp.key,tmp);
        tmp=new BIMUniformatClassification("E1020","Institutional Equipment",3); mapUniformat.put(tmp.key,tmp);
        tmp=new BIMUniformatClassification("E1030","Vehicular Equipment",3); mapUniformat.put(tmp.key,tmp);
        tmp=new BIMUniformatClassification("E1090","Other Equipment",3); mapUniformat.put(tmp.key,tmp);

        tmp=new BIMUniformatClassification("E20","Furnishings",2); mapUniformat.put(tmp.key,tmp);
        tmp=new BIMUniformatClassification("E2010","Fixed Furnishings",3); mapUniformat.put(tmp.key,tmp);
        tmp=new BIMUniformatClassification("E2020","Movable Furnishings",3); mapUniformat.put(tmp.key,tmp);


        tmp=new BIMUniformatClassification("F","Special Construction and Demolition",1); mapUniformat.put(tmp.key,tmp);
        tmp=new BIMUniformatClassification("F10","Special Construction",2); mapUniformat.put(tmp.key,tmp);
        tmp=new BIMUniformatClassification("F1010","Special Structures",3); mapUniformat.put(tmp.key,tmp);
        tmp=new BIMUniformatClassification("F1020","Integrated Construction",3); mapUniformat.put(tmp.key,tmp);
        tmp=new BIMUniformatClassification("F1030","Special Construction Systems",3); mapUniformat.put(tmp.key,tmp);
        tmp=new BIMUniformatClassification("F1040","Special Facilities",3); mapUniformat.put(tmp.key,tmp);
        tmp=new BIMUniformatClassification("F1050","Special Controls and Instrumentation",3); mapUniformat.put(tmp.key,tmp);

        tmp=new BIMUniformatClassification("F20","Selective Building Demolition",2); mapUniformat.put(tmp.key,tmp);
        tmp=new BIMUniformatClassification("F2010","Building Elements Demolition",3); mapUniformat.put(tmp.key,tmp);
        tmp=new BIMUniformatClassification("F2020","Hazardous Components Abatement",3); mapUniformat.put(tmp.key,tmp);
    }
}

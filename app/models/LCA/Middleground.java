package models.LCA;

import javax.persistence.*;

//This class is not used but it create automaticly the table for the controller MiddleGround

@Entity
@Table(name="tbl_middleground_structure")
public class Middleground
{
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUuidSrc() {
        return uuidSrc;
    }

    public void setUuidSrc(String uuidSrc) {
        this.uuidSrc = uuidSrc;
    }

    public String getUuidParent() {
        return uuidParent;
    }

    public void setUuidParent(String uuidParent) {
        this.uuidParent = uuidParent;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getRef() {
        return ref;
    }

    public void setRef(String ref) {
        this.ref = ref;
    }

    public Double getKgbym3() {
        return kgbym3;
    }

    public void setKgbym3(Double kgbym3) {
        this.kgbym3 = kgbym3;
    }

    public Double getKgbym2() {
        return kgbym2;
    }

    public void setKgbym2(Double kgbym2) {
        this.kgbym2 = kgbym2;
    }

    public String getRefDensity() {
        return refDensity;
    }

    public void setRefDensity(String refDensity) {
        this.refDensity = refDensity;
    }

    public Double getRValueIn() {
        return rValueIn;
    }

    public void setRValueIn(Double rValueIn) {
        this.rValueIn = rValueIn;
    }

    public Double getRValue() {
        return rValue;
    }

    public void setRValue(Double rValue) {
        this.rValue = rValue;
    }

    public String getRefRValue() {
        return refRValue;
    }

    public void setRefRValue(String refRValue) {
        this.refRValue = refRValue;
    }

    public Double getThicknessMin() {
        return thicknessMin;
    }

    public void setThicknessMin(Double thicknessMin) {
        this.thicknessMin = thicknessMin;
    }

    public Double getThicknessMax() {
        return thicknessMax;
    }

    public void setThicknessMax(Double thicknessMax) {
        this.thicknessMax = thicknessMax;
    }

    public Integer getIndicator() {
        return indicator;
    }

    public void setIndicator(Integer indicator) {
        this.indicator = indicator;
    }

    public String getUuidProcess() {
        return uuidProcess;
    }

    public void setUuidProcess(String uuidProcess) {
        this.uuidProcess = uuidProcess;
    }

    public String getUuidFlow() {
        return uuidFlow;
    }

    public void setUuidFlow(String uuidFlow) {
        this.uuidFlow = uuidFlow;
    }

    public Long getIdProcess() {
        return idProcess;
    }

    public void setIdProcess(Long idProcess) {
        this.idProcess = idProcess;
    }

    public Long getIdFlow() {
        return idFlow;
    }

    public void setIdFlow(Long idFlow) {
        this.idFlow = idFlow;
    }

    public String getEntityType() {
        return entityType;
    }

    public void setEntityType(String entityType) {
        this.entityType = entityType;
    }

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private long id;

    @Column(name = "uuid_src")
    private String uuidSrc;
    @Column(name = "uuid_parent")
    private String uuidParent;
    private String name;
    private int level;

    private String unit;
    private String ref;

    @Column(nullable = true) private Double kgbym3;
    @Column(nullable = true) private Double kgbym2;
    @Column(nullable = true) private String refDensity;
    @Column(nullable = true) private Double rValueIn;
    @Column(nullable = true) private Double rValue;
    @Column(nullable = true) private String refRValue;

    @Column(nullable = true) private Double thicknessMin;
    @Column(nullable = true) private Double thicknessMax;

    @Column(nullable = true) private Integer indicator;

    @Column(name = "uuid_process",nullable = true)
    private String uuidProcess;

    @Column(name = "uuid_flow",nullable = true)
    private String uuidFlow;

    @Column(name = "process_id",nullable = true)
    private Long idProcess;

    @Column(name = "flow_id",nullable = true)
    private Long idFlow;

    private String entityType;

}

/**---------------------------   
 * PROJECT: UBUBI
 * Auth:
 *   Mathieu Dupuis
 * Date: ETE 2017
 *
 * Copyright [2017] [Mathieu Dupuis]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. 
 *
 *--------------------------*/
package models.LCA;


import javax.persistence.*;
import org.openlca.core.model.ImpactCategory;

/**
 * Created by jape on 8/23/2016.
 */
@Entity
@Table(name="tbl_ULCIA")
public class ULCIA {

    //Uncertainty LCIA

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public LCIResultSet getResultSet() {
        return resultSet;
    }

    public void setResultSet(LCIResultSet resultSet) {
        this.resultSet = resultSet;
    }

    public ImpactCategory getImpactCategory() {
        return impactCategory;
    }

    public void setImpactCategory(ImpactCategory impactCategory) {
        this.impactCategory = impactCategory;
    }

    public double getMean() {
        return mean;
    }

    public void setMean(double mean) {
        this.mean = mean;
    }

    public double getStd() {
        return std;
    }

    public void setStd(double std) {
        this.std = std;
    }

    public double getMedian() {
        return median;
    }

    public void setMedian(double median) {
        this.median = median;
    }

    public double getQuantilesTwoPointFive() {
        return quantilesTwoPointFive;
    }

    public void setQuantilesTwoPointFive(double quantilesTwoPointFive) {
        this.quantilesTwoPointFive = quantilesTwoPointFive;
    }

    public double getQuantilesFive() {
        return quantilesFive;
    }

    public void setQuantilesFive(double quantilesFive) {
        this.quantilesFive = quantilesFive;
    }

    public double getQuantilesTwentyFive() {
        return quantilesTwentyFive;
    }

    public void setQuantilesTwentyFive(double quantilesTwentyFive) {
        this.quantilesTwentyFive = quantilesTwentyFive;
    }

    public double getQuantilesSeventyFive() {
        return quantilesSeventyFive;
    }

    public void setQuantilesSeventyFive(double quantilesSeventyFive) {
        this.quantilesSeventyFive = quantilesSeventyFive;
    }

    public double getQuantilesNinetyFive() {
        return quantilesNinetyFive;
    }

    public void setQuantilesNinetyFive(double quantilesNinetyFive) {
        this.quantilesNinetyFive = quantilesNinetyFive;
    }

    public double getQuantilesNinetySevenPointFive() {
        return quantilesNinetySevenPointFive;
    }

    public void setQuantilesNinetySevenPointFive(double quantilesNinetySevenPointFive) {
        this.quantilesNinetySevenPointFive = quantilesNinetySevenPointFive;
    }

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private long id;

    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(nullable = false)
    private LCIResultSet resultSet;

    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(nullable = false)
    private ImpactCategory impactCategory;

    private double mean; // moyenne

    private double std; //standard deviation, l'ecart type

    private double median;

    private double quantilesTwoPointFive;

    private double quantilesFive;

    private double quantilesTwentyFive;

    private double quantilesSeventyFive;

    private double quantilesNinetyFive;

    private double quantilesNinetySevenPointFive;
}

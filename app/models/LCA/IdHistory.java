package models.LCA;

import java.io.Serializable;

public class IdHistory implements Serializable{
    protected long id;
    protected int revision;

    @Override
    public int hashCode() {
        int hsCode;
        hsCode = (int)((id*37+revision) % Integer.MAX_VALUE);

        return hsCode;
    }

    @Override
    public boolean equals(Object arg0) {
        if(arg0 == null) return false;
        if(!(arg0 instanceof IdHistory)) return false;
        IdHistory arg1 = (IdHistory) arg0;
        return (this.id == arg1.id) &&
                (this.revision == arg1.revision);
    }

}

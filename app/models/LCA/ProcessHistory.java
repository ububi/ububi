package models.LCA;

import org.openlca.core.model.*;
import org.openlca.core.model.Process;

import java.util.Date;

import javax.persistence.*;

@Entity
@Table(name = "tbl_processes_history")
public class ProcessHistory {
    public long getRevision() {
        return revision;
    }

    public void setRevision(long revision) {
        this.revision = revision;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public ActionType getActionType() {
        return actionType;
    }

    public void setActionType(ActionType actionType) {
        this.actionType = actionType;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    public String getRefId() {
        return refId;
    }

    public void setRefId(String refId) {
        this.refId = refId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public long getVersion() {
        return version;
    }

    public void setVersion(long version) {
        this.version = version;
    }

    public long getLastChange() {
        return lastChange;
    }

    public void setLastChange(long lastChange) {
        this.lastChange = lastChange;
    }

    public long getIdCategory() {
        return idCategory;
    }

    public void setIdCategory(long idCategory) {
        this.idCategory = idCategory;
    }

    public AllocationMethod getDefaultAllocationMethod() {
        return defaultAllocationMethod;
    }

    public void setDefaultAllocationMethod(AllocationMethod defaultAllocationMethod) {
        this.defaultAllocationMethod = defaultAllocationMethod;
    }

    public long getIdLocation() {
        return idLocation;
    }

    public void setIdLocation(long idLocation) {
        this.idLocation = idLocation;
    }

    public long getIdDocumentation() {
        return idDocumentation;
    }

    public void setIdDocumentation(long idDocumentation) {
        this.idDocumentation = idDocumentation;
    }

    public ProcessType getProcessType() {
        return processType;
    }

    public void setProcessType(ProcessType processType) {
        this.processType = processType;
    }

    public long getIdQuantitativeReference() {
        return idQuantitativeReference;
    }

    public void setIdQuantitativeReference(long idQuantitativeReference) {
        this.idQuantitativeReference = idQuantitativeReference;
    }

    public boolean isInfrastructureProcess() {
        return infrastructureProcess;
    }

    public void setInfrastructureProcess(boolean infrastructureProcess) {
        this.infrastructureProcess = infrastructureProcess;
    }

    public long getIdCurrency() {
        return idCurrency;
    }

    public void setIdCurrency(long idCurrency) {
        this.idCurrency = idCurrency;
    }

    public long getIdDqSystem() {
        return idDqSystem;
    }

    public void setIdDqSystem(long idDqSystem) {
        this.idDqSystem = idDqSystem;
    }

    public String getDqEntry() {
        return dqEntry;
    }

    public void setDqEntry(String dqEntry) {
        this.dqEntry = dqEntry;
    }

    public long getIdExchangeDqSystem() {
        return idExchangeDqSystem;
    }

    public void setIdExchangeDqSystem(long idExchangeDqSystem) {
        this.idExchangeDqSystem = idExchangeDqSystem;
    }

    public long getIdSocialDqSystem() {
        return idSocialDqSystem;
    }

    public void setIdSocialDqSystem(long idSocialDqSystem) {
        this.idSocialDqSystem = idSocialDqSystem;
    }

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name="revision")
    private long revision;

    @Column(name="id")
    private long id;

    @Column(name="action")
    @Enumerated(EnumType.STRING)
    private ActionType actionType;

    @Column(name="dt_datetime")
    @Temporal(TemporalType.TIMESTAMP)
    private Date timestamp;

    @Column(name = "ref_id")
    private String refId;

    @Column(name = "name")
    private String name;

    @Lob
    @Column(name = "description")
    private String description;

    // @Version
    @Column(name = "version")
    private long version;

    @Column(name = "last_change")
    private long lastChange;

    @Column(name = "f_category")
    private long idCategory;

    @Column(name = "default_allocation_method")
    @Enumerated(EnumType.STRING)
    private AllocationMethod defaultAllocationMethod;

    @Column(name = "f_location")
    private long idLocation;

    @Column(name = "f_process_doc")
    private long idDocumentation;

    @Column(name = "process_type")
    @Enumerated(EnumType.STRING)
    private ProcessType processType = ProcessType.UNIT_PROCESS;

    @Column(name = "f_quantitative_reference")
    private long idQuantitativeReference;

    @Column(name = "infrastructure_process")
    private boolean infrastructureProcess;

    @Column(name = "f_currency")
    public long idCurrency;

    @Column(name = "f_dq_system")
    public long idDqSystem;

    @Column(name = "dq_entry")
    public String dqEntry;

    @Column(name = "f_exchange_dq_system")
    public long idExchangeDqSystem;

    @Column(name = "f_social_dq_system")
    public long idSocialDqSystem;

    @Override
    public Process clone() {
        return null;
    }

}

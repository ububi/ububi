/**---------------------------   
 * PROJECT: UBUBI
 * Auth:
 *   Mathieu Dupuis
 * Date: ETE 2017
 *
 * Copyright [2017] [Mathieu Dupuis]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. 
 *
 *--------------------------*/
package models.LCA;

import org.openlca.core.model.Flow;
import javax.persistence.*;


/**
 * Created by mathieu on 8/23/2016.
 */
@Entity
@Table(name="tbl_BIMFlows")
public class BIMFlow
{
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long id;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(nullable = false)
	private BIMModel model;

	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinColumn(nullable = false)
	private Flow flow;

    @Column(name = "flow_id", insertable = false, updatable = false)
	private long idFlow;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public BIMModel getModel() {
		return model;
	}

	public void setModel(BIMModel model) {
		this.model = model;
	}

	public Flow getFlow() {
		return flow;
	}

	public void setFlow(Flow flow) {
		this.flow = flow;
	}

	public long getIdFlow() {
		return idFlow;
	}

	public void setIdFlow(long idFlow) {
		this.idFlow = idFlow;
	}

	public BIMEntity getBimEntity() {
		return bimEntity;
	}

	public void setBimEntity(BIMEntity bimEntity) {
		this.bimEntity = bimEntity;
	}

	@Embedded
	private BIMEntity bimEntity;

}

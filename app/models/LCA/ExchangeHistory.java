package models.LCA;

import org.openlca.core.model.*;

import java.util.Date;
import javax.persistence.*;

/**
 * Created by arasivaneswaran on 2017-08-08.
 */
@Entity
@Table(name = "tbl_exchanges_history")
public class ExchangeHistory {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name="revision")
    private long revision;

    @Column(name="id")
    private long id;

    @Column(name="action")
    @Enumerated(EnumType.STRING)
    private ActionType actionType;

    @Column(name="dt_datetime")
    @Temporal(TemporalType.TIMESTAMP)
    private Date timestamp;

    @Column(name = "avoided_product")
    private boolean avoidedProduct;

    @Column(name = "f_owner")
    private long idOwner;

    @Column(name = "f_flow")
    private long idFlow;

    @Column(name = "f_flow_property_factor")
    private long idFlowPropertyFactor;

    @Column(name = "is_input")
    private boolean input;

    @Column(name = "base_uncertainty")
    private Double baseUncertainty;

    @Column(name = "f_default_provider")
    private long defaultProviderId;

    @Column(name = "resulting_amount_value")
    private double amountValue;

    @Column(name = "resulting_amount_formula")
    private String amountFormula;

    @Column(name = "f_unit")
    private long idUnit;

    @Column(name = "dq_entry")
    private String dqEntry;

    @Embedded
    private Uncertainty uncertainty;

    @Column(name = "cost_value")
    public Double costValue;

    @Column(name = "cost_formula")
    public String costFormula;

    @Column(name = "description")
    public String description;

    public long getRevision() {
        return revision;
    }

    public void setRevision(long revision) {
        this.revision = revision;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public ActionType getActionType() {
        return actionType;
    }

    public void setActionType(ActionType actionType) {
        this.actionType = actionType;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    public boolean isAvoidedProduct() {
        return avoidedProduct;
    }

    public void setAvoidedProduct(boolean avoidedProduct) {
        this.avoidedProduct = avoidedProduct;
    }

    public long getIdOwner() {
        return idOwner;
    }

    public void setIdOwner(long idOwner) {
        this.idOwner = idOwner;
    }

    public long getIdFlow() {
        return idFlow;
    }

    public void setIdFlow(long idFlow) {
        this.idFlow = idFlow;
    }

    public long getIdFlowPropertyFactor() {
        return idFlowPropertyFactor;
    }

    public void setIdFlowPropertyFactor(long idFlowPropertyFactor) {
        this.idFlowPropertyFactor = idFlowPropertyFactor;
    }

    public boolean isInput() {
        return input;
    }

    public void setInput(boolean input) {
        this.input = input;
    }

    public Double getBaseUncertainty() {
        return baseUncertainty;
    }

    public void setBaseUncertainty(Double baseUncertainty) {
        this.baseUncertainty = baseUncertainty;
    }

    public long getDefaultProviderId() {
        return defaultProviderId;
    }

    public void setDefaultProviderId(long defaultProviderId) {
        this.defaultProviderId = defaultProviderId;
    }

    public double getAmountValue() {
        return amountValue;
    }

    public void setAmountValue(double amountValue) {
        this.amountValue = amountValue;
    }

    public String getAmountFormula() {
        return amountFormula;
    }

    public void setAmountFormula(String amountFormula) {
        this.amountFormula = amountFormula;
    }

    public long getIdUnit() {
        return idUnit;
    }

    public void setIdUnit(long idUnit) {
        this.idUnit = idUnit;
    }

    public String getDqEntry() {
        return dqEntry;
    }

    public void setDqEntry(String dqEntry) {
        this.dqEntry = dqEntry;
    }

    public Uncertainty getUncertainty() {
        return uncertainty;
    }

    public void setUncertainty(Uncertainty uncertainty) {
        this.uncertainty = uncertainty;
    }

    public Double getCostValue() {
        return costValue;
    }

    public void setCostValue(Double costValue) {
        this.costValue = costValue;
    }

    public String getCostFormula() {
        return costFormula;
    }

    public void setCostFormula(String costFormula) {
        this.costFormula = costFormula;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public long getIdCurrency() {
        return idCurrency;
    }

    public void setIdCurrency(long idCurrency) {
        this.idCurrency = idCurrency;
    }

    @Column(name = "f_currency")
    public long idCurrency;

    @Override
    public String toString() {
        return "Exchange [flow=" + idFlow + ", input=" + input + ",amount="
                + amountValue + ", unit=" + idUnit + "]";
    }

    @Override
    public ExchangeHistory clone() {
        return null;
    }
}

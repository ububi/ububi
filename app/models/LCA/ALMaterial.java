/**---------------------------   
 * PROJECT: UBUBI
 * Auth:
 *   Mathieu Dupuis
 * Date: ETE 2017
 *
 * Copyright [2017] [Mathieu Dupuis]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. 
 *
 *--------------------------*/
package models.LCA;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 * Created by mathieu on 8/23/2016.
 */
@Entity
@Table(name="tbl_ALMaterial")
public class ALMaterial
{
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long id;

	@Column(name = "uniformat")
	public String uniformat;

	@Column(name = "pattern")
	public String pattern;

	@Column(name = "processUUID")
	@NotNull
	public String processUUID;

	@Column(name = "ratio")
	public double ratio;

	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinColumn(nullable = true, foreignKey = @ForeignKey( name = "none" , value = ConstraintMode.NO_CONSTRAINT))
	private LCADatabase lcaDatabase;

	@Column(name = "id", insertable = false, updatable = false)
	private long idDatabase;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getUniformat() {
		return uniformat;
	}

	public void setUniformat(String uniformat) {
		this.uniformat = uniformat;
	}

	public String getPattern() {
		return pattern;
	}

	public void setPattern(String pattern) {
		this.pattern = pattern;
	}

	public String getProcessUUID() {
		return processUUID;
	}

	public void setProcessUUID(String processUUID) {
		this.processUUID = processUUID;
	}

	public double getRatio() {
		return ratio;
	}

	public void setRatio(double ratio) {
		this.ratio = ratio;
	}

	public LCADatabase getLcaDatabase() {
		return lcaDatabase;
	}

	public void setLcaDatabase(LCADatabase lcaDatabase) {
		this.lcaDatabase = lcaDatabase;
	}

	public long getIdDatabase() {
		return idDatabase;
	}

	public void setIdDatabase(long idDatabase) {
		this.idDatabase = idDatabase;
	}
}

/**---------------------------   
 * PROJECT: UBUBI
 * Auth:
 *   Mathieu Dupuis
 * Date: ETE 2017
 *
 * Copyright [2017] [Mathieu Dupuis]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. 
 *
 *--------------------------*/
package models.LCA;

import javax.persistence.*;

import org.openlca.core.model.Flow;
import org.openlca.core.model.Process;
import play.data.format.Formats;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by mathieu on 8/23/2016.
 */
@Entity
@Table(name="tbl_BIMModels")
public class BIMModel
{
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long id;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public Process getRootProcess() {
		return rootProcess;
	}

	public void setRootProcess(Process rootProcess) {
		this.rootProcess = rootProcess;
	}

	public long getIdRootProcess() {
		return idRootProcess;
	}

	public void setIdRootProcess(long idRootProcess) {
		this.idRootProcess = idRootProcess;
	}

	public Flow getRootFlow() {
		return rootFlow;
	}

	public void setRootFlow(Flow rootFlow) {
		this.rootFlow = rootFlow;
	}

	public long getIdRootFlow() {
		return idRootFlow;
	}

	public void setIdRootFlow(long idRootFlow) {
		this.idRootFlow = idRootFlow;
	}

	public List<BIMProcess> getProcesses() {
		return processes;
	}

	public void setProcesses(List<BIMProcess> processes) {
		this.processes = processes;
	}

	public List<BIMFlow> getFlows() {
		return flows;
	}

	public void setFlows(List<BIMFlow> flows) {
		this.flows = flows;
	}

	public List<BIMExchange> getExchanges() {
		return exchanges;
	}

	public void setExchanges(List<BIMExchange> exchanges) {
		this.exchanges = exchanges;
	}

	public List<BIMCategory> getCategories() {
		return categories;
	}

	public void setCategories(List<BIMCategory> categories) {
		this.categories = categories;
	}

	@Column(length = 255)
	private String name;

	@Formats.DateTime(pattern="yyyy-MM-dd")
	private Date creationDate;

	@OneToOne()
	private Process rootProcess;

	@Column(name = "rootProcess_id", insertable = false, updatable = false)
	private long idRootProcess;

	@OneToOne
	private Flow rootFlow;

	@Column(name = "rootFlow_id", insertable = false, updatable = false)
	private long idRootFlow;

	@OneToMany(mappedBy="model",fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
	private List<BIMProcess> processes = new ArrayList<>();

	@OneToMany(mappedBy="model",fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
	private List<BIMFlow> flows = new ArrayList<>();

	@OneToMany(mappedBy="model",fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
	private List<BIMExchange> exchanges = new ArrayList<>();

	@OneToMany(mappedBy="model",fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
	private List<BIMCategory> categories = new ArrayList<>();
}

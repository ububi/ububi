package models.LCA;

import org.openlca.core.model.Parameter;
import org.openlca.core.model.ParameterScope;
import org.openlca.core.model.Uncertainty;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "tbl_parameters_history")
public class ParameterHistory {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name="revision")
    private long revision;

    @Column(name="id")
    private long id;

    @Column(name="action")
    @Enumerated(EnumType.STRING)
    private ActionType actionType;

    @Column(name="dt_datetime")
    @Temporal(TemporalType.TIMESTAMP)
    private Date timestamp;

    @Column(name = "ref_id")
    private String refId;

    @Column(name = "name")
    private String name;

    @Lob
    @Column(name = "description")
    private String description;

    // @Version
    @Column(name = "version")
    private long version;

    @Column(name = "last_change")
    private long lastChange;

    @Column(name = "f_category")
    private long idCategory;

    @Column(name = "f_owner")
    private long idOwner;

    @Column(name = "scope")
    @Enumerated(EnumType.STRING)
    private ParameterScope scope = ParameterScope.GLOBAL;

    @Column(name = "is_input_param")
    private boolean inputParameter;

    @Column(name = "value")
    private double value;

    @Column(name = "formula")
    private String formula;

    @Embedded
    private Uncertainty uncertainty;

    public long getRevision() {
        return revision;
    }

    public void setRevision(long revision) {
        this.revision = revision;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public ActionType getActionType() {
        return actionType;
    }

    public void setActionType(ActionType actionType) {
        this.actionType = actionType;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    public String getRefId() {
        return refId;
    }

    public void setRefId(String refId) {
        this.refId = refId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public long getVersion() {
        return version;
    }

    public void setVersion(long version) {
        this.version = version;
    }

    public long getLastChange() {
        return lastChange;
    }

    public void setLastChange(long lastChange) {
        this.lastChange = lastChange;
    }

    public long getIdCategory() {
        return idCategory;
    }

    public void setIdCategory(long idCategory) {
        this.idCategory = idCategory;
    }

    public long getIdOwner() {
        return idOwner;
    }

    public void setIdOwner(long idOwner) {
        this.idOwner = idOwner;
    }

    public ParameterScope getScope() {
        return scope;
    }

    public void setScope(ParameterScope scope) {
        this.scope = scope;
    }

    public boolean isInputParameter() {
        return inputParameter;
    }

    public void setInputParameter(boolean inputParameter) {
        this.inputParameter = inputParameter;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }

    public String getFormula() {
        return formula;
    }

    public void setFormula(String formula) {
        this.formula = formula;
    }

    public Uncertainty getUncertainty() {
        return uncertainty;
    }

    public void setUncertainty(Uncertainty uncertainty) {
        this.uncertainty = uncertainty;
    }

    public String getExternalSource() {
        return externalSource;
    }

    public void setExternalSource(String externalSource) {
        this.externalSource = externalSource;
    }

    public String getSourceType() {
        return sourceType;
    }

    public void setSourceType(String sourceType) {
        this.sourceType = sourceType;
    }

    @Column(name = "external_source")
    private String externalSource;

    @Column(name = "source_type")
    private String sourceType;

    @Override
    public Parameter clone() {
        return null;
    }

    @Override
    public String toString() {
        return "Parameter [formula=" + formula + ", name=" + name
                + ", type=" + scope + "]";
    }
}

package models.LCA;

import javax.persistence.*;
import java.sql.Timestamp;
import java.time.Instant;

@Entity
@Table(name="tbl_Log")
public class Log
{
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    long id;

    long idBegin=0;
    long idEnd=0;

    String action="";

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getIdBegin() {
        return idBegin;
    }

    public void setIdBegin(long idBegin) {
        this.idBegin = idBegin;
    }

    public long getIdEnd() {
        return idEnd;
    }

    public void setIdEnd(long idEnd) {
        this.idEnd = idEnd;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getDescrition() {
        return descrition;
    }

    public void setDescrition(String descrition) {
        this.descrition = descrition;
    }

    public Timestamp getCalc_time() {
        return calc_time;
    }

    public void setCalc_time(Timestamp calc_time) {
        this.calc_time = calc_time;
    }

    @Lob
    String descrition="";

    private Timestamp calc_time = Timestamp.from(Instant.now());
}

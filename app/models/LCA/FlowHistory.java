package models.LCA;

import org.openlca.core.model.*;

import java.util.*;

import javax.persistence.*;

@Entity
@Table(name = "tbl_flows_history")
public class FlowHistory {
    public long getRevision() {
        return revision;
    }

    public void setRevision(long revision) {
        this.revision = revision;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public ActionType getActionType() {
        return actionType;
    }

    public void setActionType(ActionType actionType) {
        this.actionType = actionType;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    public String getRefId() {
        return refId;
    }

    public void setRefId(String refId) {
        this.refId = refId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public long getVersion() {
        return version;
    }

    public void setVersion(long version) {
        this.version = version;
    }

    public long getLastChange() {
        return lastChange;
    }

    public void setLastChange(long lastChange) {
        this.lastChange = lastChange;
    }

    public long getIdCategory() {
        return idCategory;
    }

    public void setIdCategory(long idCategory) {
        this.idCategory = idCategory;
    }

    public FlowType getFlowType() {
        return flowType;
    }

    public void setFlowType(FlowType flowType) {
        this.flowType = flowType;
    }

    public String getCasNumber() {
        return casNumber;
    }

    public void setCasNumber(String casNumber) {
        this.casNumber = casNumber;
    }

    public String getFormula() {
        return formula;
    }

    public void setFormula(String formula) {
        this.formula = formula;
    }

    public boolean isInfrastructureFlow() {
        return infrastructureFlow;
    }

    public void setInfrastructureFlow(boolean infrastructureFlow) {
        this.infrastructureFlow = infrastructureFlow;
    }

    public String getSynonyms() {
        return synonyms;
    }

    public void setSynonyms(String synonyms) {
        this.synonyms = synonyms;
    }

    public long getIdLocation() {
        return idLocation;
    }

    public void setIdLocation(long idLocation) {
        this.idLocation = idLocation;
    }

    public long getIdReferenceFlowProperty() {
        return idReferenceFlowProperty;
    }

    public void setIdReferenceFlowProperty(long idReferenceFlowProperty) {
        this.idReferenceFlowProperty = idReferenceFlowProperty;
    }

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name="revision")
    private long revision;

    @Column(name="id")
    private long id;

    @Column(name="action")
    @Enumerated(EnumType.STRING)
    private ActionType actionType;

    @Column(name="dt_datetime")
    @Temporal(TemporalType.TIMESTAMP)
    private Date timestamp;

    @Column(name = "ref_id")
    private String refId;

    @Column(name = "name")
    private String name;

    @Lob
    @Column(name = "description")
    private String description;

    // @Version
    @Column(name = "version")
    private long version;

    @Column(name = "last_change")
    private long lastChange;

    @Column(name = "f_category")
    private long idCategory;

    @Column(name = "flow_type")
    @Enumerated(EnumType.STRING)
    private FlowType flowType;

    @Column(name = "cas_number")
    private String casNumber;

    @Column(name = "formula")
    private String formula;

    @Column(name = "infrastructure_flow")
    private boolean infrastructureFlow;

    @Column(name = "synonyms")
    public String synonyms;


    @Column(name = "f_location")
    private long idLocation;


    @Column(name = "f_reference_flow_property")
    private long idReferenceFlowProperty;

    @Override
    public Flow clone() {
        return null;
    }

}

/**---------------------------   
 * PROJECT: UBUBI
 * Auth:
 *   Mathieu Dupuis
 * Date: ETE 2017
 *
 * Copyright [2017] [Mathieu Dupuis]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. 
 *
 *--------------------------*/
package models.LCA;


import javax.persistence.*;

import org.openlca.core.model.ImpactCategory;
import org.openlca.core.model.Process;


/**
 * Created by jape on 8/23/2016.
 */
@Entity
@Table(name="tbl_LCIA")
public class LCIA {

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private long id;

    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(nullable = false)
    private LCIResultSet resultSet;

    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(nullable = false)
    private ImpactCategory impactCategory;

    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(nullable = true, foreignKey = @ForeignKey( name = "none" , value = ConstraintMode.NO_CONSTRAINT))
    private Process process;

    private double totalImpact;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public LCIResultSet getResultSet() {
        return resultSet;
    }

    public void setResultSet(LCIResultSet resultSet) {
        this.resultSet = resultSet;
    }

    public ImpactCategory getImpactCategory() {
        return impactCategory;
    }

    public void setImpactCategory(ImpactCategory impactCategory) {
        this.impactCategory = impactCategory;
    }

    public Process getProcess() {
        return process;
    }

    public void setProcess(Process process) {
        this.process = process;
    }

    public double getTotalImpact() {
        return totalImpact;
    }

    public void setTotalImpact(double totalImpact) {
        this.totalImpact = totalImpact;
    }

    public double getProcessContribution() {
        return processContribution;
    }

    public void setProcessContribution(double processContribution) {
        this.processContribution = processContribution;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    private double processContribution;

    //Pas le meme unit que dans LCI
    private String unit;
}

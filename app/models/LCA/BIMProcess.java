/**---------------------------   
 * PROJECT: UBUBI
 * Auth:
 *   Mathieu Dupuis
 * Date: ETE 2017
 *
 * Copyright [2017] [Mathieu Dupuis]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. 
 *
 *--------------------------*/
package models.LCA;

import javax.persistence.*;

import org.hibernate.annotations.Formula;
import org.openlca.core.model.Process;

import java.io.Serializable;

/**
 * Created by mathieu on 8/23/2016.
 */

/*String queryModel = "SELECT bp FROM BIMProcesses bp left join Process p on bp.process " +
		"left join ProcessDocumentation pd on p.documentation " +
		"left join Exchange e on p.exchanges " +
		"left join Flow f on e.flow " +
		"left join Parameter param on p.parameters " +
		"where bp.model_id=:modelId";*/

@Entity
@Table(name="tbl_BIMProcesses")
public class BIMProcess
{
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long id;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(nullable = false)
	private BIMModel model;

	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinColumn(nullable = false)
	private Process process;

    @Column(name = "process_id", insertable = false, updatable = false)
    private long idProcess;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public BIMModel getModel() {
		return model;
	}

	public void setModel(BIMModel model) {
		this.model = model;
	}

	public Process getProcess() {
		return process;
	}

	public void setProcess(Process process) {
		this.process = process;
	}

	public long getIdProcess() {
		return idProcess;
	}

	public void setIdProcess(long idProcess) {
		this.idProcess = idProcess;
	}

	public BIMEntity getBimEntity() {
		return bimEntity;
	}

	public void setBimEntity(BIMEntity bimEntity) {
		this.bimEntity = bimEntity;
	}

	@Embedded
	private BIMEntity bimEntity;

}

/**---------------------------   
 * PROJECT: UBUBI
 * Auth:
 *   Mathieu Dupuis
 * Date: ETE 2017
 *
 * Copyright [2017] [Mathieu Dupuis]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. 
 *
 *--------------------------*/
package models.LCA;


import javax.persistence.*;

import java.sql.Timestamp;
import java.time.Instant;

/**
 * Created by jape on 8/23/2016.
 */
@Entity
@Table(name="tbl_LCIResultSet")
public class LCIResultSet {
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getIdBimModel() {
        return idBimModel;
    }

    public void setIdBimModel(long idBimModel) {
        this.idBimModel = idBimModel;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Timestamp getCalc_time() {
        return calc_time;
    }

    public void setCalc_time(Timestamp calc_time) {
        this.calc_time = calc_time;
    }

    public String getJsonData() {
        return jsonData;
    }

    public void setJsonData(String jsonData) {
        this.jsonData = jsonData;
    }

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private long id;
    
    private long idBimModel;

    private String description;
    
    private Timestamp calc_time = Timestamp.from(Instant.now());

    @Lob
    private String jsonData;
}

/**---------------------------   
 * PROJECT: UBUBI
 * Auth:
 *   Mathieu Dupuis
 * Date: ETE 2017
 *
 * Copyright [2017] [Mathieu Dupuis]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. 
 *
 *--------------------------*/
package models.LCA;

import org.openlca.core.model.ImpactCategory;
import org.openlca.core.model.Process;

import javax.persistence.*;

/**
 * Created by jape on 8/23/2016.
 */
@Entity
@Table(name="tbl_LCIGraph")
public class LCIGraph {
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public LCIResultSet getResultSet() {
        return resultSet;
    }

    public void setResultSet(LCIResultSet resultSet) {
        this.resultSet = resultSet;
    }

    public ImpactCategory getImpactCategory() {
        return impactCategory;
    }

    public void setImpactCategory(ImpactCategory impactCategory) {
        this.impactCategory = impactCategory;
    }

    public Process getProcess() {
        return process;
    }

    public void setProcess(Process process) {
        this.process = process;
    }

    public Process getParent_process() {
        return parent_process;
    }

    public void setParent_process(Process parent_process) {
        this.parent_process = parent_process;
    }

    public long getIdNode() {
        return idNode;
    }

    public void setIdNode(long idNode) {
        this.idNode = idNode;
    }

    public long getIdNodeParent() {
        return idNodeParent;
    }

    public void setIdNodeParent(long idNodeParent) {
        this.idNodeParent = idNodeParent;
    }

    public double getQty() {
        return qty;
    }

    public void setQty(double qty) {
        this.qty = qty;
    }

    public double getPercent() {
        return percent;
    }

    public void setPercent(double percent) {
        this.percent = percent;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public double getUncertainty() {
        return uncertainty;
    }

    public void setUncertainty(double uncertainty) {
        this.uncertainty = uncertainty;
    }

    //LCIA Graph
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private long id;

    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(nullable = false)
    private LCIResultSet resultSet;

    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(nullable = false)
    private ImpactCategory impactCategory;


    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(nullable = true, foreignKey = @ForeignKey( name = "none" , value = ConstraintMode.NO_CONSTRAINT))
    private Process process;

    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(nullable = true, foreignKey = @ForeignKey( name = "none" , value = ConstraintMode.NO_CONSTRAINT))
    private Process parent_process;
    private long idNode;
    private long idNodeParent;

    //TODO check if its enough precision
    private double qty;
    private double percent;
    private double total;
    private double uncertainty;

}

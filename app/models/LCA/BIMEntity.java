/**---------------------------   
 * PROJECT: UBUBI
 * Auth:
 *   Mathieu Dupuis
 * Date: ETE 2017
 *
 * Copyright [2017] [Mathieu Dupuis]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. 
 *
 *--------------------------*/
package models.LCA;

import javax.persistence.Embeddable;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

/**
 * Created by mathieu on 8/30/2016.
 */
@Embeddable
public class BIMEntity
{
	public BIMEntity()
	{
		this("",null);
	}

	public BIMEntity(String bimKey, BIMEntityType bimEntityType)
	{
		this.bimKey=bimKey;
		this.bimEntityType=bimEntityType;
	}

	public BIMEntity(BIMEntity bimEntity)
    {
        this(bimEntity.bimKey,bimEntity.bimEntityType);
    }

	public String getBimKey() {
		return bimKey;
	}

	public void setBimKey(String bimKey) {
		this.bimKey = bimKey;
	}

	public BIMEntityType getBimEntityType() {
		return bimEntityType;
	}

	public void setBimEntityType(BIMEntityType bimEntityType) {
		this.bimEntityType = bimEntityType;
	}

	private String bimKey;

	@Enumerated(EnumType.STRING)
	private BIMEntityType bimEntityType;

}

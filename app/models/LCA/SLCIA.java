/**---------------------------   
 * PROJECT: UBUBI
 * Auth:
 *   Mathieu Dupuis
 * Date: ETE 2017
 *
 * Copyright [2017] [Mathieu Dupuis]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. 
 *
 *--------------------------*/
package models.LCA;


import javax.persistence.*;

import org.openlca.core.model.Exchange;
import org.openlca.core.model.ImpactCategory;

/**
 * Created by jape on 8/23/2016.
 */
@Entity
@Table(name="tbl_SLCIA")
public class SLCIA {

    //Sensitivity LCIA

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public LCIResultSet getResultSet() {
        return resultSet;
    }

    public void setResultSet(LCIResultSet resultSet) {
        this.resultSet = resultSet;
    }

    public ImpactCategory getImpactCategory() {
        return impactCategory;
    }

    public void setImpactCategory(ImpactCategory impactCategory) {
        this.impactCategory = impactCategory;
    }

    public Exchange getExchange() {
        return exchange;
    }

    public void setExchange(Exchange exchange) {
        this.exchange = exchange;
    }

    public String getInputType() {
        return inputType;
    }

    public void setInputType(String inputType) {
        this.inputType = inputType;
    }

    public double getSensitivity() {
        return sensitivity;
    }

    public void setSensitivity(double sensitivity) {
        this.sensitivity = sensitivity;
    }

    public double getRelativeSensitivity() {
        return relativeSensitivity;
    }

    public void setRelativeSensitivity(double relativeSensitivity) {
        this.relativeSensitivity = relativeSensitivity;
    }

    public double getTotalSensitivity() {
        return totalSensitivity;
    }

    public void setTotalSensitivity(double totalSensitivity) {
        this.totalSensitivity = totalSensitivity;
    }

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private long id;

    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(nullable = false)
    private LCIResultSet resultSet;

    @ManyToOne(cascade = CascadeType.DETACH, fetch = FetchType.LAZY)
    @JoinColumn(nullable = false)
    private ImpactCategory impactCategory;

    @ManyToOne(cascade = CascadeType.DETACH, fetch = FetchType.LAZY)
    @JoinColumn(nullable = true, foreignKey = @ForeignKey( name = "none" , value = ConstraintMode.NO_CONSTRAINT))
    private Exchange exchange;

    private String inputType;
    private double sensitivity;
    private double relativeSensitivity;
    private double totalSensitivity;

}
